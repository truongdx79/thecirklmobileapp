package com.thecirkl;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.remobile.toast.RCTToastPackage;
import com.RNFetchBlob.RNFetchBlobPackage;
import fr.bamlab.rnimageresizer.ImageResizerPackage;
import com.beefe.picker.PickerViewPackage;
import com.reactnativevietnam.RNNetworkStatePackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.ninty.system.setting.SystemSettingPackage;
import com.horcrux.svg.SvgPackage;
import com.reactnativecomponent.splashscreen.RCTSplashScreenPackage;
//import io.sentry.RNSentryPackage;
import com.github.yamill.orientation.OrientationPackage;
import com.geektime.rnonesignalandroid.ReactNativeOneSignalPackage;
import com.github.xinthink.rnmk.ReactMaterialKitPackage;
import com.babisoft.ReactNativeLocalization.ReactNativeLocalizationPackage;
import com.BV.LinearGradient.LinearGradientPackage;
import com.wix.interactable.Interactable;
//import com.reactnative.ivpusic.imagepicker.PickerPackage;
import ui.gradientblurview.RNGradientBlurViewPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.rpt.reactnativecheckpackageinstallation.CheckPackageInstallationPackage;
import com.cmcewen.blurview.BlurViewPackage;
import com.remobile.toast.RCTToastPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new RNFetchBlobPackage(),
            new ImageResizerPackage(),
            new PickerViewPackage(),
            new RNNetworkStatePackage(),
            new VectorIconsPackage(),
            new SystemSettingPackage(),
            new SvgPackage(),
            new RCTSplashScreenPackage(),
//            new RNSentryPackage(),
            new OrientationPackage(),
            new ReactNativeOneSignalPackage(),
            new ReactMaterialKitPackage(),
            new ReactNativeLocalizationPackage(),
            new LinearGradientPackage(),
            new Interactable(),
//            new PickerPackage(),
            new RNGradientBlurViewPackage(),
            new RNDeviceInfo(),
            new CheckPackageInstallationPackage(),
            new BlurViewPackage(),
            new RCTToastPackage()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }
}
