/**
* Created by bav on Thu May 31 2018
* Copyright (c) 2018 bav
*/

import { AppRegistry } from 'react-native';
import Setup from './src/setup';

AppRegistry.registerComponent('thecirkl', () => Setup);
