/**
* Created by bav on Mon Jul 16 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import { Scene, Router, Tabs, Stack, Actions } from 'react-native-router-flux';
import { transitionConfig, getSceneStyle } from '../common/transitionConfig';
import { TabBarTablet } from '../components';
import langs from '../languages/common';
import { common } from '../config/common';

// import component
import LoadInitial from '../container/loadInitial';
import AppIntro from '../container/intro';
import Login from '../container/login';
import Register from '../container/register';
import ForgotPassword from '../container/forgotPassword';
import CreatePassword from '../container/forgotPassword';

import Dashboard from '../container/dashboard';
import Setting from '../container/setting';

class NavigatorTablet extends PureComponent {
  render() {
    return (
      <Router getSceneStyle={getSceneStyle}>
        <Stack key='root' transitionConfig={transitionConfig}>
          <Scene
            key='loadingInitial'
            component={LoadInitial}
            swipeEnabled={false}
            panHandlers={null}
            direction='fade'
            hideNavBar
          />
          <Scene
            key='appIntro'
            component={AppIntro}
            swipeEnabled={false}
            panHandlers={null}
            hideNavBar
          />
          <Scene
            key='login'
            component={Login}
            swipeEnabled={false}
            panHandlers={null}
            hideNavBar
            initial
          />
          <Scene
            key='register'
            component={Register}
            hideNavBar
          />
          <Scene
            key='forgotPassword'
            component={ForgotPassword}
            hideNavBar
          />
          <Scene
            key='createPassword'
            component={CreatePassword}
            hideNavBar
          />

          <Tabs
            key="tabbar"
            swipeEnabled={false}
            panHandlers={null}
            showLabel
            hideNavBar
            tabBarPosition="bottom"
            tabBarComponent={(props) =>
              <TabBarTablet
                backgroundColor={common().BACKGROUND_COLOR_TABBAR}
                colorIconActive={common().COLOR_TEXT_TABBAR_ACTIVE}
                colorIconInactive={common().COLOR_TEXT_TABBAR_INACTIVE}
                separator
                bgSeparator={common().COLOR_TEXT_TABBAR_INACTIVE}
                {...props}
              />
            }
            activeTintColor={common().COLOR_TEXT_TABBAR_ACTIVE}
            inactiveTintColor={common().COLOR_TEXT_TABBAR_INACTIVE}
            transitionConfig={transitionConfig}
            direction='horizontal'
            initial
          >
            <Stack
              key="tab_1"
              hideNavBar
              transitionConfig={transitionConfig}
              icon='dashboard'
              direction='horizontal'
            >
              <Scene
                key='dashboard'
                component={Dashboard}
                swipeEnabled={false}
                panHandlers={null}
              />
            </Stack>
            <Stack
              key="tab_2"
              hideNavBar
              transitionConfig={transitionConfig}
              icon='home'
              direction='horizontal'
            >
            </Stack>
            <Stack
              key="tab_3"
              hideNavBar
              transitionConfig={transitionConfig}
              icon='music-note'
              direction='horizontal'
            >
            </Stack>
            <Stack
              key="tab_4"
              hideNavBar
              transitionConfig={transitionConfig}
              icon='view-agenda'
              direction='horizontal'
            >
            </Stack>
            <Stack
              key="tab_5"
              hideNavBar
              transitionConfig={transitionConfig}
              icon='menu'
              direction='horizontal'
            >
              <Scene
                key='setting'
                component={Setting}
                swipeEnabled={false}
                panHandlers={null}
              />
            </Stack>
          </Tabs>
        </Stack>
      </Router>
    );
  }
}

export default NavigatorTablet;
