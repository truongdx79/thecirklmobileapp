/**
* Created by bav on Mon Jul 16 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import { Scene, Router, Tabs, Stack, Actions } from 'react-native-router-flux';
import { transitionConfig, getSceneStyle } from '../common/transitionConfig';
import { TabBar } from '../components';
import langs from '../languages/common';
import { common } from '../config/common';

// import component
import LoadInitial from '../container/loadInitial';
import AppIntro from '../container/intro';
import Login from '../container/login';
import LoginLinkedin from '../container/login/LoginLinkedin';
import Interest from '../container/profile/Interest';
import Education from '../container/profile/Education';
import Personal from '../container/profile/Personal';
import Social from '../container/profile/Social';
import UpdateProfile from '../container/profile';
import Register from '../container/register';
import ForgotPassword from '../container/forgotPassword';
import CreatePassword from '../container/forgotPassword/CreatePassword';
import Home from '../container/home';
import EventDetail from '../container/home/EventDetail';

import Dashboard from '../container/dashboard';
import Setting from '../container/setting';

class NavigatorMobile extends PureComponent {
  render() {
    return (
      <Router getSceneStyle={getSceneStyle}>
        <Stack key='root' transitionConfig={transitionConfig}>
          <Scene
            key='loadingInitial'
            component={LoadInitial}
            swipeEnabled={false}
            panHandlers={null}
            hideNavBar
            // onEnter={() => {
            //   setTimeout(() => {
            //     this.props.autoLogin();
            //   }, 4000);
            // }}
          />
          {/* <Scene
            key='appIntro'
            component={AppIntro}
            swipeEnabled={false}
            panHandlers={null}
            hideNavBar
          /> */}
          <Scene
            key='login'
            component={Login}
            swipeEnabled={false}
            panHandlers={null}
            hideNavBar
          // initial
          />
          {/* <Scene
            key='loginLinkedin'
            component={LoginLinkedin}
            swipeEnabled={false}
            panHandlers={null}
            hideNavBar
          // initial
          /> */}
          <Scene
            key='interest'
            component={Interest}
            hideNavBar
          // initial
          />
          <Scene
            key='personal'
            component={Personal}
            hideNavBar
          // initial
          />
          <Scene
            key='education'
            component={Education}
            hideNavBar
          // initial
          />
          <Scene
            key='social'
            component={Social}
            hideNavBar
          // initial
          />
          <Scene
            key='updateProfile'
            component={UpdateProfile}
            hideNavBar
          // initial
          />
          {/* <Scene
            key='register'
            component={Register}
            hideNavBar
          // initial
          /> */}
          <Scene
            key='forgotPassword'
            component={ForgotPassword}
            hideNavBar
          />
          <Scene
            key='createPassword'
            component={CreatePassword}
            hideNavBar
          />

          <Tabs
            key="tabbar"
            swipeEnabled={false}
            panHandlers={null}
            showLabel
            hideNavBar
            tabBarPosition="bottom"
            tabBarComponent={(props) =>
              <TabBar
                {...props}
                backgroundColor={common().BACKGROUND_COLOR_TABBAR}
                colorIconActive={common().COLOR_TEXT_TABBAR_ACTIVE}
                colorIconInactive={common().COLOR_TEXT_TABBAR_INACTIVE}
              />
            }
            activeTintColor={common().COLOR_TEXT_TABBAR_ACTIVE}
            inactiveTintColor={common().COLOR_TEXT_TABBAR_INACTIVE}
            transitionConfig={transitionConfig}
          // initial
          >
            <Stack
              key="tab_1"
              hideNavBar
              transitionConfig={transitionConfig}
              icon='home'
            >
              <Scene
                key='home'
                component={Home}
                swipeEnabled={false}
                panHandlers={null}
              />
              <Scene
                key='eventDetail'
                component={EventDetail}
                swipeEnabled={false}
                panHandlers={null}
              />
            </Stack>
            <Stack
              key="tab_2"
              hideNavBar
              transitionConfig={transitionConfig}
              icon='work'
            >
              <Scene
                key='setting'
                component={Setting}
                swipeEnabled={false}
                panHandlers={null}
              />
            </Stack>
            <Stack
              key="tab_3"
              hideNavBar
              transitionConfig={transitionConfig}
              icon='gps-not-fixed'
            >
              <Scene
                key='setting'
                component={Setting}
                swipeEnabled={false}
                panHandlers={null}
              />
            </Stack>
            <Stack
              key="tab_4"
              hideNavBar
              transitionConfig={transitionConfig}
              icon='email'
            >
              <Scene
                key='setting'
                component={Setting}
                swipeEnabled={false}
                panHandlers={null}
              />
            </Stack>
            <Stack
              key="tab_5"
              hideNavBar
              transitionConfig={transitionConfig}
              icon='settings'
            >
              <Scene
                key='setting'
                component={Setting}
                swipeEnabled={false}
                panHandlers={null}
              />
            </Stack>
          </Tabs>
        </Stack>
      </Router>
    );
  }
}

export default NavigatorMobile;
