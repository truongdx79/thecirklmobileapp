/**
* Created by bav on Thu May 31 2018
* Copyright (c) 2018 bav
*/
'use strick';

import React, { PureComponent } from 'react';
import { View, BackHandler } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { RNNetworkStateEventEmitter } from "react-native-network-state";
import Notify from './notify';
import { isTablet } from './common/utils';
import { Alert, Loading, SmartNotify } from './components';
import { autoLogin } from './redux/actions/authen';
import langs from './languages/common';

let Navigator = null;

class AppNavigator extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      loadComponent: true
    }
  }

  componentDidMount() {
    // if (isTablet) {
    //   Navigator = require('./navigator/NavigatorTabLet').default
    // } else {
    Navigator = require('./navigator/NavigatorMobile').default
    // }
    this.setState({ loadComponent: false })

    BackHandler.addEventListener("hardwareBackPress", this._handleBackButton);
    // RNNetworkStateEventEmitter.addListener('networkChanged', this._handleNetworkChange);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this._handleBackButton);
    // RNNetworkStateEventEmitter.removeEventListener('networkChanged', this._handleNetworkChange);
  }

  /**
   * type of data ==>
   * isConnected: boolean
   * type: string
   * isFast: boolean
   */
  _handleNetworkChange = (data) => {
    console.log('_handleNetworkChange', data)
    if (!data.isConnected) {
      const data = {
        title: langs.lostConnectInternet,
        timeDismiss: 4000,
        autoHide: true
      }
      window.notify.show(data)
    }
  }

  /**
   * function call when back android pressed
   * default return false, component will set Action.pop()
   * return true to overwrite
   */
  _handleBackButton = () => {
    console.log(`Actions.currentScene => ${Actions.currentScene}`);
    switch (Actions.currentScene) {
      case 'loadingInitial':
      case 'login':
        console.log(`exitApp`);
        BackHandler.exitApp();
        return true
      default:
        return false
    }
  }

  // _handleBackButton = () => {
  //   // console.log('handleBackButton')
  //   switch (Actions.currentScene) {
  //     case 'loadingInitial':
  //     case 'login':
  //       BackHandler.exitApp();
  //       return true;
  //     // case 'dashboard':
  //     // case 'inbox':
  //     // case 'utilities':
  //     // case 'setting':
  //     //   return true
  //     // case 'createTicket':
  //     //   // need pop to notifi
  //     //   if (Actions._currentParams && Actions._currentParams.shouldBackInbox) {
  //     //     Actions.pop()
  //     //     Actions.tab_2()
  //     //   } else {
  //     //     Actions.pop()
  //     //   }
  //     //   return true
  //     default:
  //       return false
  //   }
  // }

  render() {
    return (
      <View style={{ flex: 1 }}>
        {
          this.state.loadComponent ? null : <Navigator {...this.props} />
        }
        <Notify />
        <Alert ref={ref => window.customAlert = ref} />
        <Loading ref={ref => window.customLoading = ref} loadingRef />
        <SmartNotify ref={ref => window.notify = ref} />
      </View>
    );
  }
}

const mapDispathToProps = {
  autoLogin
}

const mapStateToProps = (state) => {
  return {
    language: state.config.language
  }
}

export default connect(mapStateToProps, mapDispathToProps)(AppNavigator);
