/**
* Created by bav on Tue Jul 17 2018
* Copyright (c) 2018 bav
*/

import Input from './Input';
import InputLabel from './InputLabel';
import InputLabel2 from './InputLabel2';
import InputPassword from './InputPassword';
import InputPhoneCode from './InputPhoneCode';

export {
  Input,
  InputLabel,
  InputLabel2,
  InputPassword,
  InputPhoneCode
}
