import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Dimensions,
  ViewPropTypes
} from 'react-native'
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { MKButton } from 'react-native-material-kit';
import { common } from '../../config/common';

const SCREEN = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {

  },
  body: {
    position: 'relative',
    // paddingLeft: 16,
    // paddingRight: 14,
    flexDirection: 'row',
    // alignItems: 'center',
  },
  view: {
    backgroundColor: common().INPUT_BACKGROUND_COLOR
  },
  viewFocus: {
    backgroundColor: common().INPUT_BACKGROUND_FORCUS
  },
  viewError: {
    borderWidth: 1,
    borderColor: 'rgba(255, 0, 0, .3)',
  },
  TextInput: {
    flex: 1,
    // paddingLeft: 5,
    padding: 0,
    margin: 0,
    color: 'rgba(0, 0, 0, 1)',
    fontSize: common().FONT_SIZE_TITLE
  },
  rightButton: {
    overflow: 'hidden',
    backgroundColor: 'transparent',
    width: 32,
    height: 32,
    borderRadius: 16,
    justifyContent: 'center',
    alignItems: 'center'
  },
  leftIcon: {
    marginRight: 16
  },
  icon: {

  },
  txtTitle: {
    fontSize: common().FONT_SIZE_CONTENT,
    fontWeight: common().FONT_WEIGHT_HEADER,
    color: 'rgba(0, 0, 0, 0.38)',
    backgroundColor: 'transparent'
  },
  separator: {
    height: StyleSheet.hairlineWidth,
    backgroundColor: common().COLOR_SEPARATOR
  },
})

class InputLabel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isFocus: false,
      text: this.props.value || ''
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.value != nextProps.value) {
      this.setState({ text: nextProps.value || '' })
    }
  }

  onRightButton = () => {
    console.log(`onRightButton`)
    this.props.onChangeText && this.props.onChangeText('');
    this.setState({
      isFocus: true,
      text: ''
    })
  }

  onFocus() {
    this.setState({
      isFocus: true
    })
  }

  focus() {
    this.refs[this.props.textInputRef].focus();
  }

  onBlur() {
    this.setState({
      isFocus: false
    });
  }

  onChangeText(text) {
    this.props.onChangeText && this.props.onChangeText(text);
    this.setState({
      isFocus: true,
      text: text
    })
  }

  render() {
    let {
      style,
      type,
      width,
      height,
      rounded,
      borderRadius,
      placeholder,
      placeholderTextColor,
      maxLength,
      editable,
      selectionColor,
      textInputRef,
      returnKeyType,
      error,
      errorStyle,
      rightIcon,
      rightIconName,
      leftIcon,
      leftIconName,
      textInputStyle,
      title,
      numberLines,
      ...otherProps
    } = this.props

    let { text, isFocus } = this.state;

    return (
      <View style={[styles.container, style]}>
        <Text numberOfLines={1} style={[styles.txtTitle]}>
          {title}
        </Text>
        <View
          style={[
            styles.body,
            {
              width,
              // height: undefined,
            },
            {paddingTop: 8, paddingBottom: 8},
            // style,
            // rounded ? { borderRadius } : undefined,
            error ? errorStyle || styles.viewError : '',
            // isFocus ? styles.viewFocus : styles.view
          ]}
        >
          {
            leftIcon && (
              <View style={styles.leftIcon}>
                <FontAwesome style={styles.icon} name={leftIconName} size={24} color={'#5E5E5E'} />
              </View>
            )
          }

          <TextInput
            style={[styles.TextInput, textInputStyle ? textInputStyle : undefined, rightIcon ? { paddingRight: 8 } : undefined]}
            ref={textInputRef}
            underlineColorAndroid='transparent'
            returnKeyType={returnKeyType}
            autoCapitalize='none'
            autoCorrect={false}
            maxLength={maxLength}
            editable={editable}
            // numberOfLines={5}
            multiline={true}
            placeholder={placeholder}
            placeholderTextColor={placeholderTextColor}
            selectionColor={selectionColor}
            onChangeText={(text) => this.onChangeText(text)}
            onFocus={this.onFocus.bind(this)}
            onBlur={this.onBlur.bind(this)}
            value={text}
            {...otherProps}
          />
          {
            (rightIcon && isFocus && text !== '') && (
              <MKButton
                onPress={this.onRightButton}
                style={styles.rightButton}
              >
                <Icon style={styles.icon} name={rightIconName} size={24} color={'#5E5E5E'} />
              </MKButton>
            )
          }
        </View>
        <View style={styles.separator} />
      </View>
    )
  }
}

InputLabel.defaultProps = {
  placeholder: '',
  placeholderTextColor: common().INPUT_COLOR_TEXT_PLACEHOLDER,
  maxLength: undefined,
  editable: true,
  selectionColor: common().INPUT_SELECTION_COLOR,
  error: false,
  returnKeyType: 'next',
  width: SCREEN.width - 36,
  // height: 40,
  rounded: false,
  borderRadius: 20,
  rightIcon: false,
  leftIcon: false,
  rightIconName: 'close',
  leftIconName: 'account-circle',
  numberLines: 1,
}

InputLabel.propTypes = {
  style: ViewPropTypes.style,
  focusStyle: ViewPropTypes.style,
  placeholder: PropTypes.string,
  placeholderTextColor: PropTypes.string,
  maxLength: PropTypes.number,
  editable: PropTypes.bool,
  selectionColor: PropTypes.string,
  onChangeText: PropTypes.func,
  textInputRef: PropTypes.string,
  error: PropTypes.bool,
  errorStyle: ViewPropTypes.style,
  returnKeyType: PropTypes.string,
  focus: PropTypes.func,
  value: PropTypes.string,
  rightIcon: PropTypes.bool,
  rightIconName: PropTypes.string,
  leftIcon: PropTypes.bool,
  leftIconName: PropTypes.string,
  title: PropTypes.string,
  textInputStyle: PropTypes.any,
  rounded: PropTypes.bool,
  borderRadius: PropTypes.number,
  width: PropTypes.number,
  height: PropTypes.number,
  numberLines: PropTypes.number,
}

export default InputLabel