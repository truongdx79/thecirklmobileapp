/**
* Created by bav on Thu Jul 26 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, ViewPropTypes, Image } from 'react-native';
import { MKButton } from 'react-native-material-kit';
import { CachedImage } from 'react-native-cached-image';
import { imgs } from '../../config/imgs';

const styles = StyleSheet.create({
  container: {
    overflow: 'hidden',
    backgroundColor: 'rgba(98, 181, 218, 1)',
    alignItems: 'center',
    justifyContent: 'center'
  },
  image: {
    borderColor: 'white',
    borderWidth: 1
  }
});

class Avatar extends PureComponent {
  render() {
    const {
      width,
      round,
      imageSource,
      style,
      onPress,
      cacheImage
    } = this.props;
    const w = width + 10;
    const w2 = width;
    const Component = onPress ? MKButton : View
    const ImageComponent = cacheImage ? CachedImage : Image
    return (
      <Component
        style={[
          styles.container,
          {
            width: w,
            height: w
          },
          round ? { borderRadius: w / 2 } : undefined
        ]}
        onPress={onPress}
      >
        <ImageComponent
          defaultSource={imgs().backgroundImageLogin}
          style={[styles.image, {
            width: w2,
            height: w2
          }]}
          source={typeof (imageSource) === 'number' ? imageSource : { uri: imageSource }}
          resizeMode='cover'
          borderRadius={w2 / 2}
        />
      </Component>
    );
  }
}

Avatar.defaultProps = {
  width: 48,
  round: true,
  cacheImage: false
}

Avatar.propTypes = {
  width: PropTypes.number,
  round: PropTypes.bool,
  imageSource: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  style: ViewPropTypes.style,
  onPress: PropTypes.func,
  cacheImage: PropTypes.bool
}

export default Avatar;
