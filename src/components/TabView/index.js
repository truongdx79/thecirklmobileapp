/**
* Created by bav on Tue Jul 24 2018
* Copyright (c) 2018 bav
*/

import TabView from './ScrollTabView';
import SingleTabView from './SingleTabView';

export {
  TabView,
  SingleTabView
}
