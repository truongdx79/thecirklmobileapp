/**
* Created by bav on Tue Jul 24 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text, FlatList, Dimensions, Animated, TouchableOpacity, Platform } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';

const ComponentAnimated = Animated.createAnimatedComponent(FlatList)
const SCREEN = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

class TabView extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      currentIndex: 0
    }

    this.refTabview = React.createRef();
    this._scrollX = new Animated.Value(0)
  }

  handlePressHeader = (index) => {
    this.refTabview.current._component.scrollToIndex({ index, animated: true });

    if (Platform.OS === 'android') {
      this.setState({ currentIndex: index })
    }
  }

  onScrollEnd = ({ nativeEvent }) => {
    const currentIndex = Math.floor(nativeEvent.contentOffset.x / nativeEvent.layoutMeasurement.width)

    this.setState({ currentIndex })
  }

  _onTouchEnd = ({ nativeEvent }) => {
    
  }

  renderNavBar = () => {
    return (
      <Animated.View
        style={{
          alignItems: 'center', flexDirection: 'row',
          opacity: this.props.scrollY.interpolate({
            inputRange: [0, 60, 100],
            outputRange: [1, 0, 0]
          }),
          height: 60
        }}
      >
        <Animated.Text
          style={{
            color: '#fff', marginLeft: 16, fontSize: 20, fontWeight: '500',
            transform: [
              {
                scale: this.props.scrollY.interpolate({
                  inputRange: [-100, -60, 0, 60, 100],
                  outputRange: [1.2, 1.2, 1, 1, 1]
                })
              }
            ]
          }}
        >
          Su kien
        </Animated.Text>
      </Animated.View>
    )
  }

  renderItemHeader = ({ item, index }) => {
    const { currentIndex } = this.state;

    return (
      <TouchableOpacity
        style={{ flex: 1, justifyContent: 'center', width: SCREEN.width / 4, paddingLeft: 16 }}
        onPress={() => this.handlePressHeader(index)}
      >
        <Animated.Text
          style={{
            color: currentIndex == index ? '#fff' : 'rgba(255, 255, 255, 0.6)',
            fontWeight: '400',
            fontSize: 14
          }}
          numberOfLines={1}
        >
          {item.title}
        </Animated.Text>
      </TouchableOpacity>
    )
  }

  renderHeader = () => {
    let {
      routes
    } = this.props;

    return (
      <Animated.View
        style={{
          borderBottomWidth: 1, borderBottomColor: 'rgba(255, 255, 255, 0.2)',
          transform: [
            {
              translateY: this.props.scrollY.interpolate({
                inputRange: [-60, 0, 60, 100],
                outputRange: [0, 0, -60, -60]
              })
            }
          ],
          height: 68
        }}
      >
        <FlatList
          data={routes}
          keyExtractor={(item, index) => String(index)}
          renderItem={this.renderItemHeader}
          extraData={this.state}
          horizontal
          showsHorizontalScrollIndicator={false}
        />
        <Animated.View
          style={{
            height: 3, backgroundColor: '#008AEE', width: 70, marginLeft: 16,
            transform: [
              {
                translateX: this._scrollX.interpolate({
                  inputRange: [0, SCREEN.width],
                  outputRange: [0, SCREEN.width / 4]
                })
              }
            ]
          }}
        >

        </Animated.View>
      </Animated.View>
    )
  }

  renderItem = ({ item, index }) => {
    return (
      <View style={{ width: SCREEN.width, height: SCREEN.height }}>
        {item.component}
      </View>
    )
  }

  render() {
    let {
      routes,
      scrollEnable
    } = this.props;

    return (
      <View style={{ flex: 1 }}>
        <Animated.View
          style={{
            marginBottom: 12,
            height: this.props.scrollY.interpolate({
              inputRange: [-60, 0, 60, 100],
              outputRange: [128, 128, 68, 68]
            })
          }}
        >
          {
            this.renderNavBar()
          }
          {
            this.renderHeader()
          }
        </Animated.View>

        <ComponentAnimated
          data={routes}
          ref={this.refTabview}
          keyExtractor={(item, index) => String(index)}
          extraData={this.state}
          renderItem={this.renderItem}
          horizontal
          showsHorizontalScrollIndicator={false}
          pagingEnabled
          removeClippedSubviews
          onMomentumScrollEnd={this.onScrollEnd}
          onTouchEnd={this._onTouchEnd}
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { x: this._scrollX } } }],
            { useNativeDriver: true }
          )}
          getItemLayout={(data, index) => (
            { length: SCREEN.width, offset: SCREEN.width * index, index }
          )}
        />
      </View>
    );
  }
}

TabView.defaultProps = {
  scrollEnable: Platform.OS == 'ios' ? true : false
}

TabView.propTypes = {
  routes: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string.isRequired,
      title: PropTypes.string,
      icon: PropTypes.string,
      component: PropTypes.node.isRequired
    }).isRequired
  ).isRequired,
  scrollEnable: PropTypes.bool
}

export default TabView;
