/**
* Created by bav on Tue Jul 24 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text, Dimensions } from 'react-native';
import { ScrollableTabView, DefaultTabBar, ScrollableTabBar } from '@valdio/react-native-scrollable-tabview';
import { common } from '../../config/common';
import DeviceInfo from 'react-native-device-info'
import { tabbarHeight } from '../../common/utils'

const SCREEN = Dimensions.get('window');

export const isTablet = DeviceInfo.isTablet();

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

class TabView extends PureComponent {
  render() {
    let {
      marginBottom
    } = this.props;
    return (
      <ScrollableTabView
        tabBarUnderlineStyle={{ height: 3, backgroundColor: common().TABVIEW_UNDERLINE_COLOR, width: isTablet ? 67 : 65, marginLeft: isTablet ? 40 : 16 }}
        tabBarBackgroundColor={common().TABVIEW_BACKGROUND_COLOR}
        tabBarActiveTextColor={common().TABVIEW_ACTIVE_TEXT_COLOR}
        tabBarInactiveTextColor={common().TABVIEW_INACTIVE_TEXT_COLOR}
        tabBarTextStyle={{ fontSize: isTablet ? 20 : common().FONT_SIZE_CONTENT, marginLeft: isTablet ? 20 : -4, }}
        renderTabBar={() =>
          <ScrollableTabBar
            style={{ borderBottomWidth: 1, borderBottomColor: common().TABVIEW_SEPARATOR_COLOR, marginBottom, height: 68 }}
            tabStyle={{ height: 68 }}
            tabsContainerStyle={[
              isTablet ? { justifyContent: 'flex-start' } : { justifyContent: 'flex-start' }
            ]}
          />
        }
        ref={(tabView) => { this.tabView = tabView; }}
        {...this.props}
      >
        {this.props.children}
      </ScrollableTabView>
    );
  }
}

TabView.defaultProps = {
  marginBottom: 3
}

TabView.propTypes = {
  marginBottom: PropTypes.number
}

export default TabView;
