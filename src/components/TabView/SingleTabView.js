/**
* Created by bav on Fri Aug 03 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text, TouchableWithoutFeedback, Keyboard, ViewPropTypes } from 'react-native';
import { common } from '../../config/common';

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  viewHeader: {
    height: 68,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingHorizontal: 16,
    borderBottomWidth: 1
  },
  viewTitle: {
    flex: 1,
    justifyContent: 'center',
  },
  tabLabel: {
    fontSize: common().FONT_SIZE_CONTENT,
    fontWeight: common().FONT_WEIGHT_HEADER
  },
  viewUnderline: {
    height: 3,
    marginBottom: -0.5
  },
  viewSeparator: {

  },
  viewBody: {
    flex: 1,
    paddingTop: 3
  }
});

class SingleTabView extends PureComponent<Props> {
  render() {
    const {
      tabLabel,
      labelColor,
      separatorColor,
      tabbarUnderlineWidth,
      tabBarUnderlineColor,
      dismissKeyboard,
      styleContent
    } = this.props;

    if (dismissKeyboard) {
      return (
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <View style={styles.container}>
            <View style={[styles.viewHeader, { borderBottomColor: separatorColor }]}>
              <View>
                <View style={styles.viewTitle}>
                  {
                    tabLabel && (
                      <Text style={[styles.tabLabel, { color: labelColor }]}>
                        {tabLabel}
                      </Text>
                    )
                  }
                </View>
                <View style={[styles.viewUnderline, { width: tabbarUnderlineWidth, backgroundColor: tabBarUnderlineColor }]} />
              </View>
            </View>
            <View style={[styles.viewBody, styleContent]}>
              {this.props.children}
            </View>
          </View>
        </TouchableWithoutFeedback>
      )
    }

    return (
      <View style={styles.container}>
        <View style={[styles.viewHeader, { borderBottomColor: separatorColor }]}>
          <View>
            <View style={styles.viewTitle}>
              {
                tabLabel && (
                  <Text style={[styles.tabLabel, { color: labelColor }]}>
                    {tabLabel}
                  </Text>
                )
              }
            </View>
            {tabLabel && <View style={[styles.viewUnderline, { width: tabbarUnderlineWidth, backgroundColor: tabBarUnderlineColor }]} />}
          </View>
        </View>
        <View style={[styles.viewBody, styleContent]}>
          {this.props.children}
        </View>
      </View>
    );
  }
}

SingleTabView.defaultProps = {
  labelColor: common().TABVIEW_ACTIVE_TEXT_COLOR,
  separatorColor: common().TABVIEW_SEPARATOR_COLOR,
  tabbarUnderlineWidth: 70,
  tabBarUnderlineColor: common().TABVIEW_UNDERLINE_COLOR,
  dismissKeyboard: false
}

SingleTabView.propTypes = {
  tabLabel: PropTypes.string,
  labelColor: PropTypes.string,
  separatorColor: PropTypes.string,
  tabbarUnderlineWidth: PropTypes.number,
  tabBarUnderlineColor: PropTypes.string,
  styleContent: ViewPropTypes.style,
  dismissKeyboard: PropTypes.bool
}

interface Props {
  /**
   * label of tab
   */
  tabLabel?: string;
  labelColor?: string;
  separatorColor?: string;
  /**
   * default value width = 70
   */
  tabbarUnderlineWidth?: number;
  tabBarUnderlineColor?: string;
  /**
   * style container
   */
  styleContent?: StyleProp<ViewStyle>;
  /**
   * enable mode dismiss keyboard (wrap TouchableWithoutFeedback)
   */
  dismissKeyboard?: boolean;
}

export default SingleTabView;
