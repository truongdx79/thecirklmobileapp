/**
* Created by bav on Wed Jul 25 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Dimensions, ViewPropTypes, ImageBackground, TouchableOpacity, Animated } from 'react-native';
import { CachedImage } from 'react-native-cached-image';
import LinearGradient from 'react-native-linear-gradient';
import { MKButton } from 'react-native-material-kit';
import { imgs } from '../../config/imgs';
import { isTablet } from '../../common/utils';

const SCREEN = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    overflow: 'hidden'
  },
  imageBackground: {
    flex: 1,
    overflow: 'hidden'
  },
  overlay: {
    flex: 1,
    overflow: 'hidden',
    padding: 16
  }
});

class Card extends PureComponent<Props> {
  render() {
    let {
      imageSource,
      containerStyle,
      materialButton,
      style,
      width,
      height,
      borderRadius,
      overLayColor,
      onPress,
      cacheImage,
      defaultImageSource,
      gradientOverlay,
      animated,
      styleImage
    } = this.props;
    let Component = onPress ? materialButton ? MKButton : TouchableOpacity : View
    let ComponentImage = cacheImage ? CachedImage : ImageBackground
    let Overlay = gradientOverlay ? LinearGradient : View
    if (animated) {
      Component = onPress ? materialButton ? Animated.createAnimatedComponent(MKButton) : Animated.createAnimatedComponent(TouchableOpacity) : Animated.View
      ComponentImage = cacheImage ? Animated.createAnimatedComponent(CachedImage) : Animated.createAnimatedComponent(ImageBackground)
      Overlay = gradientOverlay ? Animated.createAnimatedComponent(LinearGradient) : Animated.View
    }

    return (
      <Component
        activeOpacity={0.8}
        onPress={onPress}
        style={[
          styles.container,
          {
            width,
            height
          },
          borderRadius ? { borderRadius: 12 } : undefined,
          containerStyle
        ]}
        pointerEvents='box-none'
      >
        {
          imageSource ? (
            <ComponentImage
              defaultSource={defaultImageSource}
              style={[styles.imageBackground, styleImage]}
              source={typeof (imageSource) === 'number' ? imageSource : { uri: imageSource }}
              resizeMode='cover'
            >
              <Overlay
                start={gradientOverlay ? { x: 0, y: 0 } : undefined}
                end={gradientOverlay ? { x: 0, y: 1 } : undefined}
                locations={[0, 0.4, 0.6, 1]}
                colors={[
                  'rgba(0, 0, 0, 0.6)',
                  'transparent',
                  'transparent',
                  'rgba(0, 0, 0, 0.6)'
                ]}
                style={[
                  styles.overlay,
                  gradientOverlay ? undefined : { backgroundColor: overLayColor },
                  style
                ]}
                pointerEvents='box-none'
              >
                {this.props.children}
              </Overlay>
            </ComponentImage>
          ) : (
              <Overlay
                start={gradientOverlay ? { x: 0, y: 0 } : undefined}
                end={gradientOverlay ? { x: 0, y: 1 } : undefined}
                locations={[0, 0.6, 1]}
                colors={[
                  'transparent',
                  'transparent',
                  'rgba(0, 0, 0, 0.6)'
                ]}
                style={[
                  styles.overlay,
                  gradientOverlay ? undefined : { backgroundColor: overLayColor },
                  style
                ]}
                pointerEvents='box-none'
              >
                {this.props.children}
              </Overlay>
            )
        }
      </Component>
    );
  }
}

Card.defaultProps = {
  borderRadius: true,
  overLayColor: 'rgba(0, 0, 0, 0.4)',
  height: isTablet ? (SCREEN.width / 2 - 32) * 9 / 16 : (SCREEN.width - 32) * 9 / 16,
  width: isTablet ? (SCREEN.width / 2 - 32) : (SCREEN.width - 32),
  materialButton: true,
  cacheImage: false,
  defaultImageSource: imgs().listhome_bedroom,
  gradientOverlay: true,
  animated: false
}

Card.propTypes = {
  imageSource: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  style: ViewPropTypes.style,
  containerStyle: ViewPropTypes.style,
  styleImage: ViewPropTypes.style,
  width: PropTypes.number,
  height: PropTypes.number,
  borderRadius: PropTypes.bool,
  overLayColor: PropTypes.string,
  onPress: PropTypes.func,
  materialButton: PropTypes.bool,
  cacheImage: PropTypes.bool,
  defaultImageSource: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  gradientOverlay: PropTypes.bool,
  animated: PropTypes.bool
}

interface Props {
  imageSource: number | string;
  style?: StyleProp<ViewStyle>;
  containerStyle?: StyleProp<ViewStyle>;
  styleImage?: StyleProp<ViewStyle>;
  /**
   * default width = SCREEN.width - 32
   */
  width?: number;
  /**
   * default height = 192
   */
  height?: number;
  /**
   * default true
   */
  borderRadius?: boolean;
  /**
   * default value = 'rgba(0, 0, 0, 0.4)'
   */
  overLayColor?: string;
  onPress?: () => void;
  /**
   * using material style when press. Default true
   */
  materialButton?: boolean;
  /**
   * Using react-native-cacheImage to load. Default false
   */
  cacheImage?: boolean;
  /**
  * Set default image when cacheImage = true
  */
  defaultImageSource?: string | number;
  /**
   * Using gradient overlay. Defaule true
   */
  gradientOverlay?: boolean;
  animated?: boolean;
}

export default Card;
