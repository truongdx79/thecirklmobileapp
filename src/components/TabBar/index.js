/**
* Created by bav on Mon Jun 04 2018
* Copyright (c) 2018 bav
*/

import TabBar from './TabBar';
import TabBarTablet from './TabBarTablet';

export {
  TabBar,
  TabBarTablet
}
