/**
* Created by bav on Mon Jun 04 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text, Dimensions, findNodeHandle, Platform, ViewPropTypes, TouchableOpacity, ScrollView, Image } from 'react-native';
import { BlurView } from 'react-native-blur';
import { MKButton } from 'react-native-material-kit';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/dist/Ionicons';
import IconMaterial from 'react-native-vector-icons/dist/MaterialIcons';
import { safeArea, tabbarHeight, isX } from '../../common/utils';
import { imgs } from '../../config/imgs';

const SCREEN = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0
  },
  viewContent: {
    flex: 1,
    justifyContent: 'center'
  },
  tabbarView: {
    flex: 1,
    justifyContent: 'center'
  },
  viewTab: {
    flex: 1,
    justifyContent: 'center'
  },
  viewIcon: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 2
  },
  next: {
    position: 'absolute',
    top: 20,
    alignSelf: 'center',
  },
  prew: {
    position: 'absolute',
    bottom: 20,
    alignSelf: 'center',
    backgroundColor: 'transparent'
  },
  icon: {
    width: 28,
    height: 28,
    marginBottom: 2
  },
  separator: {
    height: StyleSheet.hairlineWidth
  },
  absolute: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  }
});

class TabBar extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      page: true,
      show: true,
      viewRef: null
    }
  }

  imageLoaded() {
    this.setState({ viewRef: findNodeHandle(this.backgroundImage) });
  }

  getPadding = () => {
    let { orientation } = this.state;
    let paddingBottom = 0;

    if (orientation === 'portrait') {
      return paddingBottom + (isX ? safeArea.portrait.bottomInset : 0);
    } else {
      return paddingBottom;
    }
  }

  render() {
    let {
      backgroundColor,
      style,
      renderIcon,
      navigation,
      navigationState,
      getLabel,
      activeTintColor,
      inactiveTintColor,
      jumpToIndex,
      colorIconActive,
      colorIconInactive,
      titleStyle,
      titleSize,
      separator,
      bgSeparator,
      hasLabel
    } = this.props;

    const numberTab = navigationState.routes.length;

    const contentsView = (
      <View style={styles.tabbarView}>
        {
          navigationState.routes.map((route, id) => {
            const color = navigationState.index === id ? activeTintColor : inactiveTintColor;
            const isActive = navigationState.index === id;
            const label = getLabel({ route });
            const icon = route.routes[0].params.icon

            return (
              <MKButton
                onPress={() => {
                  if (navigationState.index !== id) {
                    navigation.navigate(route.routeName);
                  } else {
                    if (route.index > 0) {
                      for (let i = 0; i < route.index; i++) {
                        Actions.pop()
                      }
                    }
                  }
                }}
                style={styles.viewTab}
                activeOpacity={.8}
                key={route.routeName}
              >
                {
                  icon ? (
                    <View style={[styles.viewIcon]}>
                      <IconMaterial name={icon} size={isActive ? 40 : 36} color={isActive ? colorIconActive : colorIconInactive} />
                      {
                        hasLabel && <Text style={[{ color, fontSize: titleSize, marginBottom: 2, fontWeight: '500' }, titleStyle]}>{label}</Text>
                      }
                    </View>
                  ) : <Text style={[{ color, fontSize: titleSize, fontWeight: '500' }, titleStyle]}>{label}</Text>
                }
              </MKButton>
            )
          })
        }
      </View>
    )

    return (
      <View style={styles.container}>
        <View
          style={[
            {
              width: tabbarHeight,
              backgroundColor: 'transparent',
              flex: 1
            }
          ]}
        >
          <BlurView
            style={styles.absolute}
            blurType="dark"
            blurAmount={12}
            blurRadius={6}
          />
          <View style={{ flex: 1 }}>
            {contentsView}
          </View>
        </View>
      </View>
    );
  }
}

TabBar.defaultProps = {
  backgroundColor: 'steelblue',
  colorIconActive: 'white',
  colorIconInactive: 'rgba(0, 0, 0, 0.6)',
  titleSize: Platform.OS === "ios" ? 12 : 11,
  separator: false,
  bgSeparator: 'rgba(0, 0, 0, 0.6)',
  hasLabel: false
}

TabBar.propTypes = {
  backgroundColor: PropTypes.string,
  style: ViewPropTypes.style,
  colorIconActive: PropTypes.string,
  colorIconInactive: PropTypes.string,
  titleStyle: Text.propTypes.style,
  titleSize: PropTypes.number,
  separator: PropTypes.bool,
  bgSeparator: PropTypes.string,
  hasLabel: PropTypes.bool
}

export default TabBar;
