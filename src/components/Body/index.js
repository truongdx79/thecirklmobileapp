/**
* Created by bav on Sat Jul 14 2018
* Copyright (c) 2018 bav
*/

import Body from './Body';
import BodyLogin from './BodyLogin';

export {
  Body,
  BodyLogin
}
