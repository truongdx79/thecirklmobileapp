import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, ImageBackground, TouchableWithoutFeedback, Keyboard } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { imgs } from '../../config/imgs';
import { common } from '../../config/common';

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  overlay: {
    ...StyleSheet.absoluteFillObject
  }
});

class BodyLogin extends PureComponent {
  render() {
    let {
      gradient
    } = this.props;

    return (
      <ImageBackground
        source={imgs().backgroundImageLogin}
        resizeMode='cover'
        style={styles.container}
      >
        <View style={{ flex: 1 }}>
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <View style={{ flex: 1 }}>
              {this.props.children}
            </View>
          </TouchableWithoutFeedback>
        </View>
      </ImageBackground>
    );
  }
}

BodyLogin.defaultProps = {
  gradient: true
}

BodyLogin.propTypes = {
  gradient: PropTypes.bool
}

export default BodyLogin;
