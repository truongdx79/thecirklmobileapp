/**
* Created by bav on Sat Jul 14 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { isTablet, tabbarHeight, tabbarHeightContain } from '../../common/utils';
import { common } from '../../config/common';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    zIndex: 0,
    backgroundColor: '#E4E4E4',
  }
});

class Body extends PureComponent<Props> {
  render() {
    let {
      gradient,
      paddingBottom,
      paddingLeft
    } = this.props;

    // if (gradient) {
    //   return (
    //     <LinearGradient
    //       start={{ x: 0, y: 0.2 }}
    //       end={{ x: 0.5, y: 1 }}
    //       locations={[0, 0.6, 1]}
    //       colors={[
    //         common().BACKGROUND_GRADIENT1,
    //         common().BACKGROUND_GRADIENT2,
    //         common().BACKGROUND_GRADIENT2
    //       ]}
    //       style={[
    //         styles.container,
    //         isTablet ? { paddingLeft: paddingLeft ? tabbarHeightContain : 0 } : { paddingBottom: paddingBottom ? tabbarHeightContain : 0 }
    //       ]}
    //     >
    //       {this.props.children}
    //     </LinearGradient>
    //   );
    // }

    return (
      <View style={[styles.container, isTablet ? { paddingLeft: paddingLeft ? tabbarHeightContain : 0 } : { paddingBottom: paddingBottom ? tabbarHeightContain : 0 }]}>
        {this.props.children}
      </View>
    );
  }
}

Body.defaultProps = {
  gradient: true,
  paddingBottom: false,
  paddingLeft: true
}

Body.propTypes = {
  gradient: PropTypes.bool,
  paddingBottom: PropTypes.bool,
  paddingLeft: PropTypes.bool
}

interface Props {
  /**
   * Using linear gradient background color. Default true
   */
  gradient?: boolean;
  /**
   * If true, childrent will margin bottom = height tabbar. Default false
   */
  paddingBottom?: boolean;
  paddingLeft?: boolean;
}

export default Body;
