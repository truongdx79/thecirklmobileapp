/**
* Created by bav on Sat Sep 08 2018
* Copyright (c) 2018 bav@luci.vn
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text, Platform, TouchableWithoutFeedback, Dimensions } from 'react-native';
import Modal from 'react-native-modal';
import { BlurView } from 'react-native-blur';
import { common } from '../../config/common';

const SCREEN = Dimensions.get('window');

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    margin: 0,
    padding: 0
  },
  viewContent: {
    flex: 1,
    flexDirection: 'row'
  },
  viewLeft: {
    flex: 1,
  },
  viewRight: {
    
  },
  absolute: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  }
});

class ModalFromRight extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isOpen !== this.props.isOpen) {
      this.setState({
        showModal: nextProps.isOpen
      })
    }
  }

  _dismissModal = () => {
    this.hide()
  }

  show = () => {
    this.setState({
      showModal: true
    })
  }

  hide = () => {
    this.setState({
      showModal: false
    }, () => {
      this.props.onHide && this.props.onHide()
    })
  }

  render() {
    const {
      widthContent,
      backgroundColor,
      ...otherProps
    } = this.props;
    return (
      <Modal
        style={[styles.modal]}
        isVisible={this.state.showModal}
        hideModalContentWhileAnimating
        useNativeDriver
        animationIn='slideInRight'
        animationOut='slideOutRight'
        onBackdropPress={this.hide}
        animationOutTiming={200}
        // {...otherProps}
      >
        <View style={styles.viewContent}>
          <BlurView
            style={styles.absolute}
            blurType="light"
            blurAmount={Platform.OS === 'ios' ? 11 : 80}
            blurRadius={Platform.OS === 'ios' ? 3 : 20}
          />
          <TouchableWithoutFeedback onPress={this._dismissModal}>
            <View style={styles.viewLeft} />
          </TouchableWithoutFeedback>
          <View style={[styles.viewRight, { width: widthContent, backgroundColor }]}>
            {this.props.children}
          </View>
        </View>
      </Modal>
    );
  }
}

ModalFromRight.defaultProps = {
  widthContent: SCREEN.width / 2,
  backgroundColor: common().BACKGROUND_GRADIENT2
}

ModalFromRight.propTypes = {
  widthContent: PropTypes.number,
  backgroundColor: PropTypes.string
}

export default ModalFromRight;
