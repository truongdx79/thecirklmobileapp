/**
* Created by bav on Thu Jul 19 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text, Animated, Modal, Platform, Dimensions, ViewPropTypes, TouchableOpacity, findNodeHandle, TouchableWithoutFeedback } from 'react-native';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import { BlurView } from 'react-native-blur';
import { common } from '../../config/common';

const SCREEN = Dimensions.get('window');
const STATUS_BAR_OFFSET = (Platform.OS === 'android' ? -25 : 0);
const TOTAL_ANGLE = 90;
const BASE_ANGLE = 0;

const styles = StyleSheet.create({
  background: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: SCREEN.width,
    height: SCREEN.height,
  },
  open: {
    position: 'absolute'
  },
  btn: {
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center'
  },
  absolute: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  },
});

const CANCELBUTTON = Animated.createAnimatedComponent(TouchableOpacity);

class MoreMenu extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isAnimating: false,
      isOpen: this.props.isOpen || false,
      target: {
        x: 0,
        y: 0,
        opacity: 1,
      },
      openVal: new Animated.Value(0),
      scale: new Animated.Value(0)
    }
  }

  componentWillReceiveProps(props) {
    if (this.props.isOpen != props.isOpen && props.isOpen) {
      this.open();
    }
  }

  getDiamButton = () => {
    const { origin } = this.props;
    return Math.sqrt(origin.width * origin.width + origin.height * origin.height);
  }

  toRadians = (degrees) => {
    return degrees * (Math.PI / 180)
  }

  getTarget = () => {
    const { origin } = this.props;
    const target = {
      x: origin.x,
      y: origin.y,
      opacity: 1
    }

    return target
  }

  getTargetButton = (index) => {
    const {
      origin,
      widthButton,
      button
    } = this.props;
    const duongCheo = this.getDiamButton() - widthButton / 2 + 20;
    const SEPARATION_ANGLE = TOTAL_ANGLE / 3;
    const angle = BASE_ANGLE + ((3 - index) * SEPARATION_ANGLE);

    let target = {
      x: 0,
      y: 0
    }

    if (origin.x < SCREEN.width * 2 / 3 && origin.y < SCREEN.height / 3) {
      target.x = origin.x + duongCheo * Math.cos(this.toRadians(angle))
      target.y = origin.y + duongCheo * Math.sin(this.toRadians(angle))
    }

    if (origin.x < SCREEN.width * 2 / 3 && origin.y > SCREEN.height / 3) {
      target.x = origin.x + duongCheo * Math.cos(this.toRadians(angle))
      target.y = origin.y + origin.width - duongCheo * Math.sin(this.toRadians(angle)) - widthButton
    }

    if (origin.x > SCREEN.width * 2 / 3 && origin.y < SCREEN.height / 3) {
      target.x = origin.x + origin.height - duongCheo * Math.cos(this.toRadians(angle)) - widthButton
      target.y = origin.y + duongCheo * Math.sin(this.toRadians(angle))
    }

    if (origin.x > SCREEN.width * 2 / 3 && origin.y > SCREEN.height / 3) {
      target.x = origin.x + origin.height - duongCheo * Math.cos(this.toRadians(angle)) - widthButton
      target.y = origin.y + origin.width - duongCheo * Math.sin(this.toRadians(angle)) - widthButton
    }

    return target;
  }

  getTargetCancelButton = () => {
    const {
      origin,
      widthButton,
    } = this.props;
    let target = {
      x: 0,
      y: 0
    }

    if (origin.x < SCREEN.width * 2 / 3 && origin.y < SCREEN.height / 3) {
      target.x = origin.x
      target.y = origin.y
    }

    if (origin.x < SCREEN.width * 2 / 3 && origin.y > SCREEN.height / 3) {
      target.x = origin.x
      target.y = origin.y + origin.width - widthButton
    }

    if (origin.x > SCREEN.width * 2 / 3 && origin.y < SCREEN.height / 3) {
      target.x = origin.x + origin.height - widthButton
      target.y = origin.y
    }

    if (origin.x > SCREEN.width * 2 / 3 && origin.y > SCREEN.height / 3) {
      target.x = origin.x + origin.height - widthButton
      target.y = origin.y + origin.width - widthButton
    }

    return target;
  }

  open = () => {
    this.setState({
      isAnimating: true,
      isOpen: true,
      target: this.getTarget()
    });
    Animated.parallel([
      Animated.spring(this.state.scale, {
        toValue: 1, ...this.props.springConfig
      }),
      Animated.spring(this.state.openVal, {
        toValue: 1, ...this.props.springConfig
      })
    ]).start(() => {
      this.setState({ isAnimating: false });
      this.props.onOpen && this.props.onOpen()
    });
  }

  close = () => {
    this.setState({
      isAnimating: true
    });
    Animated.parallel([
      Animated.spring(this.state.scale, {
        toValue: 0, ...this.props.springConfig
      }),
      Animated.spring(this.state.openVal, {
        toValue: 0, ...this.props.springConfig
      })
    ]).start(() => {
      this.setState({
        isAnimating: false,
        isOpen: false,
      });

      this.props.onClose && this.props.onClose()
    })
  }

  renderButton = (btn, index) => {
    const {
      origin,
      widthButton,
      buttonBackgroundColor,
      buttonStyle
    } = this.props;

    const {
      scale,
      target,
      openVal
    } = this.state;

    const openStyle = [styles.open, {
      left: openVal.interpolate({ inputRange: [0, 1], outputRange: [origin.x, this.getTargetButton(index).x] }),
      top: openVal.interpolate({ inputRange: [0, 1], outputRange: [origin.y + STATUS_BAR_OFFSET, this.getTargetButton(index).y + STATUS_BAR_OFFSET] }),
      width: openVal.interpolate({ inputRange: [0, 1], outputRange: [origin.width, widthButton] }),
      height: openVal.interpolate({ inputRange: [0, 1], outputRange: [origin.height, widthButton] }),
      transform: [
        {
          scale: scale
        }
      ]
    }];

    const COMPONENT = btn.onPress ? Animated.createAnimatedComponent(TouchableOpacity) : Animated.View

    return (
      <COMPONENT
        key={index}
        style={[
          openStyle,
          styles.btn,
          buttonStyle,
          {
            width: widthButton,
            height: widthButton,
            borderRadius: widthButton / 2,
            backgroundColor: buttonBackgroundColor
          }
        ]}
        onPress={() => {
          this.close()
          btn.onPress && btn.onPress(btn, index)
        }}
      >
        {
          btn.iconName && <Icon name={btn.iconName} size={24} color={'white'} />
        }
      </COMPONENT>
    )
  }

  renderCancelButton = () => {
    const {
      origin,
      widthButton,
      cancelButtonIconName,
      cancelButtonStyle,
      cancelButtonBackgroundColor
    } = this.props;

    const {
      openVal,
      scale
    } = this.state;

    const openStyle = [styles.open, {
      left: openVal.interpolate({ inputRange: [0, 1], outputRange: [origin.x, this.getTargetCancelButton().x] }),
      top: openVal.interpolate({ inputRange: [0, 1], outputRange: [origin.y + STATUS_BAR_OFFSET, this.getTargetCancelButton().y + STATUS_BAR_OFFSET] }),
      width: openVal.interpolate({ inputRange: [0, 1], outputRange: [origin.width, widthButton] }),
      height: openVal.interpolate({ inputRange: [0, 1], outputRange: [origin.height, widthButton] })
    }];

    return (
      <CANCELBUTTON
        style={[
          openStyle,
          styles.btn,
          cancelButtonStyle,
          {
            width: widthButton,
            height: widthButton,
            borderRadius: widthButton / 2,
            backgroundColor: cancelButtonBackgroundColor,
            opacity: openVal.interpolate({
              inputRange: [0, 0.4, 1],
              outputRange: [0, 0, 1]
            })
          },
          Platform.OS == 'ios' ? {
            transform: [
              {
                scale: scale
              },
              {
                rotate: openVal.interpolate({
                  inputRange: [0, 1],
                  outputRange: ['0deg', '90deg']
                })
              }
            ]
          } : {
              transform: [
                {
                  scale: scale
                }
              ]
            }
        ]}
        onPress={() => this.close()}
      >
        <Icon name={cancelButtonIconName} size={24} color={'white'} />
      </CANCELBUTTON>
    )
  }

  render() {
    const {
      origin,
      widthButton,
      buttonBackgroundColor,
      backgroundOverlay,
      button,
      cancelButton
    } = this.props;

    const {
      isOpen,
      isAnimating,
      openVal,
      scale,
      target,
    } = this.state;

    const modalOpacityStyle = {
      opacity: openVal.interpolate({ inputRange: [0, 1], outputRange: [0, target.opacity] })
    };

    const background = (
      <TouchableWithoutFeedback onPress={() => this.close()}>
        <Animated.View
          style={[
            styles.background,
            { backgroundColor: backgroundOverlay },
            modalOpacityStyle
          ]}
        >
          <BlurView
            style={styles.absolute}
            blurType="light"
            blurAmount={Platform.OS === 'ios' ? 11 : 80}
            blurRadius={Platform.OS === 'ios' ? 3 : 20}
          />
          <View style={[styles.absolute, { backgroundColor: 'rgba(0, 0, 0, 0.4)' }]} />
        </Animated.View>
      </TouchableWithoutFeedback>
    );

    return (
      <Modal visible={isOpen} transparent={true} onRequestClose={() => this.close()}>
        {background}
        {
          button.map((btn, index) => {
            return this.renderButton(btn, index)
          })
        }
        {/* {cancelButton && this.renderCancelButton()} */}
      </Modal>
    );
  }
}

MoreMenu.defaultProps = {
  springConfig: { tension: 30, friction: 7 },
  backgroundOverlay: Platform.OS == 'ios' ? 'transparent' : 'rgba(88, 92, 124, 0.8)',
  isOpen: false,
  widthButton: 60,
  buttonBackgroundColor: '#242843',
  button: [
    {
      iconName: 'eject',
      onPress: () => { }
    },
    {
      iconName: 'info',
      onPress: () => { }
    },
    {
      iconName: 'history',
      onPress: () => { }
    },
    {
      iconName: 'today',
      onPress: () => { }
    },
  ],
  cancelButton: true,
  cancelButtonIconName: 'clear',
  cancelButtonBackgroundColor: '#242843'
}

MoreMenu.propTypes = {
  origin: PropTypes.shape({
    x: PropTypes.number,
    y: PropTypes.number,
    width: PropTypes.number,
    height: PropTypes.number,
  }),
  springConfig: PropTypes.shape({
    tension: PropTypes.number,
    friction: PropTypes.number,
  }),
  isOpen: PropTypes.bool,
  onOpen: PropTypes.func,
  onClose: PropTypes.func,
  backgroundOverlay: PropTypes.string,
  widthButton: PropTypes.number,
  buttonBackgroundColor: PropTypes.string,
  buttonStyle: ViewPropTypes.style,
  button: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string,
      iconName: PropTypes.string,
      onPress: PropTypes.func
    })
  ),
  cancelButton: PropTypes.bool,
  cancelButtonIconName: PropTypes.string,
  cancelButtonStyle: ViewPropTypes.style,
  cancelButtonBackgroundColor: PropTypes.string
}

export default MoreMenu;
