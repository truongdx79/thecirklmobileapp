/**
* Created by bav on Thu Jul 19 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet, View, Text, ViewPropTypes, Platform, TouchableOpacity,
  ActivityIndicator, Animated, Dimensions
} from 'react-native';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import { common } from '../../config/common';

const SCREEN = Dimensions.get('window');
const isIOS = Platform.OS === 'ios';
const styles = StyleSheet.create({
  container: {
    overflow: 'hidden'
  },
  shadow: {
    elevation: 2,
    shadowOffset: {
      width: 2,
      height: 2
    },
    shadowRadius: 4,
    shadowOpacity: 0.4
  },
  viewContent: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 12,
    overflow: 'hidden'
  },
  indicator: {
    alignSelf: 'center'
  },
  title: {
    fontSize: 15,
    marginLeft: 8
  },
  center: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  firstBorder: {
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10
  },
  lastBorder: {
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10
  },
  borderRadius: {
    borderRadius: 10
  }
});

class SceneCustom extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isActive: false,
      loading: false,
      isEdit: this.props.selectMode ? true : false
    }
    this.scale = new Animated.Value(1)
  }

  componentWillUnmount() {
    if (this.timeoutLoading) {
      clearTimeout(this.timeoutLoading)
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.selectMode !== nextProps.selectMode) {
      if (nextProps.selectMode) {
        this.setState({ isEdit: true });
      } else {
        this.setState({ isEdit: false });
      }
    }
  }

  showLoading = () => {
    this.setState({
      loading: true
    })
  }

  hideLoading = () => {
    if (this.timeoutLoading) {
      clearTimeout(this.timeoutLoading)
    }
    this.setState({
      loading: false
    })
  }

  onPressButton = () => {
    this.props.onPress && this.props.onPress()
    if (this.props.isControl && !this.state.isEdit) {
      this.setState({
        loading: true
      })
      if (this.timeoutLoading) {
        clearTimeout(this.timeoutLoading)
      }

      this.timeoutLoading = setTimeout(() => {
        this.setState({
          loading: false
        })
      }, this.props.timeoutLoading)

    } else {
      this.setState(preState => {
        let isActive = !preState.isActive;
        this.props.onChange && this.props.onChange(isActive)
        return { isActive }
      })
    }
  }

  onPressIn = () => {
    this.scaleAnimated = Animated.spring(this.scale, {
      toValue: isIOS ? 1.3 : 1.1, tension: 200, friction: 7
    }).start()
  }

  onPressOut = () => {
    if (this.scaleAnimated) {
      this.scaleAnimated.stop()
    }

    Animated.spring(this.scale, {
      toValue: 1, tension: 200, friction: 7
    }).start()
  }

  render() {
    let {
      style,
      width,
      height,
      backgroundColorActive,
      backgroundColorInactive,
      iconName,
      iconColorActive,
      iconColorInactive,
      title,
      titleColorActive,
      titleColorInactive,
      onChange,
      isControl,
      state,
      firstBorder,
      lastBorder,
      borderRadius,
      selectMode
    } = this.props;
    const { loading, isEdit } = this.state;
    let isActive = this.state.isActive;

    if (isControl) {
      isActive = false
    }

    const ComponentButton = loading ? Animated.View : Animated.createAnimatedComponent(TouchableOpacity)

    return (
      <ComponentButton
        activeOpacity={0.6}
        style={[
          styles.container,
          style,
          firstBorder ? styles.firstBorder : undefined,
          lastBorder ? styles.lastBorder : undefined,
          borderRadius ? styles.borderRadius : undefined,
          {
            width,
            height,
            backgroundColor: isActive ? backgroundColorActive : backgroundColorInactive,
            shadowColor: isActive ? backgroundColorActive : 'black',
            transform: [
              {
                scale: this.scale
              }
            ]
          },
          loading ? styles.center : undefined
        ]}
        onPress={this.onPressButton}
        onPressIn={this.onPressIn}
        onPressOut={this.onPressOut}
      >
        {
          loading ? <ActivityIndicator style={styles.indicator} size='small' /> : (
            <View style={styles.viewContent}>
              <Icon name={isEdit ? 'menu' : iconName} size={18} color={isActive ? iconColorActive : iconColorInactive} />
              <Text style={[styles.title, { color: isActive ? titleColorActive : titleColorInactive }]}>
                {title}
              </Text>
            </View>
          )
        }
      </ComponentButton>
    );
  }
}

SceneCustom.defaultProps = {
  width: SCREEN.width - 32,
  height: 55,
  backgroundColorActive: common().DEVICE_BACKGROUND_COLOR_ACTIVE,
  backgroundColorInactive: 'rgba(255, 255, 255, 0.1)',
  iconColorActive: common().DEVICE_ICON_COLOR_ACTIVE,
  iconColorInactive: 'white',
  titleColorActive: common().DEVICE_TEXT_COLOR_ACTIVE,
  titleColorInactive: 'white',
  isControl: false,
  timeoutLoading: 1000,
  firstBorder: false,
  lastBorder: false,
  borderRadius: false,
  selectMode: false
}

SceneCustom.propTypes = {
  style: ViewPropTypes.style,
  width: PropTypes.number,
  height: PropTypes.number,
  backgroundColorActive: PropTypes.string,
  backgroundColorInactive: PropTypes.string,
  iconName: PropTypes.string,
  iconColorActive: PropTypes.string,
  iconColorInactive: PropTypes.string,
  title: PropTypes.string.isRequired,
  titleColorActive: PropTypes.string,
  titleColorInactive: PropTypes.string,
  onChange: PropTypes.func,
  isControl: PropTypes.bool,
  timeoutLoading: PropTypes.number,
  onPress: PropTypes.func,
  firstBorder: PropTypes.bool,
  lastBorder: PropTypes.bool,
  borderRadius: PropTypes.bool,
  selectMode: PropTypes.bool
}

export default SceneCustom;
