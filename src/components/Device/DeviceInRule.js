/**
* Created by bav on Thu Aug 23 2018
* Copyright (c) 2018 bav@luci.vn
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text, ViewPropTypes, Animated, TouchableOpacity } from 'react-native';
import { MKButton } from 'react-native-material-kit';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import { common } from '../../config/common';

const styles = StyleSheet.create({
  container: {
    borderRadius: 10,
    overflow: 'hidden',
    padding: 8
  },
  viewTop: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  viewBottom: {
    flex: 1,
    justifyContent: 'flex-end'
  },
  viewState: {
    fontSize: 14,
    marginRight: 4
  },
  title: {
    fontSize: 14
  },
});

class DeviceInRule extends PureComponent<Props> {
  constructor(props) {
    super(props);

    this.scale = new Animated.Value(1);
  }

  onPressIn = () => {
    this.scaleAnimated = Animated.spring(this.scale, {
      toValue: 1.3, tension: 200, friction: 7
    }).start()
  }

  onPressOut = () => {
    if (this.scaleAnimated) {
      this.scaleAnimated.stop()
    }

    Animated.spring(this.scale, {
      toValue: 1, tension: 200, friction: 7
    }).start()
  }

  onPress = () => {
    this.props.onPress && this.props.onPress()
  }

  render() {
    const {
      width,
      backgroundColor,
      iconColor,
      titleColor,
      iconName,
      deviceName,
      roomName,
      rightTitle,
      materialButton,
      style,
      delayLongPress,
      onLongPress
    } = this.props;
    const ComponentButton = Animated.createAnimatedComponent(materialButton ? MKButton : TouchableOpacity)

    return (
      <ComponentButton
        style={[
          styles.container,
          style,
          {
            width,
            height: width,
            backgroundColor,
            transform: [
              {
                scale: this.scale
              }
            ]
          },
        ]}
        onPress={this.onPress}
        delayLongPress={delayLongPress}
        onLongPress={onLongPress}
        onPressIn={this.onPressIn}
        onPressOut={this.onPressOut}
      >
        <View style={styles.viewTop}>
          <Icon
            name={iconName}
            size={36}
            color={iconColor}
          />
          {
            <Text style={[styles.viewState, { color: titleColor }]}>
              {rightTitle}
            </Text>
          }
        </View>
        <View style={styles.viewBottom}>
          <Text
            numberOfLines={1}
            style={[
              styles.title,
              { color: titleColor }
            ]}
          >
            {deviceName}
          </Text>
          <Text
            numberOfLines={1}
            style={[
              styles.title,
              { color: titleColor }
            ]}
          >
            {roomName}
          </Text>
        </View>
      </ComponentButton>
    );
  }
}

DeviceInRule.defaultProps = {
  width: 108,
  backgroundColor: common().DEVICE_BACKGROUND_COLOR_INACTIVE,
  iconColor: common().DEVICE_ICON_COLOR_INACTIVE,
  titleColor: common().DEVICE_TEXT_COLOR_INACTIVE,
  materialButton: true,
  delayLongPress: 1000,
}

DeviceInRule.propTypes = {
  width: PropTypes.number,
  iconName: PropTypes.string,
  deviceName: PropTypes.string,
  roomName: PropTypes.string,
  rightTitle: PropTypes.string,
  backgroundColor: PropTypes.string,
  iconColor: PropTypes.string,
  titleColor: PropTypes.string,
  materialButton: PropTypes.bool,
  onPress: PropTypes.func,
  style: ViewPropTypes.style,
  delayLongPress: PropTypes.number,
  onLongPress: PropTypes.func
}

interface Props {
  width?: number;
  iconName?: string;
  deviceName?: string;
  roomName?: string;
  rightTitle?: string;
  backgroundColor?: string;
  iconColor?: string;
  titleColor?: string;
  materialButton?: boolean;
  onPress?: () => void;
  style?: StyleProp<ViewStyle>;
}

export default DeviceInRule;
