/**
* Created by bav on Wed Aug 01 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View } from 'react-native';
import { G, Circle, Text, Line } from 'react-native-svg';
import range from 'lodash.range';

const styles = StyleSheet.create({
  container: {

  }
});

class CircleFace extends PureComponent {
  getFaceRadius = (faceRadius, i) => {

  }

  render() {
    const { r, stroke, stop } = this.props;
    const faceRadius = r + 8;
    const currentStop = stop.toAngle * 90 / Math.PI - 45

    return (
      <G>
        {
          range(180).map(i => {
            const cos = Math.cos(2 * Math.PI / 180 * i);
            const sin = Math.sin(2 * Math.PI / 180 * i);

            return (
              <Line
                key={i}
                stroke={'steelblue'}
                strokeWidth={1}
                x1={cos * ((Math.abs(i - currentStop) < 10) ? faceRadius + 1.4 * (9 - Math.abs(currentStop - i)) : faceRadius)}
                y1={sin * ((Math.abs(i - currentStop) < 10) ? faceRadius + 1.4 * (9 - Math.abs(currentStop - i)) : faceRadius)}
                x2={cos * (faceRadius - 24)}
                y2={sin * (faceRadius - 24)}
              />
            );
          })
        }
      </G>
    );
  }
}

CircleFace.defaultProps = {

}

CircleFace.propTypes = {

}

export default CircleFace;
