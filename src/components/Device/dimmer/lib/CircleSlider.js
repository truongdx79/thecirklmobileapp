/**
* Created by bav on Sun Jun 24 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, PanResponder, View, Animated, Image } from 'react-native';
import Svg, { Circle, G, LinearGradient, Path, Defs, Stop } from 'react-native-svg';
import range from 'lodash.range';
import { interpolateHcl as interpolateGradient } from 'd3-interpolate';
import extractBrush from 'react-native-svg/lib/extract/extractBrush';
import CircleFace from './CircleFace';

const AnimatedComponent = Animated.createAnimatedComponent(Circle);

function calculateArcColor(index0, segments, gradientColorFrom, gradientColorTo) {
  const interpolate = interpolateGradient(gradientColorFrom, gradientColorTo);

  return {
    fromColor: interpolate(index0 / segments),
    toColor: interpolate((index0 + 1) / segments),
  }
}

function calculateArcCircle(index0, segments, radius, startAngle0 = 0, angleLength0 = 2 * Math.PI) {
  const startAngle = startAngle0 % (2 * Math.PI);
  const angleLength = angleLength0 % (2 * Math.PI);
  const index = index0 + 1;
  const fromAngle = angleLength / segments * (index - 1) + startAngle;
  const toAngle = angleLength / segments * index + startAngle;
  const fromX = radius * Math.sin(fromAngle);
  const fromY = -radius * Math.cos(fromAngle);
  const realToX = radius * Math.sin(toAngle);
  const realToY = -radius * Math.cos(toAngle);
  const toX = radius * Math.sin(toAngle + 0.005);
  const toY = -radius * Math.cos(toAngle + 0.005);

  return {
    fromX,
    fromY,
    toX,
    toY,
    realToX,
    realToY,
    toAngle
  };
}

function getGradientId(index) {
  return `gradient${index}`;
}

export default class CircleSlider extends PureComponent {

  state = {
    circleCenterX: false,
    circleCenterY: false,
  }

  static propTypes = {
    fill: PropTypes.string,
    path: PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);
    this.lastFill = this.props.fill;
    this.scale = new Animated.Value(0);
    this.state = {
      fillValue: new Animated.Value(0),
    };
    this.state.fillValue.addListener(() => {
      const { fill, angleLength, startAngle } = this.props;
      const { fillValue } = this.state;
      const fillColor = startAngle + 1;
      const fillPartition = fillColor / 7;
      const color = fillValue.interpolate({
        inputRange: [fillColor - fillColor, fillColor - (fillPartition * 6), fillColor - (fillPartition * 5), fillColor - (fillPartition * 4), fillColor - (fillPartition * 3), fillColor - (fillPartition * 2), fillColor - (fillPartition * 1), fillColor],
        outputRange: ['rgb(0,0,0)', 'rgb(255,0,0)', 'rgb(0,255,0)', 'rgb(0,0,255)', 'rgb(255,0,255)', 'rgb(0,255,255)', 'rgb(255,255,0)', 'rgb(255,255,255)'],
      });
      // this.path.setNativeProps({
      //   fill: extractBrush(color.__getAnimatedValue()),
      // });
    });
  }

  animate = () => {
    const { fillValue } = this.state;
    const { angleLength } = this.props;
    Animated.spring(fillValue, {
      toValue: angleLength,
    }).start();
  };

  componentWillMount() {
    this._sleepPanResponder = PanResponder.create({
      onMoveShouldSetPanResponder: (evt, gestureState) => true,
      onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,
      onPanResponderGrant: (evt, gestureState) => this.setCircleCenter(),
      onPanResponderMove: (evt, { moveX, moveY }) => {
        const { circleCenterX, circleCenterY } = this.state;
        const { angleLength, startAngle, onUpdate } = this.props;

        const currentAngleStop = (startAngle + angleLength) % (2 * Math.PI);
        let newAngle = Math.atan2(moveY - circleCenterY, moveX - circleCenterX) + Math.PI / 2;

        if (newAngle < 0) {
          newAngle += 2 * Math.PI;
        }

        let newAngleLength = currentAngleStop - newAngle;

        if (newAngleLength < 0) {
          newAngleLength += 2 * Math.PI;
        }

        onUpdate({ startAngle: newAngle, angleLength: newAngleLength % (2 * Math.PI) });
      }
    });

    this._wakePanResponder = PanResponder.create({
      onMoveShouldSetPanResponder: (evt, gestureState) => true,
      onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,
      onPanResponderGrant: (evt, gestureState) => {
        Animated.timing(this.scale, {
          toValue: 1,
          duration: 100
        }).start()

        this.setCircleCenter()
      },
      onPanResponderMove: (evt, { moveX, moveY }) => {
        const { circleCenterX, circleCenterY } = this.state;
        const { angleLength, startAngle, onUpdate } = this.props;

        let newAngle = Math.atan2(moveY - circleCenterY, moveX - circleCenterX) + Math.PI / 2;
        let newAngleLength = (newAngle - startAngle) % (2 * Math.PI);

        if (newAngleLength < 0) {
          newAngleLength += 2 * Math.PI;
        }

        onUpdate({ startAngle, angleLength: newAngleLength });
      },
      onPanResponderRelease: () => {
        Animated.spring(this.scale, {
          toValue: 0,
          tension: 30, friction: 7
        }).start()
      }
    });
  }

  componentDidUpdate() {
    this.animate();
  }

  setNativeProps = (props) => {
    this._component && this._component.setNativeProps(props);
  }

  onLayout = () => {
    this.setCircleCenter();
  }

  setCircleCenter = () => {
    this._circle.measure((x, y, w, h, px, py) => {
      const halfOfContainer = this.getContainerWidth() / 2;
      this.setState({ circleCenterX: px + halfOfContainer, circleCenterY: py + halfOfContainer });
    });
  }

  getContainerWidth() {
    const { strokeWidth, radius } = this.props;
    return strokeWidth + radius * 2 + 72;
  }

  render() {
    const { startAngle, angleLength, segments, strokeWidth, radius, gradientColorFrom, gradientColorTo, bgCircleColor,
      showClockFace, clockFaceColor, startIcon, stopIcon, IconColor, showCircleFace } = this.props;
    const containerWidth = this.getContainerWidth();
    const start = calculateArcCircle(0, segments, radius, startAngle, angleLength);
    const stop = calculateArcCircle(segments - 1, segments, radius, startAngle, angleLength);
    return (
      <View style={{ width: containerWidth, height: containerWidth }} onLayout={this.onLayout}>
        <Svg
          height={containerWidth}
          width={containerWidth}
          ref={circle => this._circle = circle}
        >
          <Defs>
            {
              range(segments).map(i => {
                const { fromX, fromY, toX, toY } = calculateArcCircle(i, segments, radius, startAngle, angleLength);
                const { fromColor, toColor } = calculateArcColor(i, segments, gradientColorFrom, gradientColorTo)
                return (
                  <LinearGradient key={i} id={getGradientId(i)} x1={fromX.toFixed(2)} y1={fromY.toFixed(2)} x2={toX.toFixed(2)} y2={toY.toFixed(2)}>
                    <Stop offset="0%" stopColor={fromColor} />
                    <Stop offset="1" stopColor={toColor} />
                  </LinearGradient>
                )
              })
            }
          </Defs>
          <G
            x={strokeWidth / 2 + radius + 36}
            y={strokeWidth / 2 + radius + 36}
          >
            <Circle
              r={radius}
              strokeWidth={strokeWidth}
              ref={ref => (this.path = ref)}
              // fill={this.state.lastFill}
              fill={'transparent'}
              stroke={bgCircleColor}
            />

            {
              showCircleFace && (
                <CircleFace
                  r={radius + strokeWidth}
                  stroke={clockFaceColor}
                  stop={stop}
                />
              )
            }

            {
              range(segments).map(i => {
                const { fromX, fromY, toX, toY } = calculateArcCircle(i, segments, radius, startAngle, angleLength);
                const d = `M ${fromX.toFixed(2)} ${fromY.toFixed(2)} A ${radius} ${radius} 0 0 1 ${toX.toFixed(2)} ${toY.toFixed(2)}`;
                return (
                  <Path
                    d={d}
                    key={i}
                    strokeWidth={strokeWidth - 10}
                    stroke={`url(#${getGradientId(i)})`}
                    fill="transparent"
                  />
                )
              })
            }

            <G
              fill={gradientColorTo}
              x={stop.toX}
              y={stop.toY}
            // transform={{ translateX: `${stop.toX}`, translateY: `${stop.toY}` }}
            // onPressIn={() => this.setState({ angleLength: angleLength + Math.PI / 2 })}

            >
              <AnimatedComponent
                r={this.scale.interpolate({
                  inputRange: [0, 1],
                  outputRange: [`${(strokeWidth - 11) / 2}`, `${(strokeWidth - 11) / 2 + 4}`]
                })}
                fill={IconColor}
                stroke={gradientColorTo}
                strokeWidth="1"
                {...this._wakePanResponder.panHandlers}
              />
              {
                stopIcon
              }
            </G>

            <G
              fill={gradientColorFrom}
              x={start.fromX}
              y={start.fromY}
              transform={{ translateX: `${start.fromX}`, translateY: `${start.fromY}` }}
            // onPressIn={() => this.setState({ startAngle: startAngle - Math.PI / 2, angleLength: angleLength + Math.PI / 2 })}

            >
              <Circle
                r={(strokeWidth - 1) / 2}
                fill={bgCircleColor}
                stroke={gradientColorFrom}
                strokeWidth="1"
                {...this._sleepPanResponder.panHandlers}
              />
              {
                startIcon
              }
            </G>
          </G>
        </Svg>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  box: {
    width: 100,
    height: 100,
  },
  image: {
    width: 500,
    height: 500,
  }
});