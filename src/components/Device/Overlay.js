/**
* Created by bav on Tue Jul 31 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet, View, findNodeHandle, TouchableWithoutFeedback,
  Modal, Animated, Easing, Dimensions, Platform, ViewPropTypes
} from 'react-native';
import { BlurView } from 'react-native-blur';
import { common } from '../../config/common';

const SCREEN = Dimensions.get('window');
const STATUS_BAR_OFFSET = (Platform.OS === 'android' ? -25 : 0);

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  background: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  absolute: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  },
  content: {
    justifyContent: 'center',
    alignItems: 'center'
  }
});

class Overlay extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isAnimating: false,
      isOpen: this.props.isOpen || false,
      target: {
        x: 0,
        y: 0,
        opacity: 0.3,
      },
      openVal: new Animated.Value(0),
      scale: new Animated.Value(0)
    }
  }

  open = () => {
    this.setState({
      isAnimating: true,
      isOpen: true,
      target: {
        x: 0,
        y: 0,
        opacity: 1,
      }
    });
    Animated.parallel([
      Animated.spring(this.state.scale, {
        toValue: 1, ...this.props.springConfig
      }),
      Animated.spring(this.state.openVal, {
        toValue: 1, ...this.props.springConfig
      })
    ]).start(() => {
      this.setState({ isAnimating: false });
      this.props.onOpen && this.props.onOpen()
    });
  }

  close = () => {
    this.setState({
      isAnimating: true
    });
    this.props.onCloseOverlay && this.props.onCloseOverlay()
    Animated.parallel([
      Animated.spring(this.state.scale, {
        toValue: 0, ...this.props.springConfig
      }),
      Animated.spring(this.state.openVal, {
        toValue: 0, ...this.props.springConfig
      })
    ]).start(() => {
      this.setState({
        isAnimating: false,
        isOpen: false,
      });
      this.props.onClose && this.props.onClose()
    })
  }

  componentWillReceiveProps(props) {
    if (this.props.isOpen != props.isOpen && props.isOpen) {
      this.open();
    }
  }

  onPressBackground = () => {
    this.props.touchToDismiss && this.close()
  }

  render() {
    const {
      origin,
      backgroundOverlay,
      style,
      blurOverlay,
      renderHeader,
      renderFooter
    } = this.props;

    const {
      isOpen,
      isAnimating,
      openVal,
      scale,
      target,
    } = this.state;

    const lightboxOpacityStyle = {
      opacity: openVal.interpolate({ inputRange: [0, 1], outputRange: [0.1, target.opacity] })
    }

    const openStyle = [styles.open, {
      left: openVal.interpolate({ inputRange: [0, 1], outputRange: [origin.x, target.x] }),
      top: openVal.interpolate({ inputRange: [0, 1], outputRange: [origin.y + STATUS_BAR_OFFSET, target.y + STATUS_BAR_OFFSET] }),
      width: openVal.interpolate({ inputRange: [0, 1], outputRange: [origin.width, SCREEN.width] }),
      height: openVal.interpolate({ inputRange: [0, 1], outputRange: [origin.height, SCREEN.height] }),
      transform: [
        {
          scale: scale
        }
      ]
    }];

    const background = (
      <Animated.View
        style={[
          styles.background,
          { backgroundColor: backgroundOverlay },
          lightboxOpacityStyle
        ]}
      >
        <BlurView
          style={styles.absolute}
          blurType="light"
          blurAmount={Platform.OS === 'ios' ? 11 : 80}
          blurRadius={Platform.OS === 'ios' ? 3 : 20}
        />
        <View style={[styles.absolute, { backgroundColor: 'rgba(0, 0, 0, 0.4)' }]} />
      </Animated.View>
    );

    const content = (
      <TouchableWithoutFeedback
        onPress={this.onPressBackground}
      >
        <Animated.View
          style={[
            openStyle,
            lightboxOpacityStyle,
            styles.content
          ]}
        >

          {
            renderHeader && renderHeader()
          }
          {this.props.children}
          {
            renderFooter && renderFooter()
          }
        </Animated.View>
      </TouchableWithoutFeedback>
    )

    return (
      <Modal visible={isOpen} transparent={true} onRequestClose={() => this.close()} >
        {background}
        {content}
      </Modal>
    );
  }
}

Overlay.defaultProps = {
  origin: {
    x: SCREEN.width / 2,
    y: SCREEN.height / 2,
    width: 0,
    height: 0
  },
  springConfig: { tension: 30, friction: 7 },
  isOpen: false,
  touchToDismiss: true,
  backgroundOverlay: Platform.OS == 'ios' ? 'transparent' : 'rgba(88, 92, 124, 0.9)',
  blurOverlay: true
}

Overlay.propTypes = {
  origin: PropTypes.shape({
    x: PropTypes.number,
    y: PropTypes.number,
    width: PropTypes.number,
    height: PropTypes.number,
  }),
  springConfig: PropTypes.shape({
    tension: PropTypes.number,
    friction: PropTypes.number,
  }),
  isOpen: PropTypes.bool,
  backgroundOverlay: PropTypes.string,
  blurOverlay: PropTypes.bool,
  touchToDismiss: PropTypes.bool,
  style: ViewPropTypes.style,
  renderHeader: PropTypes.func,
  renderFooter: PropTypes.func,
  onOpen: PropTypes.func,
  onClose: PropTypes.func
}

export default Overlay;
