/**
* Created by bav on Wed Aug 15 2018
* Copyright (c) 2018 bav@luci.vn
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text, Dimensions, TouchableOpacity, LayoutAnimation } from 'react-native';
import { MKSlider } from 'react-native-material-kit';
import colorsys from 'colorsys';
import ColorWheel from '../ColorWheel';
import { CustomLayoutSpring } from '../../common/animation';
import { common } from '../../config/common';
import { getColorFromLevel, getLevelFromColor } from '../../common/device';
import Icon from '../Icon';

const SCREEN = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  viewSelectColor: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 25
  },
  button: {
    width: 45,
    height: 45,
    borderRadius: 45 / 2,
    borderWidth: 3,
    borderColor: 'rgba(255, 255, 255, 0.1)',
    backgroundColor: 'black'
  },
  btnColor: {
    width: 39,
    height: 39,
    borderRadius: 39 / 2,
  },
  viewBtn: {
    width: 48,
    height: 48,
    borderRadius: 48 / 2
  },
  btnTurnOff: {
    width: 45,
    height: 45,
    borderRadius: 45 / 2,
    borderWidth: 3,
    borderColor: 'rgba(255, 255, 255, 0.1)',
    backgroundColor: 'black',
    justifyContent: 'center',
    alignItems: 'center'
  },
  circle: {
    backgroundColor: common().CIRCLE_BACKGROUND_COLOR,
    elevation: 4,
    shadowRadius: 20,
    shadowOpacity: 0.6,
    shadowColor: common().BACKGROUND_COLOR_BUTTON_ACTIVE,
    marginBottom: 70
  },
  colorWhell: {
    position: 'absolute'
  },
  viewSlider: {
    position: 'absolute',
    bottom: 48
  },
  thumb: {
    width: 36,
    height: 36,
    borderRadius: 18
  },
  viewTextSlider: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: 4,
    marginBottom: 10
  },
  title: {
    color: common().COLOR_TEXT_NORMAL,
    fontSize: common().FONT_SIZE_TITLE
  }
});

const COLOR = ['#ff0000', '#ffff00', '#00ff00', '#0000ff', '#00ffff']

const Button = ({ color, onPress }) => {
  return (
    <TouchableOpacity
      style={[
        styles.button
      ]}
      onPress={onPress}
    >
      <View style={[styles.btnColor, { backgroundColor: color, flex: 1 }]} />
    </TouchableOpacity>
  )
}

class RGB extends PureComponent {
  constructor(props) {
    super(props);
    const defaultVal = {
      state: 'off',
      rgb: '00:00:00'
    }

    this.state = {
      showMoreColor: false,
      val: props.val || defaultVal,
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.val !== this.props.val) {
      this.setState({
        val: nextProps.val
      })
    }
  }

  onChangeColorCircle = (hsv) => {
    let color = colorsys.hsv2Hex(hsv)
    this.setState(prewState => {
      let val = prewState.val;
      if (color === '#000000') {
        val = {
          state: 'off',
          rgb: '00:00:00'
        }
      } else {
        val = {
          state: 'on',
          rgb: getLevelFromColor(color)
        }
      }

      this.props.onChangColor && this.props.onChangColor(val)

      return {
        val
      }
    })
  }

  onTurnOff = () => {
    const defaultVal = {
      state: 'off',
      rgb: '00:00:00'
    }

    this.props.onChangColor && this.props.onChangColor(defaultVal)
    this.setState({
      val: defaultVal
    })
  }

  onChangeSelectColor = (color) => {
    LayoutAnimation.configureNext(CustomLayoutSpring);
    if (this.state.showMoreColor) {
      this.setState(prewState => {
        let val = prewState.val;
        if (color === '#000000') {
          val = {
            state: 'off',
            rgb: '00:00:00'
          }
        } else {
          val = {
            state: 'on',
            rgb: getLevelFromColor(color)
          }
        }

        this.props.onChangColor && this.props.onChangColor(val)
        return {
          val
        }
      })
    }

    this.setState({
      showMoreColor: !this.state.showMoreColor
    })
  }

  onChangeSlider = (value) => {
    const { val } = this.state;
    let hsv = colorsys.hex2Hsv(getColorFromLevel(val.rgb))
    hsv.v = Math.round(value);
    let color = colorsys.hsv2Hex(hsv)
    this.setState(prewState => {
      let val = prewState.val;
      if (color === '#000000') {
        val = {
          state: 'off',
          rgb: '00:00:00'
        }
      } else {
        val = {
          state: 'on',
          rgb: getLevelFromColor(color)
        }
      }

      this.props.onChangColor && this.props.onChangColor(val)

      return {
        val
      }
    })
  }

  render() {
    const {
      statusTitle,
      circleRadius
    } = this.props;
    const { showMoreColor, val } = this.state;
    const numberBrightness = colorsys.hex2Hsv(getColorFromLevel(val.rgb)).v;

    return (
      <View style={styles.container}>
        <View style={[styles.viewSelectColor, { width: circleRadius }]}>
          {
            COLOR.map((cl, index) => {
              if (showMoreColor) {
                return (
                  <Button
                    key={cl}
                    color={cl}
                    onPress={() => this.onChangeSelectColor(cl)}
                  />
                )
              } else {
                if (index < 4) {
                  return <View style={styles.viewBtn} key={cl} />
                }

                return (
                  <Button
                    key={cl}
                    color={cl}
                    onPress={() => this.onChangeSelectColor(cl)}
                  />
                )
              }

            })
          }
          <TouchableOpacity onPress={this.onTurnOff} style={styles.btnTurnOff}>
            <Icon
              name='power-settings-new'
              size={24}
              color={common().CHECKBOX_ACTIVE_COLOR}
            />
          </TouchableOpacity>
        </View>
        <View style={[styles.circle, { width: circleRadius, height: circleRadius, borderRadius: circleRadius }]} />>

        <View style={[styles.viewSlider, { width: circleRadius }]}>
          <View style={styles.viewTextSlider}>
            <Text style={styles.title}>
              {statusTitle}
            </Text>
            <Text style={styles.title}>
              {`${numberBrightness}%`}
            </Text>
          </View>
          <MKSlider
            lowerTrackColor={common().SLIDER_ACTIVE_COLOR}
            upperTrackColor={common().SLIDER_INACTIVE_COLOR}
            value={numberBrightness}
            onConfirm={this.onChangeSlider}
          />
        </View>

        <ColorWheel
          initialColor={getColorFromLevel(val.rgb)}
          onColorChange={this.onChangeColorCircle}
          style={[styles.colorWhell, { width: circleRadius - 68, height: circleRadius - 68 }]}
          thumbStyle={[styles.thumb]}
        />
      </View>
    );
  }
}

RGB.defaultProps = {
  val: {
    rgb: '00:00:00',
    state: 'off'
  },
  circleRadius: Math.min((SCREEN.width - 64), 540),
  statusTitle: 'Brightness'
}

RGB.propTypes = {
  val: PropTypes.shape({
    state: PropTypes.string,
    level: PropTypes.string,
    unit: PropTypes.string
  }),
  circleRadius: PropTypes.number,
  statusTitle: PropTypes.string,
  onChangColor: PropTypes.func
}

export default RGB;

