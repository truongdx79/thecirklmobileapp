/**
* Created by bav on Wed Aug 01 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text, ViewPropTypes, Animated, ActivityIndicator, Easing } from 'react-native';
import { MKButton } from 'react-native-material-kit';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import Curtain from './Curtain';
import { common } from '../../config/common';
import MoreMenu from './MoreMenu';
import Overlay from './Overlay';
import { stringToNumber, numberToString } from '../../common/math';

const styles = StyleSheet.create({
  container: {
    borderRadius: 10,
    overflow: 'hidden',
    elevation: 2,
    shadowOffset: {
      width: 2,
      height: 2
    },
    shadowRadius: 4,
    shadowOpacity: 0.4,
    padding: 8
  },
  viewTop: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  viewIndicator: {
    position: 'absolute',
    top: 2,
    right: 2
  },
  viewBottom: {
    flex: 1,
    justifyContent: 'flex-end'
  },
  viewState: {
    fontSize: 14,
    marginRight: 4
  },
  title: {
    fontSize: 14
  },
  select: {
    width: 28,
    height: 28,
    borderRadius: 14,
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center'
  },
  viewSelected: {

  },
  viewNotSelected: {
    borderWidth: 1
  }
});

class DeviceCurtain extends PureComponent {
  constructor(props) {
    super(props);
    const defaultVal = {
      state: 'off',
      level: '0'
    }

    this.state = {
      val: props.selectMode ? defaultVal : props.val,
      buttonRect: {},
      isOpenMoreMenu: false,
      showControlPanel: false,
      loading: false,
      isSelect: props.isSelect || false
    }
    this.scale = new Animated.Value(1);
    this.ware = new Animated.Value(0);
    this.pressAnimated = new Animated.Value(props.selectMode ? props.isSelect ? 1 : 0 : (props.val && stringToNumber(props.val.level) !== 0) ? 1 : 0);
    this.button = React.createRef();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isControl) {
      if (nextProps.val !== this.state.val) {
        if (this.timeoutLoading) {
          clearTimeout(this.timeoutLoading)
        }
        if (stringToNumber(nextProps.val.level) == 0) {
          Animated.timing(this.pressAnimated, {
            toValue: 0,
            duration: 300
          }).start()
        } else {
          Animated.timing(this.pressAnimated, {
            toValue: 1,
            duration: 300
          }).start()
        }

        this.setState({
          val: nextProps.val,
          loading: false
        })
      } else {
        if (stringToNumber(nextProps.val.level) == 0) {
          Animated.timing(this.pressAnimated, {
            toValue: 0,
            duration: 300
          }).start()
        } else {
          Animated.timing(this.pressAnimated, {
            toValue: 1,
            duration: 300
          }).start()
        }
      }
    } else {
      if (this.props.selectMode !== nextProps.selectMode) {
        if (nextProps.selectMode) {
          this.pressAnimated.setValue(0)
        } else {
          if (stringToNumber(this.state.val.level) !== 0) {
            Animated.timing(this.pressAnimated, {
              toValue: 1,
              duration: 300
            }).start()
          } else {
            Animated.timing(this.pressAnimated, {
              toValue: 0,
              duration: 300
            }).start()
          }

          this.setState({
            isSelect: false
          })
        }
      }
    }
  }

  componentWillUnmount() {
    if (this.timeoutLoading) {
      clearTimeout(this.timeoutLoading)
    }
  }

  onPress = () => {
    this.props.onPress && this.props.onPress();
    if (this.props.isControl && this.button) {
      this.button.current.measure((ox, oy, width, height, px, py) => {
        this.setState({
          buttonRect: { x: px, y: py, width: width, height: height }
        }, () => {
          this.setState({ showControlPanel: true })
        });
      });
    } else if (this.props.selectMode) {
      if (this.props.typeSelectMode == 'muti') {
        this.setState(prewState => {
          const isSelect = !prewState.isSelect
          if (!isSelect) {
            Animated.timing(this.pressAnimated, {
              toValue: 0,
              duration: 300
            }).start()
          } else {
            Animated.timing(this.pressAnimated, {
              toValue: 1,
              duration: 300
            }).start()
          }

          this.props.onChangeSelect && this.props.onChangeSelect(isSelect)

          return { isSelect }
        })
      }
    } else {
      this.setState(prewState => {
        const val = stringToNumber(prewState.val.level) == 0 ? this.props.numberOffSpeed : 0

        if (val === 0) {
          Animated.timing(this.pressAnimated, {
            toValue: 0,
            duration: 300
          }).start()
        } else {
          Animated.timing(this.pressAnimated, {
            toValue: 1,
            duration: 300
          }).start()
        }

        const status = {
          ...prewState.val,
          level: numberToString(val),
          state: numberToString(val) == 0 ? 'off' : 'on'
        }

        this.props.onChange && this.props.onChange(status)

        return { val: status }
      })
    }
  }

  onPressIn = () => {
    this.scaleAnimated = Animated.spring(this.scale, {
      toValue: 1.3, tension: 200, friction: 7
    }).start()
  }

  onPressOut = () => {
    if (this.scaleAnimated) {
      this.scaleAnimated.stop()
    }

    Animated.spring(this.scale, {
      toValue: 1, tension: 200, friction: 7
    }).start()
  }

  onLongPress = () => {
    this.onPressOut()
    if (this.button) {
      this.button.current.measure((ox, oy, width, height, px, py) => {
        this.setState({
          buttonRect: { x: px, y: py, width: width, height: height }
        }, () => {
          this.setState({ isOpenMoreMenu: true })
        });
      });
    }
  }

  longPressDevice = () => {
    this.onPressOut()
    this.props.onLongPress && this.props.onLongPress()
  }

  onChangeLevel = (val) => {
    if (!this.props.isControl) {
      this.props.onChange && this.props.onChange(val)
      this.setState({
        val
      })
    }
  }

  onSwiperComplete = (val) => {
    this.setState({ loading: true })
    this.props.onPress && this.props.onPress(val)
    this.props.onControl && this.props.onControl(val)
    if (this.timeoutLoading) {
      clearTimeout(this.timeoutLoading)
    }
    this.timeoutLoading = setTimeout(() => {
      this.setState({ loading: false })
    }, this.props.timeoutLoading)
  }

  startWare = () => {
    Animated.timing(this.ware, {
      toValue: 1,
      duration: 100,
      easing: Easing.linear
    }).start(() => {
      Animated.timing(this.ware, {
        toValue: -1,
        duration: 100,
        easing: Easing.linear
      }).start(() => {
        this.startWare()
      })
    })
  }

  stopWare = () => {
    Animated.timing(this.ware, {
      toValue: 0,
      duration: 0,
      easing: Easing.linear
    }).start()
  }

  onSchedule = () => {
    this.props.onSelectSchedule && this.props.onSelectSchedule()
  }

  onHistory = () => {
    this.props.onSelectHistory && this.props.onSelectHistory()
  }

  onInfo = () => {
    this.props.onSelectInfo && this.props.onSelectInfo()
  }

  renderSelect = () => {
    const { isSelect } = this.state;
    const { iconColorActive, iconColorInactive, typeSelectMode } = this.props;
    if (typeSelectMode == 'muti') {
      return (
        <View
          style={[
            styles.select,
            isSelect ? styles.viewSelected : styles.viewNotSelected,
            isSelect ? { backgroundColor: iconColorActive } : { borderColor: iconColorInactive }
          ]}
        >
          {
            isSelect && <Icon name='check' color={common().DEVICE_BACKGROUND_COLOR_ACTIVE} size={24} />
          }
        </View>
      )
    } else {
      return (
        <Icon name='more-horiz' color={iconColorInactive} size={24} />
      )
    }
  }

  render() {
    let {
      width,
      backgroundColorActive,
      backgroundColorInactive,
      iconColorActive,
      iconColorInactive,
      titleColorActive,
      titleColorInactive,
      delayLongPress,
      iconName,
      deviceName,
      isControl,
      selectMode,
      style,
      colorIndicator,
      showHeader,
      onControl,
      statusTitle,
      loadingTitle,
      longPress
    } = this.props;
    const { val, loading, showControlPanel, buttonRect, isSelect } = this.state;

    const ComponentButton = Animated.createAnimatedComponent(MKButton)
    const AnimatedComponent = Animated.createAnimatedComponent(Icon)

    return (
      <View
        ref={this.button}
        renderToHardwareTextureAndroid
        style={[style, { borderRadius: 10 }]}
      >
        <ComponentButton
          style={[
            styles.container,
            {
              width,
              height: width,
              backgroundColor: this.pressAnimated.interpolate({
                inputRange: [0, 1],
                outputRange: [backgroundColorInactive, backgroundColorActive]
              }),
              shadowColor: (stringToNumber(val.level) !== 0 || isSelect) ? backgroundColorActive : backgroundColorInactive,
              transform: [
                {
                  scale: this.scale
                },
                {
                  rotateZ: this.ware.interpolate({
                    inputRange: [-1, 0, 1],
                    outputRange: ['-1.2deg', '0deg', '1.2deg']
                  })
                }
              ]
            }
          ]}
          onPress={this.onPress}
          delayLongPress={delayLongPress}
          onLongPress={(isControl && longPress) ? this.onLongPress : this.longPressDevice}
          onPressIn={this.onPressIn}
          onPressOut={this.onPressOut}
        >
          <View style={styles.viewTop}>
            <AnimatedComponent
              name={iconName} size={36}
              color={selectMode ? isSelect ? iconColorActive : iconColorInactive : stringToNumber(val.level) !== 0 ? iconColorActive : iconColorInactive}
            />
            {
              loading && !selectMode && <ActivityIndicator
                size='small'
                color={colorIndicator}
                style={styles.viewIndicator}
              />
            }
            {
              !loading && !selectMode && <Text style={[styles.viewState, { color: stringToNumber(val.level) !== 0 ? titleColorActive : titleColorInactive }]}>
                {statusTitle}
              </Text>
            }

            {
              selectMode && this.renderSelect()
            }
          </View>
          <View style={styles.viewBottom}>
            <Text numberOfLines={2} style={[styles.title, { color: selectMode ? isSelect ? iconColorActive : iconColorInactive : stringToNumber(val.level) !== 0 ? iconColorActive : iconColorInactive }]}>
              {deviceName}
            </Text>
          </View>
        </ComponentButton>
        <MoreMenu
          isOpen={this.state.isOpenMoreMenu}
          origin={this.state.buttonRect}
          onClose={() => this.setState({ isOpenMoreMenu: false })}
          button={[
            {
              iconName: 'today',
              onPress: this.onSchedule
            },
            {
              iconName: 'history',
              onPress: this.onHistory
            },
            {
              iconName: 'info',
              onPress: this.onInfo
            }
          ]}
        />
        <Overlay
          isOpen={showControlPanel}
          origin={buttonRect}
          onClose={() => this.setState({ showControlPanel: false })}
          touchToDismiss
          blurOverlay
        >
          <Curtain
            isControl={isControl}
            onControl={onControl}
            showHeader={showHeader}
            val={val}
            statusTitle={statusTitle}
            loadingTitle={loadingTitle}
          />
        </Overlay>
      </View>
    );
  }
}

DeviceCurtain.defaultProps = {
  val: {
    level: '0',
    state: 'close'
  },
  width: 108,
  backgroundColorActive: common().DEVICE_BACKGROUND_COLOR_ACTIVE,
  backgroundColorInactive: common().DEVICE_BACKGROUND_COLOR_INACTIVE,
  iconColorActive: common().DEVICE_ICON_COLOR_ACTIVE,
  iconColorInactive: common().DEVICE_ICON_COLOR_INACTIVE,
  titleColorActive: common().DEVICE_TEXT_COLOR_ACTIVE,
  titleColorInactive: common().DEVICE_TEXT_COLOR_INACTIVE,
  isControl: false,
  longPress: true,
  isSelect: false,
  selectMode: false,
  typeSelectMode: 'muti',
  colorIndicator: '#fff',
  delayLongPress: 1000,
  timeoutLoading: 15000,
  showHeader: true,
  iconName: 'view-column',
  statusTitle: 'Level'
}

DeviceCurtain.propTypes = {
  val: PropTypes.shape({
    state: PropTypes.string,
    level: PropTypes.string,
    unit: PropTypes.string
  }),
  width: PropTypes.number,
  iconName: PropTypes.string,
  backgroundColorActive: PropTypes.string,
  backgroundColorInactive: PropTypes.string,
  iconColorActive: PropTypes.string,
  iconColorInactive: PropTypes.string,
  deviceName: PropTypes.string,
  titleColorActive: PropTypes.string,
  titleColorInactive: PropTypes.string,
  isControl: PropTypes.bool,
  longPress: PropTypes.bool,
  isSelect: PropTypes.bool,
  selectMode: PropTypes.bool,
  typeSelectMode: PropTypes.oneOf(['muti', 'onePress']),
  /**
   * onPress when isControl = true
   * return current state
   */
  onPress: PropTypes.func,
  onLongPress: PropTypes.func,
  onChange: PropTypes.func,
  /**
   * onChange when isSelect = true or isControl = false
   * return state had change
   */
  onChangeSelect: PropTypes.func,
  style: ViewPropTypes.style,
  colorIndicator: PropTypes.string,
  delayLongPress: PropTypes.number,
  timeoutLoading: PropTypes.number,
  iconName: PropTypes.string,
  showHeader: PropTypes.bool,
  onControl: PropTypes.func,
  onSelectSchedule: PropTypes.func,
  onSelectHistory: PropTypes.func,
  onSelectInfo: PropTypes.func,
  statusTitle: PropTypes.string,
  loadingTitle: PropTypes.string
}

export default DeviceCurtain;
