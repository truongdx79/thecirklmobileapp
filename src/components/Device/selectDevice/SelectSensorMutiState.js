/**
* Created by bav on Wed Aug 22 2018
* Copyright (c) 2018 bav@luci.vn
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text, Dimensions } from 'react-native';
import { RowInput } from '../../Row';
import { common } from '../../../config/common';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center'
  }
});

class SelectSensorMutiState extends PureComponent<Props> {
  constructor(props) {
    super(props);
    this.state = {
      val: props.val
    }
  }

  onChangeLevel = (level) => {
    let val = {
      ...this.state.val,
      level
    }

    this.props.onChange && this.props.onChange(val)
    this.setState({
      val
    })
  }

  render() {
    const {
      levelTitle,
      inputTitle,
      maxLengthInput,
      widthContent
    } = this.props;
    const { val } = this.state;

    return (
      <RowInput
        value={val.level}
        leftTitle={`${levelTitle} (${val.unit})`}
        placeholder={inputTitle}
        onChangeText={this.onChangeLevel}
        returnKeyType='done'
        keyboardType='numeric'
        maxLength={maxLengthInput}
        borderRadius
        style={{ width: widthContent - 32 }}
      />
    );
  }
}

SelectSensorMutiState.defaultProps = {
  val: {
    level: '0',
    unit: '%'
  },
  levelTitle: 'Level',
  inputTitle: 'Input level',
  maxLengthInput: 4,
  widthContent: Dimensions.get('window').width
}

SelectSensorMutiState.propTypes = {
  val: PropTypes.shape({
    state: PropTypes.string,
    level: PropTypes.string,
    unit: PropTypes.string
  }),
  onChange: PropTypes.func
}

type valStatic = {
  state?: 'on' | 'off',
  level?: string,
  unit?: string
}

interface Props {
  val?: valStatic;
  arrayStatus?: Array;
  levelTitle?: string;
  inputTitle?: string;
  maxLengthInput?: number;
  onChange?: () => void;
  widthContent?: number;
}

export default SelectSensorMutiState;
