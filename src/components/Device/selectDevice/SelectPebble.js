/**
* Created by bav on Wed Aug 22 2018
* Copyright (c) 2018 bav@luci.vn
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text, Dimensions } from 'react-native';
import { Row } from '../../Row';
import { common } from '../../../config/common';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center'
  }
});

class SelectPebble extends PureComponent<Props> {
  constructor(props) {
    super(props);
    this.state = {
      val: props.val
    }
  }

  onChangeState = (status) => {
    const val = {
      act: status.act
    }

    this.props.onChange && this.props.onChange(val)
    this.setState({
      val
    })
  }

  render() {
    const {
      arrayStatus,
      widthContent
    } = this.props;
    const { val } = this.state;

    return (
      <View style={styles.container}>
        {
          arrayStatus.map((status, idx) => {
            return (
              <Row
                key={idx}
                leftTitle={status.title}
                firstBorder={idx == 0}
                lastBorder={arrayStatus.length - 1 == idx}
                style={{ marginBottom: 5, width: widthContent - 32 }}
                backgroundColor={(val.act == status.act) ? common().DEVICE_BACKGROUND_COLOR_ACTIVE : undefined}
                onPress={() => this.onChangeState(status)}
              />
            )
          })
        }
      </View>
    );
  }
}

SelectPebble.defaultProps = {
  val: {
    act: '0'
  },
  arrayStatus: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
  widthContent: Dimensions.get('window').width
}

SelectPebble.propTypes = {
  val: PropTypes.shape({
    state: PropTypes.string,
    level: PropTypes.string,
    unit: PropTypes.string
  }),
  arrayStatus: PropTypes.array,
  onChange: PropTypes.func
}

type valStatic = {
  act?: string | number,
  level?: string,
  unit?: string
}

interface Props {
  val?: valStatic;
  arrayStatus?: Array;
  onChange?: () => void;
  widthContent?: number;
}

export default SelectPebble;
