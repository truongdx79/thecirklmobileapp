/**
* Created by bav on Fri Aug 31 2018
* Copyright (c) 2018 bav@luci.vn
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Animated, Dimensions } from 'react-native';
import Interactable from 'react-native-interactable';
import { Row } from '../../Row';
import langs from '../../../languages/device';
import Icon from '../../Icon';
import { common } from '../../../config/common';

const SCREEN = Dimensions.get('window');
const IconComponent = Animated.createAnimatedComponent(Icon);
const styles = StyleSheet.create({
  container: {
    paddingVertical: 12
  },
  viewSlider: {
    height: 24,
    alignSelf: 'center',
    justifyContent: 'center',
    position: 'relative'
  },
  viewLineSlider: {
    height: 5,
    backgroundColor: common().CARD_BACKGROUND_COLOR,
    position: 'absolute'
  },
  viewCircleSlider: {
    width: 24,
    height: 24,
    borderRadius: 12,
    backgroundColor: 'white',
    elevation: 3,
    shadowColor: common().BACKGROUND_COLOR_CARD,
    shadowOpacity: 0.4,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 4,
  },
  viewNode: {
    position: 'absolute',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  viewPoint: {
    width: 16,
    height: 16,
    borderRadius: 8,
    backgroundColor: common().SLIDER_CIRCLE_COLOR,
    position: 'absolute'
  },
  viewPointActive: {
    width: 16,
    height: 16,
    borderRadius: 8,
    backgroundColor: common().BACKGROUND_COLOR_BUTTON_ACTIVE,
    position: 'absolute'
  }
});

class SelectCurtain extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      val: props.val
    }
    this._deltaX = new Animated.Value(this.getInitialPosition().x)
  }

  onSnap = ({ nativeEvent }) => {
    const val = {
      state: nativeEvent.index == 0 ? 'off' : 'on',
      level: String(nativeEvent.index)
    }

    this.props.onChange && this.props.onChange(val)
    this.setState({
      val
    })
  }

  getLevelFromVal = (val) => {
    return val.level == 0 ? 0 : parseInt(val.level) - 1
  }

  getSnapPoints = () => {
    const { widthContent, numberLevel } = this.props;
    let width = widthContent - 32 - 24;
    let snapPoints = []
    for (i = 0; i < numberLevel; i++) {
      let point = {
        x: width * i / (numberLevel - 1) - 12,
        id: i
      }

      snapPoints.push(point)
    }

    return snapPoints
  }

  getInitialPosition = () => {
    const { val, widthContent, numberLevel } = this.props;
    let width = widthContent - 32 - 24;

    let point = {
      x: val.level == 0 ? -12 : width * parseInt(val.level) / (numberLevel - 1) - 12
    }

    return point
  }

  render() {
    const {
      arrayIcon,
      widthContent,
      numberLevel
    } = this.props;
    const { val } = this.state;
    let width = widthContent - 32 - 24;

    return (
      <View style={styles.container}>
        <Row
          leftTitle={langs.statusDevice}
          rightTitle={`${langs.levelTitle} ${val.level}`}
          style={{ width: widthContent - 32 }}
          borderRadius
        />

        <IconComponent
          name={arrayIcon[this.getLevelFromVal(val)]}
          size={240}
          color={common().DEVICE_ICON_COLOR_ACTIVE}
          style={{ alignSelf: 'center' }}
        />

        <View style={[styles.viewSlider, { width }]}>
          <View style={[styles.viewLineSlider, { width }]} />
          <View style={{ overflow: 'hidden', width, position: 'absolute', height: 5 }}>
            <Animated.View
              style={[
                styles.viewLineSlider,
                {
                  backgroundColor: common().BACKGROUND_COLOR_BUTTON_ACTIVE,
                  width,
                  transform: [{
                    translateX: this._deltaX.interpolate({
                      inputRange: [0, width],
                      outputRange: [-width, 0]
                    })
                  }]
                }
              ]}
            />
          </View>
          <View style={styles.viewNode}>
            {
              this.getSnapPoints().map((point, idx) => {
                return (
                  <View
                    key={idx}
                    style={[
                      styles.viewPoint,
                      { left: point.x }
                    ]}
                  />
                )
              })
            }
          </View>
          <View style={styles.viewNode}>
            {
              this.getSnapPoints().map((point, idx) => {
                return (
                  <Animated.View
                    key={idx}
                    style={[
                      styles.viewPointActive,
                      {
                        left: point.x,
                        opacity: this._deltaX.interpolate({
                          inputRange: [idx * (width / (numberLevel - 1)), (idx + 1) * (width / (numberLevel - 1))],
                          outputRange: [0, 1]
                        })
                      },
                    ]}
                  />
                )
              })
            }
          </View>
          <Interactable.View
            horizontalOnly
            snapPoints={this.getSnapPoints()}
            initialPosition={this.getInitialPosition()}
            onSnap={this.onSnap}
            animatedValueX={this._deltaX}
            animatedNativeDriver
          >
            <Animated.View style={styles.viewCircleSlider} />
          </Interactable.View>
        </View>
      </View>
    );
  }
}

SelectCurtain.defaultProps = {
  val: {
    level: '0',
    state: 'close'
  },
  arrayIcon: ['view-column', 'view-column', 'view-array', 'view-array', 'view-array', 'view-column', 'view-column'],
  numberLevel: 7,
  widthContent: SCREEN.width
}

SelectCurtain.propTypes = {
  val: PropTypes.shape({
    state: PropTypes.string,
    level: PropTypes.string,
    unit: PropTypes.string
  }),
  arrayIcon: PropTypes.array,
  numberLevel: PropTypes.number,
  onChange: PropTypes.func,
  widthContent: PropTypes.number
}

export default SelectCurtain;
