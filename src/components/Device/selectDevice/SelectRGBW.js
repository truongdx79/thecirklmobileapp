/**
* Created by bav on Thu Aug 30 2018
* Copyright (c) 2018 bav@luci.vn
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text, Dimensions } from 'react-native';
import { MKButton } from 'react-native-material-kit';
import Overlay from '../Overlay';
import RGBW from '../RGBW';
import { Row } from '../../Row';
import langs from '../../../languages/device';
import { common } from '../../../config/common';
import { getColorFromLevel, getLevelFromColor } from '../../../common/device';

const COLOR = ['#ff0000', '#ffff00', '#00ff00', '#0000ff', '#00ffff']
const styles = StyleSheet.create({
  container: {

  },
  viewSelectColor: {
    alignItems: 'center'
  },
  viewButton: {
    height: 55,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    backgroundColor: 'rgba(255, 255, 255, 0.2)',
    width: '100%'
  },
  title: {
    color: common().COLOR_TEXT_NORMAL,
    fontSize: common().FONT_SIZE_TITLE,
    fontWeight: common().FONT_WEIGHT_TITLE
  },
  txtSelectColor: {
    color: common().COLOR_TEXT_ACTIVE,
    fontSize: common().FONT_SIZE_CONTENT,
    fontWeight: common().FONT_WEIGHT_TITLE,
    marginBottom: 8,
    alignSelf: 'flex-start'
  }
});

class SelectRGBW extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      val: props.val,
      showControlPanel: false,
      buttonRect: {},
    }
    this.button = React.createRef();
  }

  onChangeColor = (val) => {
    this.props.onChange && this.props.onChange(val)
    this.setState({
      val
    })
  }

  onViewMoreColor = () => {
    this.button.current.measure((ox, oy, width, height, px, py) => {
      this.setState({
        buttonRect: { x: px, y: py, width: width, height: height }
      }, () => {
        this.setState({ showControlPanel: true })
      });
    });
  }

  onSelectColor = (color) => {
    let rgbw = getLevelFromColor(color)
    let val = {
      state: color === '#000000' ? 'off' : 'on',
      rgbw
    }

    this.props.onChange && this.props.onChange(val)
    this.setState({
      val
    })
  }

  render() {
    const { widthContent } = this.props;
    const { val, showControlPanel, buttonRect } = this.state;

    return (
      <View style={styles.container}>
        <View style={styles.viewSelectColor}>
          <Row
            backgroundColor={getColorFromLevel(val.rgbw)}
            style={{ marginBottom: 20, width: widthContent - 32 }}
            borderRadius
          />
          <Text style={styles.txtSelectColor}>
            {langs.selectColor}
          </Text>
          {
            COLOR.map((cl, idx) => {
              return (
                <Row
                  firstBorder={idx == 0}
                  key={cl}
                  backgroundColor={cl}
                  style={{ marginBottom: 5, width: widthContent - 32 }}
                  onPress={() => this.onSelectColor(cl)}
                  leftTitle={' '}
                  rightIconName={cl === getColorFromLevel(val.rgbw) ? 'check' : undefined}
                />
              )
            })
          }
          <View
            style={{ width: '100%' }}
            ref={this.button}
            renderToHardwareTextureAndroid
            shouldRasterizeIOS
          >
            <MKButton
              onPress={this.onViewMoreColor}
              style={styles.viewButton}
            >
              <Text style={styles.title}>
                {langs.moreColor}
              </Text>
            </MKButton>
          </View>
        </View>
        <Overlay
          isOpen={showControlPanel}
          origin={buttonRect}
          onClose={() => this.setState({ showControlPanel: false })}
          touchToDismiss
          blurOverlay
        >
          <RGBW
            val={val}
            onChangColor={this.onChangeColor}
            statusTitle={langs.colorTitle}
          />
        </Overlay>
      </View>
    );
  }
}

SelectRGBW.defaultProps = {
  val: {
    rgbw: '00:00:00:00',
    state: 'off'
  },
  widthContent: Dimensions.get('window').width
}

SelectRGBW.propTypes = {
  val: PropTypes.shape({
    state: PropTypes.string,
    rgbw: PropTypes.string,
    unit: PropTypes.string
  }),
  onChange: PropTypes.func,
  widthContent: PropTypes.number
}

export default SelectRGBW;
