/**
* Created by bav on Wed Aug 22 2018
* Copyright (c) 2018 bav@luci.vn
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text, Dimensions } from 'react-native';
import { Row } from '../Row';
import { common } from '../../config/common';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center'
  }
});

class SwitchOnOff extends PureComponent<Props> {
  constructor(props) {
    super(props);
    this.state = {
      val: props.val
    }
  }

  onChangeState = (status) => {
    const val = {
      state: status.state
    }

    this.props.onChange && this.props.onChange(val)
    this.setState({
      val
    })
  }

  render() {
    const {
      arrayStatus,
      widthContent
    } = this.props;
    const { val } = this.state;

    return (
      <View style={[styles.container, { width: widthContent }]}>
        {
          arrayStatus.map((status, idx) => {
            return (
              <Row
                key={idx}
                leftTitle={status.title}
                firstBorder={idx == 0}
                lastBorder={arrayStatus.length - 1 == idx}
                style={{ marginBottom: 5, width: widthContent - 32 }}
                backgroundColor={(val.state == status.state) ? common().DEVICE_BACKGROUND_COLOR_ACTIVE : undefined}
                onPress={() => this.onChangeState(status)}
              />
            )
          })
        }
      </View>
    );
  }
}

SwitchOnOff.defaultProps = {
  val: {
    state: 'off'
  },
  arrayStatus: ['on', 'off'],
  widthContent: Dimensions.get('window').width
}

SwitchOnOff.propTypes = {
  val: PropTypes.shape({
    state: PropTypes.string,
    level: PropTypes.string,
    unit: PropTypes.string
  }),
  arrayStatus: PropTypes.array,
  onChange: PropTypes.func
}

type valStatic = {
  state?: 'on' | 'off',
  level?: string,
  unit?: string
}

interface Props {
  val?: valStatic;
  arrayStatus?: Array;
  onChange?: () => void;
  widthContent?: number;
}

export default SwitchOnOff;
