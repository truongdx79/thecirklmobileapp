/**
* Created by bav on Mon Aug 06 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text, ActivityIndicator, Animated } from 'react-native';
import { MKButton } from 'react-native-material-kit';
import { Swiper } from '../Swiper';
import Icon from '../Icon';
import { common } from '../../config/common';
import { stringToNumber, numberToString } from '../../common/math';

const styles = StyleSheet.create({
  viewIcon: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 25
  },
  btnIcon: {
    width: 36,
    height: 36,
    borderRadius: 18,
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center'
  },
  content: {
    fontSize: common().FONT_SIZE_TITLE,
    fontWeight: common().FONT_WEIGHT_TITLE,
    marginTop: 10
  },
  viewLoading: {
    width: 36,
    height: 36,
    justifyContent: 'center',
    alignItems: 'center'
  }
});

class Dimmer extends PureComponent<Props> {
  constructor(props) {
    super(props);
    const defaulVal = {
      state: 'off',
      level: '0'
    }

    this.state = {
      val: props.val || defaulVal,
      loading: false
    }

    this.refSwiper = React.createRef()
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.isControl) {
      if (nextProps.val !== this.props.val) {
        if (this.timeoutLoading) {
          clearTimeout(this.timeoutLoading)
        }

        this.setState({
          currentVal: nextProps.val,
          loading: false
        })
      }
    }
  }

  componentWillUnmount() {
    if (this.timeoutLoading) {
      clearTimeout(this.timeoutLoading)
    }
  }

  getInitialValue = () => {
    const { val } = this.state;
    let status = 0;
    if (val.state === 'on') {
      status = val.level === '-1' ? this.props.maxLevel : stringToNumber(val.level)
    } else {
      status = 0
    }

    return status
  }

  onPressIcon = () => {
    const val = {
      level: this.state.val.state === 'off' ? '100' : '0',
      state: this.state.val.state === 'off' ? 'on' : 'off'
    }

    if (this.refSwiper) {
      this.refSwiper.current.snapToValue(val.level)
    }

    this.setState({ val })
  }

  changeStateDevice = (level, onSwiper) => {
    if (this.props.isControl) {
      this.setState({
        loading: true
      })

      if (this.timeoutLoading) {
        clearTimeout(this.timeoutLoading)
      }
      this.timeoutLoading = setTimeout(() => {
        this.setState({ loading: false })
      }, this.props.timeoutLoading)
    }

    const val = {
      state: level === 0 ? 'off' : 'on',
      level: String(level)
    }

    this.setState({
      val
    })

    return onSwiper && onSwiper(val)
  }

  onChange = (level) => {
    this.changeStateDevice(level, this.props.onChange)
  }

  onSwiperComplete = (level) => {
    this.changeStateDevice(level, this.props.onSwiperComplete)
  }

  render() {
    const {
      colorIndicator,
      iconName,
      statusTitle,
      loadingTitle,
      minLevel,
      maxLevel,
      step,
      showHeader,
      backgroundActive,
      onlyEventWhenStop,
      ...otherProps
    } = this.props;
    const { val, loading } = this.state;
    const AnimatedComponent = Animated.createAnimatedComponent(Icon)

    return (
      <View>
        {
          showHeader && (
            <View style={styles.viewIcon}>
              {
                loading && <View style={styles.viewLoading}>
                  <ActivityIndicator
                    size='small'
                    color={colorIndicator}
                    style={styles.viewIndicator}
                  />
                </View>
              }
              {
                !loading && (
                  <MKButton
                    onPress={this.onPressIcon}
                    style={styles.btnIcon}
                  >
                    <AnimatedComponent
                      name={iconName}
                      size={36}
                      color={backgroundActive}
                    />
                  </MKButton>
                )
              }

              <Text style={[styles.content, { color: backgroundActive }]}>
                {loading ? loadingTitle : statusTitle}
              </Text>
            </View>
          )
        }
        <Swiper
          {...otherProps}
          ref={this.refSwiper}
          initialValue={this.getInitialValue()}
          minLevel={minLevel}
          maxLevel={maxLevel}
          step={step}
          onlyEventWhenStop={onlyEventWhenStop}
          onValueChange={this.onChange}
          onSwiperComplete={this.onSwiperComplete}
        />
      </View>
    );
  }
}

Dimmer.defaultProps = {
  val: {
    level: '50',
    state: 'on'
  },
  colorIndicator: '#fff',
  minLevel: 0,
  maxLevel: 100,
  step: 1,
  showHeader: true,
  backgroundActive: common().BACKGROUND_COLOR_CARD,
  isControl: false,
  iconName: 'highlight',
  levelTitle: 'light',
  statusTitle: 'Level',
  loadingTitle: 'Loading...',
  onlyEventWhenStop: false,
  timeoutLoading: 15000,
}

Dimmer.propTypes = {
  val: PropTypes.shape({
    state: PropTypes.string,
    level: PropTypes.string,
    unit: PropTypes.string
  }),
  colorIndicator: PropTypes.string,
  minLevel: PropTypes.number,
  maxLevel: PropTypes.number,
  step: PropTypes.number,
  showHeader: PropTypes.bool,
  backgroundActive: PropTypes.string,
  isControl: PropTypes.bool,
  iconName: PropTypes.string,
  statusTitle: PropTypes.string,
  loadingTitle: PropTypes.string,
  onlyEventWhenStop: PropTypes.bool,
  onChange: PropTypes.func,
  onSwiperComplete: PropTypes.func,
  timeoutLoading: PropTypes.number
}

type valStatic = {
  state?: 'on' | 'off',
  level?: string,
  unit?: string
}

interface Props {
  /**
   * current state of device
   */
  val?: valStatic;
  /**
   * default width = 124
   */
  width?: number;
  /**
   * default width = 315
   */
  height?: number;
  backgroundControl?: string;
  backgroundColor?: string;
  /**
   * default borderRadius = 38
   */
  borderRadius?: number;
  /**
   * Only callback event when stop touch. Default true
   */
  onlyEventWhenStop?: boolean;
  /**
   * default minLevel = 0
   */
  minLevel?: number;
  /**
   * default maxLevel = 100
   */
  maxLevel?: number;
  /**
   * default step = 1
   */
  step?: number;
  /**
   * call when change step
   */
  onChange?: (val: { level: string, state: 'on' | 'off' }) => void;
  /**
   * call when stop touch
   */
  onSwiperComplete?: (val: { level: string, state: 'on' | 'off' }) => void;
  /**
   * Enable animated scale when press. Default true
   */
  scaleWhenPress?: boolean;
  /**
   * default scale = 1.1
   */
  scale?: number;
  styleContainer?: StyleProp<ViewStyle>;
  styleControl?: StyleProp<ViewStyle>;
  /**
   * default false. Set true to enable mode control real device
   */
  isControl?: boolean;
  /**
   * icon device show on header (material icon)
   */
  iconName?: string;
  statusTitle?: string;
  loadingTitle?: string;
  /**
   * default value = 15000 (15s)
   */
  timeoutLoading?: string;
  colorIndicator?: string;
}

export default Dimmer;
