/**
* Created by bav on Thu Aug 16 2018
* Copyright (c) 2018 bav@luci.vn
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Dimensions, ViewPropTypes, Text } from 'react-native';
import { CircleSeeker } from 'react-native-circle-seeker';
import Svg, { Circle, G, LinearGradient, Path, Defs, Stop, Line } from 'react-native-svg';
import range from 'lodash.range';
import { common } from '../../../config/common';

const { width } = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  viewCircle: {
    shadowRadius: 24,
    shadowOpacity: 0.74,
    position: 'absolute'
  }
});

class IrDaikin extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      angle: 0
    }
  }

  onChange = (value) => {
    this.setState({
      angle: value
    })
  }

  onPressOut = (value) => {
    // console.log('onPressOut', value)
  }

  render() {
    const {
      enable,
      style,
      width,
      withLine,
      withLineBackground,
      colorPoint,
      colorCircleBackground,
      colorCircle
    } = this.props;
    const faceRadius = (width + 50) / 2
    const currentStop = Math.floor(this.state.angle / 2)
    const currentTemp = Math.floor((this.state.angle*14/360) + 18)
    return (
      <View style={styles.container}>
        <View style={{ width: width + 50, height: width + 50, justifyContent: 'center', alignItems: 'center' }}>
          <View style={{ position: 'absolute' }}>
            <Text style={{ fontSize: 48, color: 'white', fontWeight: '400' }}>
              {`${currentTemp}˚C`}
            </Text>
          </View>
          <Svg
            width={width + 50}
            height={width + 50}
          >
            <G
              x={(width + 50) / 2}
              y={(width + 50) / 2}
            >
              {
                range(180).map(i => {
                  const cos = Math.cos(2 * Math.PI / 180 * (i - 45));
                  const sin = Math.sin(2 * Math.PI / 180 * (i - 45));

                  return (
                    <Line
                      key={i}
                      stroke={'#466088'}
                      strokeWidth={1}
                      x1={cos * ((Math.abs(i - currentStop) < 10) ? faceRadius + 1.4 * (9 - Math.abs(currentStop - i)) : faceRadius)}
                      y1={sin * ((Math.abs(i - currentStop) < 10) ? faceRadius + 1.4 * (9 - Math.abs(currentStop - i)) : faceRadius)}
                      x2={cos * (faceRadius - 24)}
                      y2={sin * (faceRadius - 24)}
                    />
                  );
                })
              }
            </G>
          </Svg>
          <CircleSeeker
            style={[
              styles.viewCircle,
              style,
              {
                width: width,
                height: width,
                shadowColor: colorCircle
              }
            ]}
            withLine={withLine}
            withLineBackground={withLineBackground}
            colorPoint={colorPoint}
            colorCircleBackground={colorCircleBackground}
            colorCircle={colorCircle}
            onChange={this.onChange}
            onPressOut={this.onPressOut}
          />
        </View>
      </View>
    );
  }
}

IrDaikin.defaultProps = {
  enable: true,
  width: Math.min((width - 132), 290),
  withLine: 30,
  withLineBackground: 40,
  colorPoint: 'white',
  colorCircleBackground: common().CIRCLE_BACKGROUND_COLOR,
  colorCircle: common().BACKGROUND_COLOR_BUTTON_ACTIVE,
}

IrDaikin.propTypes = {
  style: ViewPropTypes.style,
  width: PropTypes.number,
  enable: PropTypes.bool,
  withLine: PropTypes.number,
  withLineBackground: PropTypes.number,
  colorPoint: PropTypes.string,
  colorCircleBackground: PropTypes.string,
  colorCircle: PropTypes.string,
  value: PropTypes.number,
  onChange: PropTypes.func,
  onPressOut: PropTypes.func,
  onPressIn: PropTypes.func,
}

export default IrDaikin;
