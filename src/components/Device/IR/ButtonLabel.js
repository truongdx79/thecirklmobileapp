/**
* Created by bav on Tue Sep 04 2018
* Copyright (c) 2018 bav@luci.vn
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text } from 'react-native';
import { MKButton } from 'react-native-material-kit';
import { common } from '../../../config/common';

const styles = StyleSheet.create({
  viewButton: {
    height: 40,
    width: 40,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden'
  },
  title: {
    fontWeight: common().FONT_WEIGHT_HEADER,
    textAlign: 'center',
    color: common().COLOR_TEXT_NORMAL
  }
});

class ButtonLabel extends PureComponent<Props> {
  onPress = () => {
    this.props.onPress && this.props.onPress(this.props.commandID)
  }

  onLongPress = () => {
    this.props.onLongPress && this.props.onLongPress(this.props.commandID)
  }

  render() {
    const {
      title,
      fontSize,
      activeButtonColor,
      inactiveButtonColor,
      isActive,
      left,
      delayLongPress,
      isLabel
    } = this.props;
    const ComponentButton = isLabel ? View : MKButton;
    
    return (
      <ComponentButton
        style={[
          styles.viewButton,
          !isLabel && { backgroundColor: isActive ? activeButtonColor : 'rgba(255, 255, 255, 0.2)' }
        ]}
        onPress={this.onPress}
        onLongPress={this.onLongPress}
        delayLongPress={delayLongPress}
      >
        <Text 
          style={[
            styles.title,
            {
              color: isActive ? common().COLOR_TEXT_NORMAL : common().COLOR_TEXT_ACTIVE,
              fontSize
            }
          ]}
        >
          {title}
        </Text>
      </ComponentButton>
    );
  }
}

ButtonLabel.defaultProps = {
  title: '1',
  fontSize: 23,
  activeButtonColor: common().BACKGROUND_COLOR_BUTTON_ACTIVE,
  inactiveButtonColor: common().BACKGROUND_COLOR_BUTTON_INACTIVE,
  isActive: false,
  delayLongPress: 1000,
  isLabel: false
}

interface Props {
  title?: string;
  fontSize?: number;
  activeButtonColor?: string;
  inactiveButtonColor?: string;
  isActive?: boolean;
  onPress?: () => void;
  onLongPress: () => void;
  delayLongPress?: number;
  isLabel?: boolean;
  commandID?: string
}

export default ButtonLabel;
