/**
* Created by bav on Tue Sep 04 2018
* Copyright (c) 2018 bav@luci.vn
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text } from 'react-native';
import ButtonIcon from './ButtonIcon';
import ButtonLabel from './ButtonLabel';
import { IR_COMMAND_TV } from '../../../common/device';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 25
  },
  row: {
    height: '20%',
    marginLeft: 30,
    marginRight: 30,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  row2: {
    height: '25%',
    marginLeft: 30,
    marginRight: 30,
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
});

class IrTV extends PureComponent {
  render() {
    const {

    } = this.props;
    return (
      <View style={{ flex: 1, width: '96%' }}>
        <View style={styles.container}>
          <View style={styles.row}>
            <ButtonIcon commandID={IR_COMMAND_TV.tv_power} iconName='power-settings-new' />
            <ButtonIcon commandID={IR_COMMAND_TV.tv_mute} iconName='volume-mute' />
          </View>
          <View style={styles.row}>
            <ButtonIcon commandID={IR_COMMAND_TV.tv_volUp} iconName='add-circle' />
            <ButtonIcon commandID={IR_COMMAND_TV.tv_up} iconName='arrow-upward' />
            <ButtonIcon commandID={IR_COMMAND_TV.tv_channelUp} iconName='arrow-upward' />
          </View>
          <View style={[{ marginLeft: 20, marginRight: 20 }, styles.row]}>
            <ButtonLabel isLabel title='VOL' fontSize={18} />
            <ButtonIcon commandID={IR_COMMAND_TV.tv_left} iconName='keyboard-arrow-left' />
            <ButtonLabel commandID={IR_COMMAND_TV.tv_ok} title='OK' fontSize={18} />
            <ButtonIcon commandID={IR_COMMAND_TV.tv_right} iconName='keyboard-arrow-right' />
            <ButtonLabel isLabel title='CH' fontSize={18} />
          </View>
          <View style={styles.row}>
            <ButtonIcon commandID={IR_COMMAND_TV.tv_volDow} iconName='add-circle' />
            <ButtonIcon commandID={IR_COMMAND_TV.tv_down} iconName='arrow-downward' />
            <ButtonIcon commandID={IR_COMMAND_TV.tv_channelDow} iconName='arrow-downward' />
          </View>
          <View style={styles.row}>
            <ButtonIcon commandID={IR_COMMAND_TV.tv_menu} iconName='menu' />
            <ButtonIcon commandID={IR_COMMAND_TV.tv_clear} iconName='clear' />
          </View>
        </View>

        <View style={styles.container}>
          <View style={styles.row2}>
            <ButtonLabel commandID={IR_COMMAND_TV.tv_ch1} title='1' />
            <ButtonLabel commandID={IR_COMMAND_TV.tv_ch2} title='2' />
            <ButtonLabel commandID={IR_COMMAND_TV.tv_ch3} title='3' />
          </View>
          <View style={styles.row2}>
            <ButtonLabel commandID={IR_COMMAND_TV.tv_ch4} title='4' />
            <ButtonLabel commandID={IR_COMMAND_TV.tv_ch5} title='5' />
            <ButtonLabel commandID={IR_COMMAND_TV.tv_ch6} title='6' />
          </View>
          <View style={styles.row2}>
            <ButtonLabel commandID={IR_COMMAND_TV.tv_ch7} title='7' />
            <ButtonLabel commandID={IR_COMMAND_TV.tv_ch8} title='8' />
            <ButtonLabel commandID={IR_COMMAND_TV.tv_ch9} title='9' />
          </View>
          <View style={styles.row2}>
            <ButtonLabel commandID={IR_COMMAND_TV.tv_list} title='List' fontSize={18} />
            <ButtonLabel commandID={IR_COMMAND_TV.tv_ch0} title='0' />
            <ButtonLabel commandID={IR_COMMAND_TV.tv_info} title='Info' fontSize={18} />
          </View>
        </View>
      </View>
    );
  }
}

IrTV.defaultProps = {

}

IrTV.propTypes = {

}

export default IrTV;
