/**
* Created by bav on Tue Sep 04 2018
* Copyright (c) 2018 bav@luci.vn
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text } from 'react-native';
import ButtonFan from './ButtonFan';
import { IR_COMMAND_FAN } from '../../../common/device';
import { ActionSheet } from '../../ActionSheet';
import LearnCommand from './LearnCommand';
import langs from '../../../languages/device';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '96%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  rowHeader: {
    width: '75%',
    height: '60%'
  },
  buttonHeader: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
});

class IrFan extends PureComponent {
  constructor(props) {
    super(props);
    this.actionsheet = React.createRef()
    this.learnCommand = React.createRef()
  }

  render() {
    const {

    } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.rowHeader}>
          <View style={styles.buttonHeader}>
            <ButtonFan
              onPress={this.onPress}
              onLongPress={this.onLongPress}
              commandID={IR_COMMAND_FAN.fan_onOff}
              title='On/Off'
              iconName='power-settings-new'
            />
          </View>
          <View style={styles.row}>
            <ButtonFan
              onPress={this.onPress}
              onLongPress={this.onLongPress}
              commandID={IR_COMMAND_FAN.fan_timer}
              title='Timer'
              iconName='access-time'
            />
            <ButtonFan
              onPress={this.onPress}
              onLongPress={this.onLongPress}
              commandID={IR_COMMAND_FAN.fan_swing}
              title='Swing'
              iconName='undo'
            />
          </View>
          <View style={styles.row}>
            <ButtonFan
              onPress={this.onPress}
              onLongPress={this.onLongPress}
              commandID={IR_COMMAND_FAN.fan_auto}
              title='Auto'
              iconName='cached'
            />
            <ButtonFan
              onPress={this.onPress}
              onLongPress={this.onLongPress}
              commandID={IR_COMMAND_FAN.fan_speed}
              title='Speed'
              iconName='undo'
            />
          </View>
        </View>
        <ActionSheet
          options={this.getOptions()}
          bottomTitle={langs.cancel}
          ref={this.actionsheet}
        />
        <LearnCommand
          leftButton={{
            text: langs.cancel,
            onPress: this.onCancelLearnCommand
          }}
          title={langs.learning}
          ref={this.learnCommand}
        />
      </View>
    );
  }

  getOptions = () => {
    return [
      {
        title: langs.learnCommand,
        onPress: this.onLearnCommand
      },
      {
        title: langs.removeCommand,
        onPress: this.onRemoveCommand
      }
    ]
  }

  onLearnCommand = () => {
    this.learnCommand.current.open()
  }

  onCancelLearnCommand = () => {

  }

  onRemoveCommand = () => {

  }

  onPress = (irCommand) => {
    let val = {
      act: 'active',
      irid: ''
    }
    this.props.onControl && this.props.onControl(val)
  }

  onLongPress = (irCommand) => {
    this.actionsheet.current.show()
  }
}

IrFan.defaultProps = {

}

IrFan.propTypes = {
  onControl: PropTypes.func
}

export default IrFan;
