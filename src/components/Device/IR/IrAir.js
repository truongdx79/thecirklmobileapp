/**
* Created by bav on Tue Sep 04 2018
* Copyright (c) 2018 bav@luci.vn
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

class IrAir extends PureComponent {
  render() {
    const {
      
    } = this.props;
    return (
      <View style={styles.container}>
        
      </View>
    );
  }
}

IrAir.defaultProps = {
  
}

IrAir.propTypes = {
  
}

export default IrAir;
