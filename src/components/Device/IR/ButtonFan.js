/**
* Created by bav on Tue Sep 04 2018
* Copyright (c) 2018 bav@luci.vn
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text } from 'react-native';
import { MKButton } from 'react-native-material-kit';
import Icon from '../../Icon';
import { common } from '../../../config/common';

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 100,
    height: 100,
    borderRadius: 50,
    overflow: 'hidden'
  },
  title: {
    fontSize: 18,
    color: common().COLOR_TEXT_NORMAL
  }
});

class ButtonFan extends PureComponent<Props> {
  onPress = () => {
    this.props.onPress && this.props.onPress(this.props.commandID)
  }

  onLongPress = () => {
    this.props.onLongPress && this.props.onLongPress(this.props.commandID)
  }

  render() {
    const {
      iconName,
      title,
      delayLongPress,
      isActive,
      activeButtonColor,
      inactiveButtonColor
    } = this.props;
    return (
      <MKButton
        style={styles.container}
        onPress={this.onPress}
        onLongPress={this.onLongPress}
        delayLongPress={delayLongPress}
      >
        <Icon
          name={iconName}
          size={50}
          color={isActive ? activeButtonColor : inactiveButtonColor}
        />
        <Text
          style={[
            styles.title,
            {
              color: isActive ? activeButtonColor : inactiveButtonColor,
            }
          ]}
        >
          {title}
        </Text>
      </MKButton>
    );
  }
}

ButtonFan.defaultProps = {
  iconName: 'power-settings-new',
  activeButtonColor: common().BACKGROUND_COLOR_BUTTON_ACTIVE,
  inactiveButtonColor: common().COLOR_TEXT_ACTIVE,
  isActive: false,
  delayLongPress: 1000
}

interface Props {
  iconName?: string;
  title?: string;
  activeButtonColor?: string;
  inactiveButtonColor?: string;
  isActive?: boolean;
  onPress?: () => void;
  onLongPress: () => void;
  delayLongPress?: number;
  commandID?: string
}

export default ButtonFan;
