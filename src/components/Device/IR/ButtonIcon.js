/**
* Created by bav on Tue Sep 04 2018
* Copyright (c) 2018 bav@luci.vn
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet } from 'react-native';
import { common } from '../../../config/common';
import { MKButton } from 'react-native-material-kit';
import Icon from '../../Icon';

const styles = StyleSheet.create({
  button: {
    height: 40, 
    width: 40, 
    backgroundColor: 'rgba(255, 255, 255, 0.2)', 
    borderRadius: 20,
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center'
  }
});

class ButtonIcon extends PureComponent<Props> {
  onPress = () => {
    this.props.onPress && this.props.onPress(this.props.commandID)
  }

  onLongPress = () => {
    this.props.onLongPress && this.props.onLongPress(this.props.commandID)
  }
  
  render() {
    const {
      iconName,
      activeButtonColor,
      inactiveButtonColor,
      isActive,
      delayLongPress
    } = this.props;
    return (
      <MKButton
        style={[
          styles.button,
          { backgroundColor: isActive ? activeButtonColor : 'rgba(255, 255, 255, 0.2)' }
        ]}
        onPress={this.onPress}
        onLongPress={this.onLongPress}
        delayLongPress={delayLongPress}
      >
        <Icon
          name={iconName}
          size={30}
          color={isActive ? common().COLOR_TEXT_NORMAL : common().COLOR_TEXT_ACTIVE}
        />
      </MKButton>
    );
  }
}

ButtonIcon.defaultProps = {
  iconName: 'volume-mute',
  activeButtonColor: common().BACKGROUND_COLOR_BUTTON_ACTIVE,
  inactiveButtonColor: common().BACKGROUND_COLOR_BUTTON_INACTIVE,
  isActive: false,
  delayLongPress: 1000
}

interface Props {
  iconName?: string;
  activeButtonColor?: string;
  inactiveButtonColor?: string;
  isActive?: boolean;
  onPress?: () => void;
  onLongPress: () => void;
  delayLongPress?: number;
  commandID?: string
}

export default ButtonIcon;
