/**
* Created by bav on Sun Aug 05 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text, LayoutAnimation, Animated, Easing, ActivityIndicator } from 'react-native';
import { MKButton } from 'react-native-material-kit';
import { common } from '../../config/common';
import Icon from '../Icon'
import { CustomLayoutSpring } from '../../common/animation';
import { numberToString, stringToNumber } from '../../common/math';

const styles = StyleSheet.create({
  container: {

  },
  viewIcon: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 25
  },
  viewDevice: {
    overflow: 'hidden'
  },
  button: {
    flex: 1,
    marginBottom: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  viewButtonBottom: {

  },
  title: {
    fontSize: common().FONT_SIZE_HEADER,
    fontWeight: common().FONT_WEIGHT_HEADER
  },
  content: {
    fontSize: common().FONT_SIZE_TITLE,
    fontWeight: common().FONT_WEIGHT_TITLE,
    marginTop: 10
  },
  viewLoading: {
    width: 36,
    height: 36,
    justifyContent: 'center',
    alignItems: 'center'
  }
});

class Curtain extends PureComponent {
  constructor(props) {
    super(props);
    const defaultVal = {
      state: 'off',
      level: '0'
    }

    this.state = {
      val: props.val ? props.val : defaultVal
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.isControl) {
      if (nextProps.val !== this.state.val) {
        this.setState({
          val: nextProps.val
        })
      }
    }
  }

  onControl = (index) => {
    if (this.props.isControl) {
      let val = {
        level: "-1",
        state: index == 0 ? 'on' : index == 1 ? 'stop' : 'off'
      }
      this.props.onPress && this.props.onPress(val)
      this.props.onControl && this.props.onControl(val)
    } else {
      let { val } = this.state;

      if (index == 0) {
        if (stringToNumber(val.level) == 0) {
          val = {
            ...val,
            level: '0'
          }
        } else {
          val = {
            ...val,
            level: `${stringToNumber(val.level - 1)}`
          }
        }

        this.props.onChange && this.props.onChange(val)
        this.setState({ val })

        return;
      }

      if (index == 1) {
        val.state = 'stop'

        this.props.onChange && this.props.onChange(val)
        this.setState({ val })
        return;
      }

      if (index = 2) {
        if (stringToNumber(val.level) == 6) {
          val = {
            ...val,
            level: '6'
          }
        } else {
          val = {
            ...val,
            level: `${stringToNumber(val.level) + 1}`
          }
        }

        this.props.onChange && this.props.onChange(val)
        this.setState({ val })
        return;
      }
    }
  }

  render() {
    const {
      height,
      width,
      borderRadius,
      backgroundActive,
      backgroundInactive,
      showHeader,
      iconName,
      statusTitle,
      loading,
      colorIndicator,
      loadingTitle
    } = this.props;
    const { val } = this.state;
    const AnimatedComponent = Animated.createAnimatedComponent(Icon)
    return (
      <View style={styles.container}>
        {
          showHeader && (
            <View style={styles.viewIcon}>
              {
                loading && <View style={styles.viewLoading}>
                  <ActivityIndicator
                    size='small'
                    color={colorIndicator}
                    style={styles.viewIndicator}
                  />
                </View>
              }
              {
                !loading && (
                  <AnimatedComponent
                    name={iconName}
                    size={36}
                    color={backgroundActive}
                  />
                )
              }

              <Text style={[styles.content, { color: backgroundActive }]}>
                {loading ? loadingTitle : statusTitle}
              </Text>
            </View>
          )
        }

        <View
          style={[
            styles.viewDevice,
            {
              height,
              width,
              borderRadius
            }
          ]}
        >
          <MKButton
            style={[styles.button, styles.viewButtonTop, { backgroundColor: backgroundInactive }]}
            onPress={() => this.onControl(0)}
          >
            <Icon
              name='code'
              size={48}
              color='white'
            />
          </MKButton>
          <MKButton
            style={[styles.button, styles.viewButtonStop, { backgroundColor: backgroundActive }]}
            onPress={() => this.onControl(1)}
          >
            <Icon
              name='pause'
              size={48}
              color={common().STATUS_OFFLINE_COLOR}
            />
          </MKButton>
          <MKButton
            style={[styles.button, styles.viewButtonBottom, { backgroundColor: backgroundInactive }]}
            onPress={() => this.onControl(2)}
          >
            <Icon
              name='first-page'
              size={48}
              color='white'
            />
          </MKButton>
        </View>
      </View>
    );
  }
}

Curtain.defaultProps = {
  val: {
    level: '6',
    state: 'on'
  },
  height: 315,
  width: 124,
  borderRadius: 38,
  typeCurtain: 'horizontail',
  backgroundActive: common().BACKGROUND_COLOR_CARD,
  backgroundInactive: 'rgba(255, 255, 255, 0.1)',
  colorIndicator: '#fff',
  isControl: false,
  loading: false,
  showHeader: true,
  iconName: 'view-column',
  statusTitle: 'Level',
  loadingTitle: 'Loading...'
}

Curtain.propTypes = {
  val: PropTypes.shape({
    state: PropTypes.string,
    level: PropTypes.string,
    unit: PropTypes.string
  }),
  width: PropTypes.number,
  height: PropTypes.number,
  borderRadius: PropTypes.number,
  typeCurtain: PropTypes.oneOf(['horizontail', 'vertical']),
  backgroundActive: PropTypes.string,
  backgroundInactive: PropTypes.string,
  iconName: PropTypes.string,
  isControl: PropTypes.bool,
  loading: PropTypes.bool,
  showHeader: PropTypes.bool,
  statusTitle: PropTypes.string,
  onChange: PropTypes.func,
  onPress: PropTypes.func,
  colorIndicator: PropTypes.string,
  loadingTitle: PropTypes.string
}

export default Curtain;
