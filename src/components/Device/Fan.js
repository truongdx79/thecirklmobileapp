/**
* Created by bav on Tue Jul 31 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text, LayoutAnimation, Animated, Easing, ActivityIndicator } from 'react-native';
import { MKButton } from 'react-native-material-kit';
import { common } from '../../config/common';
import Icon from '../Icon'
import { CustomLayoutSpring } from '../../common/animation';
import { stringToNumber, numberToString } from '../../common/math';

const styles = StyleSheet.create({
  container: {

  },
  viewIcon: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 25
  },
  viewDevice: {
    overflow: 'hidden'
  },
  viewNote: {
    flex: 1,
    marginBottom: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  title: {
    fontSize: common().FONT_SIZE_HEADER,
    fontWeight: common().FONT_WEIGHT_HEADER
  },
  content: {
    fontSize: common().FONT_SIZE_TITLE,
    fontWeight: common().FONT_WEIGHT_TITLE,
    marginTop: 10
  },
  viewLoading: {
    width: 36,
    height: 36,
    justifyContent: 'center',
    alignItems: 'center'
  }
});

class Fan extends PureComponent<Props> {
  constructor(props) {
    super(props);
    const defaultVal = {
      state: 'off',
      level: 0
    }

    this.state = {
      val: props.val || defaultVal
    }
    this.speed = new Animated.Value(0)
    this.startRotate(stringToNumber(props.val.level) || 0)
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.isControl) {
      if (nextProps.val !== this.state.val) {
        this.startRotate(stringToNumber(nextProps.val.level))
        this.setState({
          val: nextProps.val
        })
      }
    }
  }

  startRotate = (value) => {
    let speed = value
    if (typeof value === String) {
      speed = value == 0 ? 0 : parseInt(value)
    }
    Animated.loop(
      Animated.timing(this.speed, {
        toValue: speed,
        duration: this.props.routeDuration,
        easing: Easing.linear
      })
    ).start()
  }

  onControl = (index) => {
    const val = {
      ...this.state.val,
      level: numberToString(index),
      state: numberToString(index) == 0 ? 'off' : 'on'
    }
    if (this.props.isControl) {
      this.props.onPress && this.props.onPress(val)
    } else {
      if (stringToNumber(this.state.val.level) !== index) {
        this.props.onChange && this.props.onChange(val)
        LayoutAnimation.configureNext(CustomLayoutSpring)
        this.setState({
          val: val
        })

        this.startRotate(index)
      }
    }
  }

  render() {
    const {
      height,
      width,
      borderRadius,
      numberOffSpeed,
      backgroundActive,
      backgroundInactive,
      showHeader,
      iconName,
      statusTitle,
      loading,
      colorIndicator,
      loadingTitle
    } = this.props;
    const { val } = this.state;
    const AnimatedComponent = Animated.createAnimatedComponent(Icon)
    return (
      <View style={styles.container}>
        {
          showHeader && (
            <View style={styles.viewIcon}>
              {
                loading && <View style={styles.viewLoading}>
                  <ActivityIndicator
                    size='small'
                    color={colorIndicator}
                    style={styles.viewIndicator}
                  />
                </View>
              }
              {
                !loading && (
                  <AnimatedComponent
                    style={{
                      transform: [
                        {
                          rotate: this.speed.interpolate({
                            inputRange: [0, 1],
                            outputRange: ['0deg', '360deg']
                          })
                        }
                      ]
                    }}
                    name={iconName}
                    size={36}
                    color={backgroundActive}
                  />
                )
              }

              <Text style={[styles.content, { color: backgroundActive }]}>
                {loading ? loadingTitle : statusTitle}
              </Text>
            </View>
          )
        }

        <View
          style={[
            styles.viewDevice,
            {
              height,
              width,
              borderRadius
            }
          ]}
        >
          {
            [...Array(numberOffSpeed || 0)].map((pan, index) => {
              return (
                <MKButton
                  key={index}
                  style={[
                    styles.viewNote,
                    {
                      backgroundColor: stringToNumber(val.level) === index ? backgroundActive : backgroundInactive
                    }
                  ]}
                  onPress={() => this.onControl(index)}
                >
                  <Text style={[styles.title, { color: stringToNumber(val.level) === index ? common().STATUS_OFFLINE_COLOR : backgroundActive }]}>
                    {index}
                  </Text>
                </MKButton>
              )
            })
          }
        </View>
      </View>
    );
  }
}

Fan.defaultProps = {
  val: {
    state: 'on',
    level: '2'
  },
  height: 315,
  width: 124,
  borderRadius: 38,
  numberOffSpeed: 4,
  backgroundActive: common().BACKGROUND_COLOR_CARD,
  backgroundInactive: 'rgba(255, 255, 255, 0.1)',
  colorIndicator: '#fff',
  isControl: false,
  loading: false,
  showHeader: true,
  iconName: 'toys',
  statusTitle: 'Speed',
  loadingTitle: 'Loading...',
  routeDuration: 4000
}

Fan.propTypes = {
  val: PropTypes.shape({
    state: PropTypes.string,
    level: PropTypes.string,
    unit: PropTypes.string
  }),
  width: PropTypes.number,
  height: PropTypes.number,
  borderRadius: PropTypes.number,
  numberOffSpeed: PropTypes.number,
  backgroundActive: PropTypes.string,
  backgroundInactive: PropTypes.string,
  iconName: PropTypes.string,
  isControl: PropTypes.bool,
  loading: PropTypes.bool,
  showHeader: PropTypes.bool,
  statusTitle: PropTypes.string,
  onChange: PropTypes.func,
  onPress: PropTypes.func,
  colorIndicator: PropTypes.string,
  loadingTitle: PropTypes.string,
  routeDuration: PropTypes.number
}

type valStatic = {
  state?: 'on' | 'off',
  level?: string,
  unit?: string
}

interface Props {
  val?: valStatic;
  width?: number;
  height?: number;
  borderRadius?: number;
  numberOffSpeed?: number;
  backgroundActive?: string;
  backgroundInactive?: string;
  iconName?: string;
  isControl?: boolean;
  loading?: boolean;
  showHeader?: boolean;
  statusTitle?: string;
  onChange?: (val: valStatic) => void;
  onPress?: (val: valStatic) => void;
  colorIndicator?: string;
  loadingTitle?: string;
  routeDuration?: number;
}

export default Fan;
