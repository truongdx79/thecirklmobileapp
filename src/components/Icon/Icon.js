/**
* Created by bav on Fri Jul 27 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text } from 'react-native';
import IconVector from 'react-native-vector-icons/dist/MaterialIcons';

class Icon extends PureComponent<Props> {
  render() {
    return (
      <IconVector 
        {...this.props}
      />
    );
  }
}

Icon.defaultProps = {
  
}

Icon.propTypes = {
  
}

interface Props {
  name: string;
  size?: number;
  color?: string;
  style?: StyleProp<ViewStyle>;
}

export default Icon;
