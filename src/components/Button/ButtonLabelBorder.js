/**
* Created by bav on Mon Jul 23 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, ViewPropTypes, Text } from 'react-native';
import { MKButton } from 'react-native-material-kit';
import { common } from '../../config/common';

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
    paddingHorizontal: 10
  },
  title: {
    fontSize: common().FONT_SIZE_TITLE,
    fontWeight: common().FONT_WEIGHT_TITLE
  }
});

class ButtonLabelBorder extends PureComponent<Props> {
  render() {
    let {
      title,
      onPress,
      height,
      style,
      backgroundColor,
      titleColor,
      titleStyle,
      borderRadius,
      disable
    } = this.props;
    const Component = disable ? View : MKButton
    return (
      <Component
        style={[
          styles.container,
          backgroundColor ? { backgroundColor } : undefined,
          borderRadius ? { borderRadius: height / 2 } : undefined,
          { height },
          disable ? { opacity: 0.6 } : undefined,
          style
        ]}
        onPress={onPress}
      >
        <Text style={[styles.title, titleStyle, titleColor ? { color: titleColor } : undefined]}>
          {` ${title} `}
        </Text>
      </Component>
    );
  }
}

ButtonLabelBorder.defaultProps = {
  titleColor: common().COLOR_TEXT_NORMAL,
  backgroundColor: 'rgba(255, 255, 255, 0.1)',
  borderRadius: true,
  height: 25,
  disable: false
}

ButtonLabelBorder.propTypes = {
  title: PropTypes.string.isRequired,
  height: PropTypes.number,
  onPress: PropTypes.func,
  disable: PropTypes.bool,
  style: ViewPropTypes.style,
  backgroundColor: PropTypes.string,
  titleColor: PropTypes.string,
  titleStyle: Text.propTypes.style,
  borderRadius: PropTypes.bool
}

interface Props {
  backgroundColor?: string;
  title: string;
  style?: StyleProp<ViewStyle>;
  titleStyle?: StyleProp<TextStyle>;
  titleColor?: string;
  borderRadius?: boolean;
  /**
   * default value 25
   */
  height?: number;
  onPress?: () => void;
  disable?: boolean;
}

export default ButtonLabelBorder;
