/**
* Created by bav on Thu Jun 14 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text, ViewPropTypes, Dimensions, ActivityIndicator } from 'react-native';
import { MKButton } from 'react-native-material-kit';
import { common } from '../../config/common';
import { isTablet } from '../../common/utils';

const SCREEN = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
    flexDirection: 'row',
  },
  title: {
    fontSize: common().FONT_SIZE_TITLE,
    fontWeight: common().FONT_WEIGHT_HEADER
  },
  disable: {
    opacity: 0.8
  }
});

class ButtonOverlay extends PureComponent<Props> {
  render() {
    let {
      title,
      backgroundColor,
      style,
      titleStyle,
      color,
      rounded,
      borderRadius,
      full,
      width,
      height,
      onPress,
      disable,
      loading
    } = this.props;
    let widthButton = full ? SCREEN.width : Math.min(width, SCREEN.width - 32)
    const ComponentButton = disable ? View : MKButton;

    return (
      <ComponentButton
        style={[
          styles.container,
          {
            width: widthButton,
            height,
            backgroundColor
          },
          style,
          rounded ? { borderRadius: borderRadius } : undefined,
          disable ? style.disable : undefined
        ]}
        onPress={!loading ? onPress : undefined}
      >
        {
          loading && <ActivityIndicator size='small' />
        }
        <Text numberOfLines={1} style={[styles.title, { color }, titleStyle]}>
          {` ${title} `}
        </Text>
      </ComponentButton>
    );
  }
}

ButtonOverlay.defaultProps = {
  backgroundColor: common().CARD_BACKGROUND_COLOR,
  color: common().COLOR_TEXT_NORMAL,
  rounded: true,
  borderRadius: 40,
  full: false,
  width: isTablet ? (SCREEN.width / 2 - 32) : (SCREEN.width - 32),
  height: 55,
  disable: false,
  loading: false
}

ButtonOverlay.propTypes = {
  title: PropTypes.string,
  backgroundColor: PropTypes.string,
  style: ViewPropTypes.style,
  titleStyle: Text.propTypes.style,
  color: PropTypes.string,
  rounded: PropTypes.bool,
  borderRadius: PropTypes.number,
  full: PropTypes.bool,
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  height: PropTypes.number,
  onPress: PropTypes.func,
  disable: PropTypes.bool,
  loading: PropTypes.bool,
}

interface Props {
  title: string;
  backgroundColor?: string;
  style?: StyleProp<ViewStyle>;
  titleStyle?: StyleProp<TextStyle>;
  color?: string;
  rounded?: boolean;
  borderRadius?: number;
  /**
   * set width button = full Dimensions if full = true
   * default value false
   */
  full?: boolean;
  /**
   * default value SCREEN.width - 32
   */
  width?: number | string;
  /**
   * default value 54
   */
  height?: number;
  onPress?: () => void;
  disable?: boolean;
  loading?: boolean;
}

export default ButtonOverlay;
