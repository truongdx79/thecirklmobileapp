/**
* Created by bav on Thu Jun 14 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text, ViewPropTypes, Dimensions } from 'react-native';
import { MKButton } from 'react-native-material-kit';
import { common } from '../../config/common';

const SCREEN = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden'
  },
  title: {
    fontSize: common().FONT_SIZE_TITLE,
    fontWeight: common().FONT_WEIGHT_HEADER,
  },
  disable: {
    opacity: 0.8
  }
});

class Button extends PureComponent<Props> {
  render() {
    let {
      title,
      backgroundColor,
      style,
      titleStyle,
      color,
      rounded,
      borderRadius,
      full,
      wrapContent,
      width,
      height,
      onPress,
      disable
    } = this.props;
    let widthButton = full ? SCREEN.width : Math.min(width, SCREEN.width - 24)
    widthButton = wrapContent ? undefined : widthButton
    const ComponentButton = disable ? View : MKButton;

    return (
      <ComponentButton
        style={[
          styles.container,
          {
            width: widthButton,
            height,
            backgroundColor
          },
          style,
          rounded ? { borderRadius: borderRadius } : undefined,
          disable ? style.disable : undefined,
          wrapContent ? { paddingLeft: 10, paddingRight: 10 } : undefined
        ]}
        onPress={onPress}
      >
        <Text numberOfLines={1} style={[styles.title, { color }, titleStyle]}>
          {` ${title} `}
        </Text>
      </ComponentButton>
    );
  }
}

Button.defaultProps = {
  backgroundColor: common().BACKGROUND_COLOR_BUTTON_ACTIVE,
  color: 'white',
  rounded: false,
  borderRadius: 50,
  full: false,
  width: SCREEN.width - 32,
  height: 55,
  disable: false
}

Button.propTypes = {
  title: PropTypes.string,
  backgroundColor: PropTypes.string,
  style: ViewPropTypes.style,
  titleStyle: Text.propTypes.style,
  color: PropTypes.string,
  rounded: PropTypes.bool,
  borderRadius: PropTypes.number,
  full: PropTypes.bool,
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  height: PropTypes.number,
  onPress: PropTypes.func,
  disable: PropTypes.bool
}

interface Props {
  backgroundColor?: string;
  title: string;
  style?: StyleProp<ViewStyle>;
  titleStyle?: StyleProp<TextStyle>;
  color?: string;
  rounded?: boolean;
  borderRadius?: number;
  /**
   * set width button = full Dimensions if full = true
   * default value false
   */
  full?: boolean;
  wrapContent?: boolean;
  /**
   * default value Math.min(300, SCREEN.width - 24)
   */
  width?: number | string;
  /**
   * default value 40
   */
  height?: number;
  onPress?: () => void;
  disable?: boolean;
}

export default Button;
