import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  ViewPropTypes,
} from 'react-native';
import { common } from '../../config/common';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent'
  },
  viewButton: {
    paddingRight: 24,
    paddingVertical: 8
  },
  disable: {
    opacity: 0.8
  },
  txtTitle: {
    fontSize: common().FONT_SIZE_CONTENT,
    fontWeight: common().FONT_WEIGHT_HEADER,
    color: common().COLOR_TEXT_ACTIVE,
    backgroundColor: 'transparent'
  },
  txtFirstTitle: {
    fontSize: common().FONT_SIZE_CONTENT,
    fontWeight: common().FONT_WEIGHT_TITLE,
    color: common().COLOR_TEXT_ACTIVE,
    backgroundColor: 'transparent'
  }
})

const ButtonLabel = props => {
  let {
    disable,
    title,
    firstTitle,
    style,
    titleStyle,
    firstTitleStyle,
    onPress,
    loading
  } = props;

  const Component = disable || !onPress ? View : TouchableOpacity

  return (
    <View style={styles.container}>
      {
        firstTitle && (
          <Text style={[styles.txtFirstTitle, firstTitleStyle]}>
            {`${firstTitle} `}
          </Text>
        )
      }
      <Component
        style={[
          styles.viewButton,
          style,
          disable ? styles.disable : undefined,
          firstTitle ? undefined : { paddingLeft: 24 }
        ]}
        onPress={onPress}
      >
        <Text numberOfLines={1} style={[styles.txtTitle, titleStyle]}>
          {title}
        </Text>
      </Component>
    </View>
  );
}

ButtonLabel.defaultProps = {
  disable: false,
  onPress: undefined,
  style: null,
  titleStyle: null,
  firstTitle: null
}

ButtonLabel.propTypes = {
  title: PropTypes.string.isRequired,
  firstTitle: PropTypes.string,
  onPress: PropTypes.func,
  disable: PropTypes.bool,
  style: ViewPropTypes.style,
  titleStyle: Text.propTypes.style,
  firstTitleStyle: Text.propTypes.style
}

export default ButtonLabel;
