/**
* Created by bav on Sat Jul 07 2018
* Copyright (c) 2018 bav
*/

import Swiper from './lib';
import SwiperWithOverlay from './lib/SwiperWithOverlay';

export {
  Swiper,
  SwiperWithOverlay
}
