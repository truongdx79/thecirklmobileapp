/**
* Created by bav on Sat Jul 07 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text, Animated, ViewPropTypes, PanResponder } from 'react-native';
import Interactable from 'react-native-interactable';

const styles = StyleSheet.create({
  container: {
    
  },
  viewContent: {
    position: 'relative',
    overflow: 'hidden'
  },
  viewControl: {
    elevation: 2,
    shadowOffset: {
      width: 2,
      height: 2
    },
    shadowRadius: 2,
    shadowOpacity: 0.6
  }
});

class Swiper extends PureComponent<Props> {
  constructor(props) {
    super(props);
    this.scale = new Animated.Value(1)
    this.enableAlertEvent = false
    this.currentValue = null
    this.refControl = React.createRef()
  }

  componentWillMount() {
    this._panResponder = PanResponder.create({
      onStartShouldSetPanResponder: () => this.props.scaleWhenPress,
      onStartShouldSetPanResponderCapture: () => this.props.scaleWhenPress,
      onMoveShouldSetPanResponder: () => this.props.scaleWhenPress,
      onPanResponderGrant: this._onPanResponderGrant,
      onPanResponderRelease: this._onPanResponderRelease
    })
  }

  _onPanResponderGrant = () => {
    Animated.spring(this.scale, {
      toValue: this.props.scale,
      friction: 300
    }).start()
  }

  _onSnap = ({ nativeEvent }) => {
    this.props.onValueChange && this.props.onValueChange(this.getValueFromIndex(nativeEvent.index))
    this.props.onSwiperComplete && this.props.onSwiperComplete(this.getValueFromIndex(nativeEvent.index))
  }

  _onDrag = ({ nativeEvent }) => {
    this.enableAlertEvent = true
    if (nativeEvent.state === 'end') {
      this.enableAlertEvent = false
      this.props.scaleWhenPress && this.resetScale()

      if (nativeEvent.y === 0) {
        if ((this.currentValue && (this.getValue(this.currentValue) !== 0)) || this.currentValue == null) {
          this.props.onValueChange && this.props.onValueChange(this.getValueFromIndex(0))
        }
      }
    }
  }

  getValue = (val) => {
    const { minLevel, maxLevel, step } = this.props;
    return (val == '0' ? 0 : parseInt(val)) + minLevel
  }

  getValueFromIndex = (index) => {
    const { minLevel, maxLevel, step } = this.props;
    return (minLevel + index * step)
  }

  _onAlertEvent = ({ nativeEvent }) => {
    if (!this.enableAlertEvent) return
    for (i = 0; i < Object.keys(nativeEvent).length; i++) {
      for (key in Object.keys(nativeEvent)) {
        if (Object.keys(nativeEvent)[key].includes(`alertPoint`)) {
          if (!this.currentValue) {
            this.currentValue = Object.keys(nativeEvent)[key].slice(10)
            this.props.onValueChange && this.props.onValueChange(this.getValue(this.currentValue))
          } else {
            if (this.currentValue == Object.keys(nativeEvent)[key].slice(10)) {
              return
            } else {
              this.currentValue = Object.keys(nativeEvent)[key].slice(10)
              this.props.onValueChange && this.props.onValueChange(this.getValue(this.currentValue))
              if (this.currentValue == 0) {
                this.props.onSwiperComplete && this.props.onSwiperComplete(this.getValueFromIndex(0))
              }
            }
          }
        }
      }
    }
  }

  _onPanResponderRelease = () => {
    this.resetScale()
  }

  resetScale = () => {
    Animated.spring(this.scale, {
      toValue: 1,
      friction: 100
    }).start()
  }

  getSnapPoints = () => {
    const { height, minLevel, maxLevel, step } = this.props;
    let snapPoints = []
    for (i = 0; i <= (maxLevel - minLevel); i = i + step) {
      const point = { y: -i * height / (maxLevel - minLevel) }
      snapPoints.push(point)
    }
    for (i = 0; i <= (maxLevel - minLevel); i = i + step) {
      const point = { y: i * height / (maxLevel - minLevel) }
      snapPoints.push(point)
    }

    return snapPoints
  }

  getAlertAreas = () => {
    const { height, minLevel, maxLevel, step } = this.props;
    let alertAreas = []
    for (i = 0; i <= (maxLevel - minLevel); i = i + step) {
      const alertPoint = { id: `alertPoint${i}`, influenceArea: { top: -i * height / (maxLevel - minLevel) }, type: i }
      alertAreas.push(alertPoint)
    }
    for (i = 0; i <= (maxLevel - minLevel); i = i + step) {
      const alertPoint = { id: `alertPoint${-i}`, influenceArea: { top: i * height / (maxLevel - minLevel) }, type: i }
      alertAreas.push(alertPoint)
    }

    return alertAreas
  }

  getBoundaries = () => {
    const { height, minLevel, maxLevel } = this.props;

    return { bottom: 0, top: -height }
  }

  getInitialPosition = () => {
    const { height, minLevel, maxLevel, initialValue } = this.props;

    return { y: -initialValue * height / (maxLevel - minLevel) }
  }

  snapToValue = (value) => {
    const { minLevel, step } = this.props;

    this.refControl && this.refControl.current.snapTo({ index: Math.floor((value - minLevel) / step) })
  }

  render() {
    const {
      height,
      width,
      backgroundControl,
      backgroundColor,
      borderRadius,
      onlyEventWhenStop,
      styleContainer,
      styleControl
    } = this.props;

    return (
      <View style={styles.container}>
        <Animated.View
          {...this._panResponder.panHandlers}
          style={[
            styles.viewContent,
            styleContainer,
            {
              height,
              width,
              backgroundColor,
              borderRadius,
              transform: [
                { scale: this.scale }
              ]
            }
          ]}
        >
          <Interactable.View
            ref={this.refControl}
            verticalOnly={true}
            animatedNativeDriver
            snapPoints={this.getSnapPoints()}
            boundaries={this.getBoundaries()}
            initialPosition={this.getInitialPosition()}
            onSnap={this._onSnap}
            onDrag={this._onDrag}
            alertAreas={onlyEventWhenStop ? undefined : this.getAlertAreas()}
            onAlert={onlyEventWhenStop ? undefined : this._onAlertEvent}
          >
            <View style={{ width, height }} />
            <View pointerEvents='none' style={[styles.viewControl, styleControl, { width, height, backgroundColor: backgroundControl, shadowColor: backgroundControl }]} />
          </Interactable.View>
        </Animated.View>
      </View>
    );
  }
}

Swiper.defaultProps = {
  height: 315,
  width: 124,
  backgroundControl: 'white',
  backgroundColor: 'rgba(0, 0, 0, 0.4)',
  borderRadius: 38,
  onlyEventWhenStop: true,
  minLevel: 0,
  maxLevel: 100,
  step: 1,
  initialValue: 0,
  scaleWhenPress: true,
  scale: 1.1
}

Swiper.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number.isRequired,
  backgroundControl: PropTypes.string,
  backgroundColor: PropTypes.string,
  borderRadius: PropTypes.number,
  onlyEventWhenStop: PropTypes.bool,
  minLevel: PropTypes.number,
  maxLevel: PropTypes.number,
  step: PropTypes.number,
  initialValue: PropTypes.number,
  onValueChange: PropTypes.func,
  onSwiperComplete: PropTypes.func,
  scaleWhenPress: PropTypes.bool,
  scale: PropTypes.number,
  styleContainer: ViewPropTypes.style,
  styleControl: ViewPropTypes.style
}

interface Props {
  /**
   * default width = 124
   */
  width?: number;
  /**
   * default width = 315
   */
  height?: number;
  backgroundControl?: string;
  backgroundColor?: string;
  /**
   * default borderRadius = 38
   */
  borderRadius?: number;
  /**
   * Only callback event when stop touch. Default true
   */
  onlyEventWhenStop?: boolean;
  /**
   * default minLevel = 0
   */
  minLevel?: number;
  /**
   * default maxLevel = 100
   */
  maxLevel?: number;
  /**
   * default step = 1
   */
  step?: number;
  /**
   * default initialValue = 0
   */
  initialValue?: number;
  /**
   * call when change step
   */
  onValueChange?: (step: number) => void;
  /**
   * call when stop touch
   */
  onSwiperComplete?: (step: number) => void;
  /**
   * Enable animated scale when press. Default true
   */
  scaleWhenPress?: boolean;
  /**
   * default scale = 1.1
   */
  scale?: number;
  styleContainer?: StyleProp<ViewStyle>;
  styleControl?: StyleProp<ViewStyle>;
}

export default Swiper;
