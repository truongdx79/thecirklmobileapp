/**
* Created by bav on Tue Jul 31 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, ViewPropTypes } from 'react-native';
import { MKButton } from 'react-native-material-kit';
import Swiper from './index';
import Overlay from './Overlay';

const styles = StyleSheet.create({
  container: {

  },
  button: {
    overflow: 'hidden'
  }
});

class SwiperWithOverlay extends PureComponent<Props> {
  constructor(props) {
    super(props);
    this.state = {
      buttonRect: {},
      isOpen: false,
    }

    this.button = React.createRef();
  }

  onOpen = () => {
    if (this.button) {
      this.button.current.measure((ox, oy, width, height, px, py) => {
        this.setState({
          buttonRect: { x: px, y: py, width: width, height: height }
        }, () => {
          this.setState({ isOpen: true })
        });
      });
    }
  }

  render() {
    const { renderHeader, renderFooter, styleButton } = this.props;
    const { isOpen, buttonRect } = this.state;
    return (
      <View
        ref={this.button}
        renderToHardwareTextureAndroid
        style={this.props.style}
        onLayout={() => { }}
      >
        <MKButton
          onPress={this.onOpen}
          style={[styles.button, styleButton]}
        >
          {this.props.children}
        </MKButton>
        <Overlay
          isOpen={isOpen}
          origin={buttonRect}
          onClose={() => this.setState({ isOpen: false })}
          touchToDismiss
          blurOverlay
          renderHeader={renderHeader}
          renderFooter={renderFooter}
        >
          <Swiper
            {...this.props}
          />
        </Overlay>
      </View>
    );
  }
}

SwiperWithOverlay.defaultProps = {
  height: 315,
  width: 124,
  backgroundControl: 'white',
  backgroundColor: 'rgba(0, 0, 0, 0.4)',
  borderRadius: 38,
  onlyEventWhenStop: true,
  minLevel: 0,
  maxLevel: 100,
  step: 1,
  initialValue: 0,
  scaleWhenPress: true,
  scale: 1.1
}

SwiperWithOverlay.propTypes = {
  style: ViewPropTypes.style,
  width: PropTypes.number,
  height: PropTypes.number.isRequired,
  backgroundControl: PropTypes.string,
  backgroundColor: PropTypes.string,
  borderRadius: PropTypes.number,
  onlyEventWhenStop: PropTypes.bool,
  minLevel: PropTypes.number,
  maxLevel: PropTypes.number,
  step: PropTypes.number,
  initialValue: PropTypes.number,
  onValueChange: PropTypes.func,
  onSwiperComplete: PropTypes.func,
  scaleWhenPress: PropTypes.bool,
  scale: PropTypes.number,
  styleContainer: ViewPropTypes.style,
  styleControl: ViewPropTypes.style,
  renderHeader: PropTypes.func,
  renderFooter: PropTypes.func,
  styleButton: ViewPropTypes.style
}

interface Props {
  /**
   * default width = 124
   */
  width?: number;
  /**
   * default width = 315
   */
  height?: number;
  backgroundControl?: string;
  backgroundColor?: string;
  /**
   * default borderRadius = 38
   */
  borderRadius?: number;
  /**
   * Only callback event when stop touch. Default true
   */
  onlyEventWhenStop?: boolean;
  /**
   * default minLevel = 0
   */
  minLevel?: number;
  /**
   * default maxLevel = 100
   */
  maxLevel?: number;
  /**
   * default step = 1
   */
  step?: number;
  /**
   * default initialValue = 0
   */
  initialValue?: number;
  /**
   * call when change step
   */
  onValueChange?: (step: number) => void;
  /**
   * call when stop touch
   */
  onSwiperComplete?: (step: number) => void;
  /**
   * Enable animated scale when press. Default true
   */
  scaleWhenPress?: boolean;
  /**
   * default scale = 1.1
   */
  scale?: number;
  styleContainer?: StyleProp<ViewStyle>;
  styleControl?: StyleProp<ViewStyle>;
  renderHeader?: () => void;
  renderFooter?: () => void;
}

export default SwiperWithOverlay;
