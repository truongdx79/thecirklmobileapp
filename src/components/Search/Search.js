/**
* Created by bav on Thu Aug 02 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet, View, Text, TextInput, Dimensions,
  SafeAreaView, FlatList, Platform
} from 'react-native';
import { MKButton } from 'react-native-material-kit';
import { ButtonIcon } from '../Button';
import Overlay from './Overlay';
import { BlurView } from 'react-native-blur';
import { common } from '../../config/common';

const SCREEN = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {

  },
  absolute: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  },
  viewSearchContainer: {
    flex: 1,
    alignItems: 'center'
  },
  viewSearch: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 24,
    paddingRight: 16,
    overflow: 'hidden',
    marginBottom: 3,
    marginTop: Platform.OS === 'ios' ? 16 : 32
  },
  textInput: {
    flex: 1,
    fontFamily: 'Quicksand',
    fontSize: common().FONT_SIZE_TITLE,
    fontWeight: common().FONT_WEIGHT_TITLE,
    color: common().INPUT_COLOR_TEXT
  },
  viewFlatlist: {
    marginTop: 20,
    borderRadius: 10,
    overflow: 'hidden'
  },
  viewRowSearch: {
    paddingHorizontal: 25,
    justifyContent: 'center'
  },
  title: {
    fontSize: common().FONT_SIZE_TITLE,
    fontWeight: common().FONT_WEIGHT_TITLE,
    color: common().INPUT_COLOR_TEXT
  },
  separator: {
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(255, 255, 255, 0.2)'
  }
});

class Search extends PureComponent<Props> {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      buttonRect: {}
    }

    this.button = React.createRef()
    this.refOverlay = React.createRef()
  }

  onPress = () => {
    if (this.button) {
      this.button.current.measure((ox, oy, width, height, px, py) => {
        this.setState({
          buttonRect: { x: px, y: py, width: width, height: height }
        }, () => {
          this.setState({ show: true })
        });
      });
    }
  }

  onSearch = () => {
    this.props.onSearch && this.props.onSearch()
  }

  closeSearch = () => {
    this.refOverlay && this.refOverlay.current.close()
  }

  renderSearchItem = ({ item, index }) => {
    const { blurOverlay, widthInput, searchData } = this.props;
    return (
      <MKButton
        style={[
          styles.viewRowSearch,
          {
            height: 50,
            width: widthInput,
            backgroundColor: 'transparent'
          },
          index !== searchData.length - 1 ? styles.separator : undefined
        ]}
        onPress={() => {
          this.refOverlay && this.refOverlay.current.close()
          this.props.onPressSearchItem && this.props.onPressSearchItem(item, index)
        }}
      >
        {
          blurOverlay && (
            <BlurView
              style={styles.absolute}
              blurType="light"
              blurAmount={Platform.OS === 'ios' ? 30 : 80}
              blurRadius={Platform.OS === 'ios' ? 3 : 20}
            />
          )
        }
        <View style={[styles.absolute, { backgroundColor: 'rgba(0, 0, 0, 0.26)' }]} />
        <Text style={styles.title}>
          {item.title}
        </Text>
      </MKButton>
    )
  }

  render() {
    const {
      style,
      placeholder,
      widthInput,
      heightInput,
      inputBackgroundColor,
      blurOverlay,
      searchData,
      ...otherProps
    } = this.props;
    const { show, buttonRect } = this.state;

    return (
      <View
        ref={this.button}
        renderToHardwareTextureAndroid
        style={[style, { borderRadius: 10 }]}
      >
        <ButtonIcon
          iconName='search'
          onPress={this.onPress}
        />
        <Overlay
          isOpen={show}
          origin={buttonRect}
          onClose={() => this.setState({ show: false })}
          ref={this.refOverlay}
          touchToDismiss
          blurOverlay
        >
          <SafeAreaView style={styles.viewSearchContainer}>
            <View
              style={[
                styles.viewSearch,
                {
                  width: widthInput,
                  height: heightInput,
                  backgroundColor: inputBackgroundColor,
                  borderRadius: heightInput / 2
                }
              ]}
            >
              {
                blurOverlay && (
                  <BlurView
                    style={styles.absolute}
                    blurType="light"
                    blurAmount={Platform.OS === 'ios' ? 30 : 80}
                    blurRadius={Platform.OS === 'ios' ? 3 : 20}
                  />
                )
              }
              <View style={[styles.absolute, { backgroundColor: 'rgba(0, 0, 0, 0.26)' }]} />
              <TextInput
                {...otherProps}
                underlineColorAndroid='transparent'
                autoCorrect={false}
                placeholder={placeholder}
                style={styles.textInput}
                autoFocus
                numberOfLines={1}
                multiline={false}
                placeholderTextColor={common().INPUT_COLOR_TEXT}
                selectionColor={common().INPUT_SELECTION_COLOR}
              />
              <ButtonIcon
                iconName='search'
                onPress={this.onSearch}
              />
            </View>

            {
              searchData.length > 0 && (
                <FlatList
                  data={searchData}
                  keyExtractor={(item, index) => String(index)}
                  renderItem={this.renderSearchItem}
                  extraData={this.state}
                  contentContainerStyle={styles.viewFlatlist}
                  keyboardShouldPersistTaps='always'
                />
              )
            }
          </SafeAreaView>
        </Overlay>
      </View>
    );
  }
}

Search.defaultProps = {
  placeholder: '',
  widthInput: Math.min(342, SCREEN.width - 32),
  heightInput: 45,
  inputBackgroundColor: 'transparent',
  blurOverlay: true,
  searchData: []
}

Search.propTypes = {
  placeholder: PropTypes.string,
  inputBackgroundColor: PropTypes.string,
  widthInput: PropTypes.number,
  heightInput: PropTypes.number,
  blurOverlay: PropTypes.bool,
  searchData: PropTypes.array,
  onSearch: PropTypes.func
}

type SearchData = Array<{
  title: string,
  onPressSearchItem?: (item: string, index: number) => void
}>;

interface Props {
  placeholder?: string;
  inputBackgroundColor?: string;
  /**
   * default value Math.min(342, SCREEN.width - 32)
   */
  widthInput?: number;
  /**
   * default value 45
   */
  heightInput?: number;
  /**
   * Enable blur overlay. Default true
   */
  blurOverlay?: boolean;
  /**
   * array data return when search
   */
  searchData?: SearchData;
  /**
   * call when press icon search
   */
  onSearch?: () => void;
}

export default Search;
