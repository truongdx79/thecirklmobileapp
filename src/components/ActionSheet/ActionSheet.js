import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
  TextPropTypes,
  Dimensions,
  Image
} from 'react-native'
import Modal from 'react-native-modal';
import { BlurView } from 'react-native-blur';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import { common } from '../../config/common';
import { imgs } from '../../config/imgs';
import DeviceInfo from 'react-native-device-info';

export const isTablet = DeviceInfo.isTablet();

const width = Dimensions.get('window').width;
const HeightActionSheet = isTablet ? 65 : 52;

const styles = StyleSheet.create({
  modal: {
    width: isTablet ? 512 : Math.min(400, width - 40),
    borderRadius: 12,
    justifyContent: 'flex-end',
    // alignItems: isTablet ? 'flex-end' : null,
    backgroundColor: 'transparent',
    overflow: 'hidden',
    position: isTablet ? 'absolute' : null,
    right: isTablet ? -43 : null,
    bottom: isTablet ? -20 : null,
  },
  viewTop: {
    borderRadius: 12,
    // backgroundColor: common().ACTION_SHEET_BACKGROUND_COLOR,
    overflow: 'hidden'
  },
  containerTitle: {
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: common().ACTION_SHEET_COLOR_SEPARATOR,
    paddingBottom: 12
  },
  txtTitle: {
    color: common().ACTION_SHEET_COLOR_TEXT_ACTIVE,
    fontSize: isTablet ? common().FONT_SIZE_TITLE_TABLET : common().FONT_SIZE_TITLE,
    fontWeight: '500',
    marginTop: 12,
  },
  txtMessage: {
    color: common().ACTION_SHEET_COLOR_TEXT,
    marginBottom: 10,
    marginTop: 4,
    fontSize: 13
  },
  containerMiddle: {

  },
  viewOption: {
    height: HeightActionSheet,
    marginTop: StyleSheet.hairlineWidth,
  },
  btnOptions: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    backgroundColor: common().ACTION_SHEET_BACKGROUND_COLOR
  },
  txtOptions: {
    color: common().ACTION_SHEET_COLOR_TEXT,
    fontSize: isTablet ? common().FONT_SIZE_TITLE_TABLET : common().FONT_SIZE_TITLE,
  },
  viewSeparator: {
    height: StyleSheet.hairlineWidth,
    backgroundColor: common().ACTION_SHEET_COLOR_SEPARATOR
  },
  btnBottom: {
    height: HeightActionSheet,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 12,
    overflow: 'hidden',
    backgroundColor: common().ACTION_SHEET_BACKGROUND_COLOR,
    marginTop: 12,
    marginBottom: isTablet ? 0 : 24
  },
  txtBottom: {
    color: common().ACTION_SHEET_COLOR_TEXT_ACTIVE,
    fontSize: isTablet ? common().FONT_SIZE_TITLE_TABLET : common().FONT_SIZE_TITLE,
    fontWeight: 'bold'
  },
  viewContentRow: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    alignSelf: 'flex-start'
  },
  iconLeft: {
    marginHorizontal: 16
  },
  iconCheck: {
    position: 'absolute',
    right: 12,
    alignSelf: 'center'
  },
  absolute: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  }
})

class ActionSheet extends Component<Props> {
  constructor(props) {
    super(props)
    this.heightModal = null,
      this.state = {
        showModal: false
      }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isOpen !== this.props.isOpen) {
      this.setState({
        showModal: nextProps.isOpen
      })
    }
  }

  componentWillUnmount() {
    if (this.timoutPressButton) {
      clearTimeout(this.timoutPressButton)
    }
  }

  handlePressBottomButton() {
    this.hide()
  }

  _dismissModal() {
    this.hide()
  }

  show = () => {
    this.setState({
      showModal: true
    })
  }

  hide = () => {
    this.setState({
      showModal: false
    }, () => {
      this.props.onHide && this.props.onHide()
    })
  }

  render() {
    let {
      title,
      message,
      options,
      titleStyle,
      messageStyle,
      bottomTitle,
      bottomStyle,
      renderTitle,
      renderBottomButton,
      renderOption,
      isOpen,
      refActionSheet,
      renderCheckSelect,
      checkSelect,
      colorCheckSelect,
      ...otherProps
    } = this.props

    const renderTitleTop = (
      <View style={styles.containerTitle}>
        <Text numberOfLines={1} style={[styles.txtTitle, titleStyle ? titleStyle : undefined]}>
          {title}
        </Text>
        {
          message ? (
            <Text numberOfLines={1} style={[styles.txtMessage, messageStyle ? messageStyle : undefined]}>
              {message}
            </Text>
          ) : null
        }
      </View>
    )

    const renderMiddle = (
      <View style={styles.containerMiddle}>
        {
          options.map((item, idx) => (
            <View key={idx} style={styles.viewOption}>
              <TouchableOpacity
                style={renderOption ? { flex: 1 } : styles.btnOptions}
                activeOpacity={0.8}
                onPress={() => {
                  this._dismissModal();

                  if (this.timoutPressButton) {
                    clearTimeout(this.timoutPressButton)
                  }

                  this.timoutPressButton = setTimeout(() => {
                    requestAnimationFrame(() => {
                      item.onPress()
                    })
                  }, 350)
                }}
              >
                {
                  renderOption ? renderOption(item) : item.leftIconName ? (
                    <View style={styles.viewContentRow}>
                      <Icon style={styles.iconLeft} name={item.leftIconName} size={24} color={common().COLOR_ICON_NORMAL} />
                      <Text numberOfLines={1} style={[styles.txtOptions, { ...item.style }]}>
                        {item.title}
                      </Text>
                    </View>
                  ) : (
                      <Text numberOfLines={1} style={[styles.txtOptions, item.style]}>
                        {item.title}
                      </Text>
                    )
                }

                {
                  renderCheckSelect && (checkSelect === idx) && (
                    <Icon style={styles.iconCheck} name='done' size={20} color={colorCheckSelect} />
                  )
                }
              </TouchableOpacity>
              {
                options.length > 1 && (
                  <View style={styles.viewSeparator} />
                )
              }
            </View>
          ))
        }
      </View>
    )

    const renderBottomTitle = (
      <TouchableOpacity
        style={styles.btnBottom}
        activeOpacity={0.8}
        onPress={this.handlePressBottomButton.bind(this)}
      >
        <BlurView
          style={styles.absolute}
          viewRef={this.state.viewRef}
          blurType="light"
          blurAmount={30}
          blurRadius={6}
        />
        <Text numberOfLines={1} style={[styles.txtBottom, bottomStyle ? bottomStyle : undefined]}>
          {bottomTitle}
        </Text>
      </TouchableOpacity>
    )

    return (
      <Modal
        style={[styles.modal, { height: this.heightModal }]}
        isVisible={this.state.showModal}
        hideModalContentWhileAnimating
        useNativeDriver
        onBackdropPress={this.hide}
        animationOutTiming={200}
        {...otherProps}
      >
        <View
          onLayout={(evt) => {
            this.heightModal = evt.nativeEvent.layout.height
          }}
        >
          <View style={styles.viewTop}>
            <BlurView
              style={styles.absolute}
              viewRef={this.state.viewRef}
              blurType="light"
              blurAmount={30}
              blurRadius={6}
            />
            {
              renderTitle ? renderTitle : title ? renderTitleTop : null
            }

            {
              options ? renderMiddle : null
            }
          </View>
          {
            renderBottomButton ? renderBottomButton : bottomTitle ? renderBottomTitle : null
          }
        </View>
      </Modal>
    )
  }
}

ActionSheet.defaultProps = {
  options: [
    { title: 'title1' },
    { title: 'title2' },
    { title: 'title3' },
  ],
  renderOption: undefined,
  renderCheckSelect: false,
  checkSelect: 0,
  colorCheckSelect: common().ACTION_SHEET_COLOR_CHECK_ICON
}

ActionSheet.propTypes = {
  title: PropTypes.string,
  message: PropTypes.string,
  options: PropTypes.arrayOf(PropTypes.shape({
    title: PropTypes.string,
    onPress: PropTypes.func,
    style: Text.propTypes.style,
  })),
  titleStyle: Text.propTypes.style,
  messageStyle: Text.propTypes.style,
  bottomTitle: PropTypes.string,
  bottomStyle: Text.propTypes.style,
  renderTitle: PropTypes.node,
  renderBottomButton: PropTypes.node,
  renderOption: PropTypes.func,
  isOpen: PropTypes.bool,
  renderCheckSelect: PropTypes.bool,
  checkSelect: PropTypes.number,
  colorCheckSelect: PropTypes.string,
  onHide: PropTypes.func
}

type OptionStatic = Array<{
  title?: string;
  onPress?: () => void;
  style?: StyleProp<TextStyle>;
}>

interface Props {
  title?: string;
  message?: string;
  options?: OptionStatic;
  titleStyle?: StyleProp<TextStyle>;
  messageStyle?: StyleProp<TextStyle>;
  bottomTitle?: string;
  bottomStyle?: StyleProp<TextStyle>;
  renderTitle?: any;
  renderBottomButton?: any;
  renderOption?: () => void;
  isOpen?: boolean;
  renderCheckSelect?: boolean;
  checkSelect?: number;
  colorCheckSelect?: string;
  onHide?: () => void;
}

export default ActionSheet