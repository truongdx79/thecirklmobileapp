/**
* Created by bav on Wed Jul 25 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text, TouchableOpacity, Dimensions, ViewPropTypes } from 'react-native';
import { MKButton } from 'react-native-material-kit';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import { common } from '../../config/common';
import { isTablet } from '../../common/utils';

const SCREEN = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    overflow: 'hidden'
  },
  viewLeft: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 16
  },
  viewRight: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 16
  },
  txtLeftTitle: {
    fontSize: common().FONT_SIZE_TITLE,
  },
  txtSubTitle: {
    fontSize: common().FONT_SIZE_SMALL,
    marginTop: 6
  }
});

class Row extends PureComponent<Props> {
  render() {
    const {
      leftTitle,
      leftIconName,
      rightTitle,
      rightIconName,
      leftTitleStyle,
      leftTitleColor,
      leftIconColor,
      rightTitleStyle,
      rightTitleColor,
      rightIconColor,
      style,
      width,
      height,
      backgroundColor,
      leftRightRatio,
      onPress,
      disable,
      materialButton,
      borderRadius,
      firstBorder,
      lastBorder,
      subTitle,
      subTitleStyle,
      subTitleColor
    } = this.props;
    let Component = onPress ? materialButton ? MKButton : TouchableOpacity : View;
    if (disable) {
      Component = View
    }

    renderLeft = (
      <View style={[styles.viewLeft, leftIconName ? { marginLeft: 14 } : undefined]}>
        {
          leftIconName && <Icon name={leftIconName} size={24} color={leftIconColor} />
        }
        <View style={leftIconName ? { marginLeft: 10 } : undefined}>
          {
            !!leftTitle && <Text style={[styles.txtLeftTitle, leftTitleStyle, { color: leftTitleColor }]}>{leftTitle}</Text>
          }
          {
            !!subTitle && <Text style={[styles.txtSubTitle, subTitleStyle, { color: subTitleColor }]}>{subTitle}</Text>
          }
        </View>
      </View>
    )

    renderRight = (
      <View style={[styles.viewRight, rightIconName ? { marginRight: 10 } : undefined]}>
        {
          !!rightTitle && <Text style={[styles.txtLeftTitle, rightTitleStyle, { color: rightTitleColor }]}>{rightTitle}</Text>
        }
        {
          rightIconName && <Icon name={rightIconName} size={24} color={rightIconColor} />
        }
      </View>
    )

    return (
      <Component
        style={[
          styles.container,
          {
            width,
            height,
            backgroundColor
          },
          borderRadius ? { borderRadius: 10 } : undefined,
          firstBorder ? { borderTopLeftRadius: 10, borderTopRightRadius: 10 } : undefined,
          lastBorder ? { borderBottomLeftRadius: 10, borderBottomRightRadius: 10 } : undefined,
          style,
          disable ? { opacity: 0.4 } : undefined
        ]}
        onPress={onPress}
      >
        {
          (leftTitle || leftIconName) && renderLeft
        }
        {
          (rightTitle || rightIconName) && renderRight
        }
      </Component>
    );
  }
}

Row.defaultProps = {
  leftTitleColor: common().COLOR_TEXT_NORMAL,
  leftIconColor: common().COLOR_TEXT_NORMAL,
  backgroundColor: common().CARD_BACKGROUND_COLOR,
  rightTitleColor: common().COLOR_TEXT_NORMAL,
  rightIconColor: common().COLOR_TEXT_NORMAL,
  subTitleColor: common().COLOR_TEXT_NORMAL,
  height: 55,
  width: isTablet ? (SCREEN.width / 2 - 32) : (SCREEN.width - 32),
  disable: false,
  materialButton: true,
  borderRadius: false,
  firstBorder: false,
  lastBorder: false
}

Row.propTypes = {
  leftTitle: PropTypes.string,
  leftIconName: PropTypes.string,
  rightTitle: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  rightIconName: PropTypes.string,
  leftTitleStyle: Text.propTypes.style,
  leftTitleColor: PropTypes.string,
  leftIconColor: PropTypes.string,
  rightTitleStyle: Text.propTypes.style,
  rightTitleColor: PropTypes.string,
  rightIconColor: PropTypes.string,
  style: ViewPropTypes.style,
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  height: PropTypes.number,
  backgroundColor: PropTypes.string,
  leftRightRatio: PropTypes.number,
  onPress: PropTypes.func,
  disable: PropTypes.bool,
  materialButton: PropTypes.bool,
  borderRadius: PropTypes.bool,
  firstBorder: PropTypes.bool,
  lastBorder: PropTypes.bool,
  subTitle: PropTypes.string,
  subTitleStyle: Text.propTypes.style,
  subTitleColor: PropTypes.string
}

interface Props {
  leftTitle?: string;
  leftIconName?: string;
  rightTitle?: string | number;
  rightIconName?: string;
  leftTitleStyle?: StyleProp<TextStyle>;
  leftTitleColor?: string;
  leftIconColor?: string;
  rightTitleStyle?: StyleProp<TextStyle>;
  rightTitleColor?: string;
  rightIconColor?: string;
  style?: StyleProp<ViewStyle>;
  /**
   * default value SCREEN.width - 32
   */
  width?: number | string;
  /**
   * default value 55
   */
  height?: number;
  backgroundColor?: string;
  /**
   * call when press row
   */
  onPress?: () => void;
  disable?: boolean;
  /**
   * use material button when true and TouchableOpacity when false
   */
  materialButton?: boolean;
  /**
   * default false. If true set borderRadius = 10
   */
  borderRadius?: boolean;
  /**
   * default false. If true set borderRadiusTopLeft and borderRadiusTopRight = 10
   */
  firstBorder?: boolean;
  /**
   * default false. If true set borderRadiusBottomLeft and borderRadiusBottomRight = 10
   */
  lastBorder?: boolean;
  /**
   * subtitle underline of title
   */
  subTitle?: string;
  subTitleStyle?: StyleProp<TextStyle>;
  subTitleColor?: string;
}

export default Row;
