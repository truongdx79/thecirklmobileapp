/**
* Created by bav on Wed Jul 25 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text, TouchableOpacity, Dimensions, ViewPropTypes } from 'react-native';
import { MKButton, MKSwitch } from 'react-native-material-kit';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import { common } from '../../config/common';
import { isTablet } from '../../common/utils';

const SCREEN = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    overflow: 'hidden'
  },
  viewLeft: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 16
  },
  viewRight: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 10
  },
  txtLeftTitle: {
    fontSize: common().FONT_SIZE_TITLE,
  },
  txtSubTitle: {
    fontSize: common().FONT_SIZE_SMALL,
    marginTop: 6
  }
});

class RowSwitch extends PureComponent<Props> {
  render() {
    const {
      leftTitle,
      leftIconName,
      rightTitle,
      leftTitleStyle,
      leftTitleColor,
      leftIconColor,
      rightTitleStyle,
      rightTitleColor,
      style,
      width,
      height,
      backgroundColor,
      leftRightRatio,
      onPress,
      disable,
      materialButton,
      borderRadius,
      firstBorder,
      lastBorder,
      isCheck,
      onChange,
      subTitle,
      subTitleStyle,
      subTitleColor,
      ...switchProp
    } = this.props;
    let Component = onPress ? materialButton ? MKButton : TouchableOpacity : View;
    if (disable) {
      Component = View
    }

    renderLeft = (
      <View style={[styles.viewLeft, leftIconName ? { marginLeft: 10 } : undefined]}>
        {
          leftIconName && <Icon name={leftIconName} size={24} color={leftIconColor} />
        }
        <View style={leftIconName ? { marginLeft: 10 } : undefined}>
          {
            leftTitle && <Text style={[styles.txtLeftTitle, leftTitleStyle, { color: leftTitleColor }]}>{leftTitle}</Text>
          }
          {
            subTitle && <Text style={[styles.txtSubTitle, subTitleStyle, { color: subTitleColor }]}>{subTitle}</Text>
          }
        </View>
      </View>
    )

    renderRight = (
      <View style={[styles.viewRight]}>
        {
          rightTitle && <Text style={[styles.txtLeftTitle, rightTitleStyle, { color: rightTitleColor }]}>{rightTitle}</Text>
        }
        <MKSwitch
          onColor={common().SWITCH_ACTIVE_COLOR}
          thumbOnColor={common().SWITCH_THUMB_ON_COLOR}
          checked={isCheck}
          onCheckedChange={onChange}
          trackSize={25}
          trackLength={43}
          thumbRadius={23/2}
          {...switchProp}
        />
      </View>
    )

    return (
      <Component
        style={[
          styles.container,
          {
            width,
            height,
            backgroundColor
          },
          borderRadius ? { borderRadius: 10 } : undefined,
          firstBorder ? { borderTopLeftRadius: 10, borderTopRightRadius: 10 } : undefined,
          lastBorder ? { borderBottomLeftRadius: 10, borderBottomRightRadius: 10 } : undefined,
          style,
          disable ? { opacity: 0.4 } : undefined
        ]}
        onPress={onPress}
      >
        {
          (leftTitle || leftIconName) && renderLeft
        }
        {
          renderRight
        }
      </Component>
    );
  }
}

RowSwitch.defaultProps = {
  leftTitleColor: common().COLOR_TEXT_NORMAL,
  leftIconColor: common().COLOR_TEXT_NORMAL,
  backgroundColor: common().CARD_BACKGROUND_COLOR,
  rightTitleColor: common().COLOR_TEXT_NORMAL,
  subTitleColor: common().COLOR_TEXT_NORMAL,
  height: 55,
  width: isTablet ? (SCREEN.width/2 - 32) : (SCREEN.width - 32),
  disable: false,
  materialButton: true,
  borderRadius: false,
  firstBorder: false,
  lastBorder: false,
  isCheck: false
}

RowSwitch.propTypes = {
  leftTitle: PropTypes.string,
  leftIconName: PropTypes.string,
  rightTitle: PropTypes.string,
  leftTitleStyle: Text.propTypes.style,
  leftTitleColor: PropTypes.string,
  leftIconColor: PropTypes.string,
  rightTitleStyle: Text.propTypes.style,
  rightTitleColor: PropTypes.string,
  style: ViewPropTypes.style,
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  height: PropTypes.number,
  backgroundColor: PropTypes.string,
  leftRightRatio: PropTypes.number,
  onPress: PropTypes.func,
  disable: PropTypes.bool,
  materialButton: PropTypes.bool,
  borderRadius: PropTypes.bool,
  firstBorder: PropTypes.bool,
  lastBorder: PropTypes.bool,
  isCheck: PropTypes.bool,
  onChange: PropTypes.func,
  subTitle: PropTypes.string,
  subTitleStyle: Text.propTypes.style,
  subTitleColor: PropTypes.string
}

interface Props {
  leftTitle?: string;
  leftIconName?: string;
  rightTitle?: string;
  leftTitleStyle?: StyleProp<TextStyle>;
  leftTitleColor?: string;
  leftIconColor?: string;
  rightTitleStyle?: StyleProp<TextStyle>;
  rightTitleColor?: string;
  style?: StyleProp<ViewStyle>;
  /**
   * default value SCREEN.width - 32
   */
  width?: number | string;
  /**
   * default value 55
   */
  height?: number;
  backgroundColor?: string;
  /**
   * call when press row
   */
  onPress?: () => void;
  disable?: boolean;
  /**
   * use material button when true and TouchableOpacity when false
   */
  materialButton?: boolean;
  /**
   * default false. If true set borderRadius = 10
   */
  borderRadius?: boolean;
  /**
   * default false. If true set borderRadiusTopLeft and borderRadiusTopRight = 10
   */
  firstBorder?: boolean;
  /**
   * default false. If true set borderRadiusBottomLeft and borderRadiusBottomRight = 10
   */
  lastBorder?: boolean;
  /**
   * subtitle underline of title
   */
  subTitle?: string;
  subTitleStyle?: StyleProp<TextStyle>;
  subTitleColor?: string;
  isCheck?: boolean;
  /**
   * call when change check
   */
  onChange?: (check: boolean) => void;
}

export default RowSwitch;
