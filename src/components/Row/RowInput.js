/**
* Created by bav on Wed Jul 25 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text, ViewPropTypes, Dimensions, TextInput, TouchableOpacity } from 'react-native';
import { MKButton } from 'react-native-material-kit';
import { ButtonIcon } from '../Button';
import { common } from '../../config/common';
import { isTablet } from '../../common/utils';

const SCREEN = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 16
  },
  titleLeft: {
    fontSize: common().FONT_SIZE_TITLE,
    fontWeight: common().FONT_WEIGHT_TITLE
  },
  input: {
    flex: 1,
    fontSize: common().FONT_SIZE_TITLE,
    fontWeight: common().FONT_WEIGHT_TITLE,
    textAlign: 'right',
    color: common().COLOR_TEXT_NORMAL,
    paddingLeft: 16
  },
  viewLeft: {

  },
  viewRight: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  txtLeftTitle: {
    fontSize: common().FONT_SIZE_TITLE,
  },
  txtSubTitle: {
    fontSize: common().FONT_SIZE_SMALL,
    marginTop: 6
  }
});

class RowInput extends PureComponent<Props> {
  constructor(props) {
    super(props);
    this.state = {
      text: this.props.value || '',
      showClearIcon: this.props.value !== '' ? true : false,
      hidePassword: props.typeInput === 'password' ? true : false
    }

    this.rowInput = React.createRef()
  }


  componentWillReceiveProps(nextProps) {
    if (this.props.value != nextProps.value) {
      this.setState({
        text: nextProps.value
      })
    }
  }

  onChangeText = (text) => {
    if (text.length > 0) {
      this.setState({
        text,
        showClearIcon: true
      })
    } else {
      this.setState({
        text,
        showClearIcon: false
      })
    }

    this.props.onChangeText && this.props.onChangeText(text);
  }

  onClearText = () => {
    this.onChangeText('')
  }

  showHidePassword = () => {
    this.setState({
      hidePassword: !this.state.hidePassword
    })
  }

  focus = () => {
    this.rowInput.current.focus()
  }

  blur = () => {
    this.rowInput.current.blur()
  }

  _replaceSpace(str) {
    return str.replace(/\u0020/, '\u00a0')
  }

  render() {
    let {
      style,
      leftTitle,
      leftTitleStyle,
      height,
      width,
      borderRadius,
      firstBorder,
      lastBorder,
      titleLeftColor,
      leftRightRatio,
      backgroundColor,
      onPress,
      materialButton,
      placeholder,
      returnKeyType,
      leftTitleComponent,
      typeInput,
      ...otherProps
    } = this.props;
    const { showClearIcon, text, hidePassword } = this.state;
    const Component = onPress ? materialButton ? MKButton : TouchableOpacity : View
    return (
      <Component
        style={[
          styles.container,
          {
            backgroundColor,
            height,
            width
          },
          borderRadius ? { borderRadius: 10 } : undefined,
          firstBorder ? { borderTopLeftRadius: 10, borderTopRightRadius: 10 } : undefined,
          lastBorder ? { borderBottomLeftRadius: 10, borderBottomRightRadius: 10 } : undefined,
          style
        ]}
      >
        {
          leftTitleComponent ? leftTitleComponent() :
            <View style={[styles.viewLeft, { flex: leftRightRatio }]}>
              {
                !!leftTitle && (
                  <Text numberOfLines={1} style={[styles.titleLeft, { color: titleLeftColor }, leftTitleStyle]}>
                    {leftTitle}
                  </Text>
                )
              }
            </View>
        }
        <View style={[styles.viewRight, { flex: 1 / leftRightRatio }]}>
          <TextInput
            {...otherProps}
            underlineColorAndroid='transparent'
            style={[styles.input]}
            placeholder={placeholder}
            placeholderTextColor={common().INPUT_COLOR_TEXT_PLACEHOLDER}
            selectionColor={common().INPUT_SELECTION_COLOR}
            autoCorrect={false}
            numberOfLines={1}
            multiline={false}
            secureTextEntry={hidePassword}
            returnKeyType={returnKeyType}
            ref={this.rowInput}
            value={this._replaceSpace(text)}
            onChangeText={this.onChangeText}
          />
          {
            (showClearIcon || typeInput === 'password') && (
              <ButtonIcon
                iconName={typeInput === 'normal' ? 'clear' : 'remove-red-eye'}
                size={18}
                onPress={typeInput === 'normal' ? this.onClearText : this.showHidePassword}
                style={{ marginRight: -8 }}
              />
            )
          }
        </View>
      </Component >
    );
  }
}

RowInput.defaultProps = {
  height: 55,
  width: isTablet ? (SCREEN.width / 2 - 32) : (SCREEN.width - 32),
  backgroundColor: common().CARD_BACKGROUND_COLOR,
  borderRadius: false,
  firstBorder: false,
  lastBorder: false,
  titleLeftColor: common().COLOR_TEXT_NORMAL,
  materialButton: true,
  leftRightRatio: 2 / 3,
  returnKeyType: 'next',
  typeInput: 'normal'
}

RowInput.propTypes = {
  style: ViewPropTypes.style,
  leftTitle: PropTypes.string,
  leftTitleStyle: Text.propTypes.style,
  height: PropTypes.number,
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  borderRadius: PropTypes.bool,
  firstBorder: PropTypes.bool,
  lastBorder: PropTypes.bool,
  titleLeftColor: PropTypes.string,
  backgroundColor: PropTypes.string,
  onPress: PropTypes.func,
  materialButton: PropTypes.bool,
  leftRightRatio: PropTypes.number,
  //TextInput
  placeholder: PropTypes.string,
  returnKeyType: PropTypes.string,
  inputStyle: ViewPropTypes.style,
  typeInput: PropTypes.oneOf(['normal', 'password'])
}

interface Props {
  leftTitle?: string;
  leftTitleStyle?: StyleProp<TextStyle>;
  leftTitleColor?: string;
  style?: StyleProp<ViewStyle>;
  /**
   * default value SCREEN.width - 32
   */
  width?: number | string;
  /**
   * default value 55
   */
  height?: number;
  backgroundColor?: string;
  /**
   * call when press row
   */
  onPress?: () => void;
  /**
   * use material button when true and TouchableOpacity when false
   */
  materialButton?: boolean;
  /**
   * default false. If true set borderRadius = 10
   */
  borderRadius?: boolean;
  /**
   * default false. If true set borderRadiusTopLeft and borderRadiusTopRight = 10
   */
  firstBorder?: boolean;
  /**
   * default false. If true set borderRadiusBottomLeft and borderRadiusBottomRight = 10
   */
  lastBorder?: boolean;
  /**
   * ratio left/right. default 2/3
   */
  leftRightRatio?: number;
  placeholder?: string;
  returnKeyType?: string;
  inputStyle?: StyleProp<ViewStyle>;
  typeInput?: 'normal' | 'password';
  value?: string;
  onChangeText?: () => void;
}

export default RowInput;
