/**
* Created by bav on Wed Jul 25 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text, ViewPropTypes, Dimensions, TextInput, TouchableOpacity } from 'react-native';
import { ButtonIcon } from '../Button';
import { common } from '../../config/common';
import { isTablet } from '../../common/utils';

const SCREEN = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 16,
    paddingRight: 4
  },
  input: {
    flex: 1,
    fontSize: common().FONT_SIZE_TITLE,
    fontWeight: common().FONT_WEIGHT_TITLE,
    color: common().COLOR_TEXT_NORMAL
  }
});

class RowInput extends PureComponent<Props> {
  constructor(props) {
    super(props);
    this.state = {
      text: this.props.value || '',
      showClearIcon: this.props.value !== '' ? true : false,
      hidePassword: props.typeInput === 'password' ? true : false
    }

    this.rowInput = React.createRef()
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.props.value) {
      this.setState({
        text: nextProps.value
      })
    }
  }

  onChangeText = (text) => {
    if (text.length > 0) {
      this.setState({
        text,
        showClearIcon: true
      })
    } else {
      this.setState({
        text,
        showClearIcon: false
      })
    }

    this.props.onChangeText && this.props.onChangeText(text);
  }

  onClearText = () => {
    this.onChangeText('')
  }

  focus = () => {
    this.rowInput.current.focus()
  }

  blur = () => {
    this.rowInput.current.blur()
  }

  showHidePassword = () => {
    this.setState({
      hidePassword: !this.state.hidePassword
    })
  }

  render() {
    let {
      style,
      height,
      width,
      borderRadius,
      firstBorder,
      lastBorder,
      backgroundColor,
      placeholder,
      inputStyle,
      returnKeyType,
      rightTitleComponent,
      typeInput,
      ...otherProps
    } = this.props;
    const { text, showClearIcon, hidePassword } = this.state;
    return (
      <View
        style={[
          styles.container,
          {
            backgroundColor,
            height,
            width
          },
          borderRadius ? { borderRadius: 10 } : undefined,
          firstBorder ? { borderTopLeftRadius: 10, borderTopRightRadius: 10 } : undefined,
          lastBorder ? { borderBottomLeftRadius: 10, borderBottomRightRadius: 10 } : undefined,
          style
        ]}
        pointerEvents='box-none'
      >
        <TextInput
          {...otherProps}
          underlineColorAndroid='transparent'
          style={[styles.input, inputStyle]}
          placeholder={placeholder}
          placeholderTextColor={common().INPUT_COLOR_TEXT_PLACEHOLDER}
          selectionColor={common().INPUT_SELECTION_COLOR}
          autoCorrect={false}
          numberOfLines={1}
          multiline={false}
          returnKeyType={returnKeyType}
          secureTextEntry={hidePassword}
          ref={this.rowInput}
          value={text}
          onChangeText={this.onChangeText}
        />
        {
          (showClearIcon || typeInput === 'password') && (
            <ButtonIcon
              iconName={typeInput === 'normal' ? 'clear' : 'remove-red-eye'}
              size={18}
              onPress={typeInput === 'normal' ? this.onClearText : this.showHidePassword}
              style={typeInput === 'password' ? { marginRight: -8 } : undefined}
            />
          )
        }
        {rightTitleComponent && rightTitleComponent()}
      </View>
    );
  }
}

RowInput.defaultProps = {
  height: 55,
  width: isTablet ? (SCREEN.width / 2 - 32) : (SCREEN.width - 32),
  backgroundColor: common().CARD_BACKGROUND_COLOR,
  borderRadius: false,
  firstBorder: false,
  lastBorder: false,
  returnKeyType: 'next',
  typeInput: 'normal'
}

RowInput.propTypes = {
  style: ViewPropTypes.style,
  height: PropTypes.number,
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  borderRadius: PropTypes.bool,
  firstBorder: PropTypes.bool,
  lastBorder: PropTypes.bool,
  backgroundColor: PropTypes.string,
  //TextInput
  placeholder: PropTypes.string,
  returnKeyType: PropTypes.string,
  inputStyle: ViewPropTypes.style,
  value: PropTypes.string,
  type: PropTypes.string,
  onChangeText: PropTypes.func,
  typeInput: PropTypes.oneOf(['normal', 'password'])
}

interface Props {
  style?: StyleProp<ViewStyle>;
  /**
   * default value SCREEN.width - 32
   */
  width?: number | string;
  /**
   * default value 55
   */
  height?: number;
  /**
   * default false. If true set borderRadius = 10
   */
  borderRadius?: boolean;
  /**
   * default false. If true set borderRadiusTopLeft and borderRadiusTopRight = 10
   */
  firstBorder?: boolean;
  /**
   * default false. If true set borderRadiusBottomLeft and borderRadiusBottomRight = 10
   */
  lastBorder?: boolean;
  backgroundColor?: string;
  type?: string;
  placeholder?: string;
  returnKeyType?: string;
  inputStyle?: StyleProp<ViewStyle>;
  value?: string;
  onChangeText?: (text: string) => void;
  typeInput?: 'normal' | 'password';
}

export default RowInput;
