/**
* Created by bav on Tue Jul 31 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  View,
  Text,
  Animated,
  Dimensions,
  SafeAreaView,
  ViewPropTypes,
  TouchableWithoutFeedback,
  LayoutAnimation,
  Platform
} from 'react-native';
import Interactable from 'react-native-interactable';
import { CustomLayoutSpring } from '../../common/animation';

const isIOS = Platform.OS === 'ios';

const styles = StyleSheet.create({
  panel: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    overflow: 'hidden'
  }
});

const SCREEN = Dimensions.get('window');

class SmartPanel extends PureComponent<Props> {
  constructor(props) {
    super(props);
    this.state = {
      tabbarSetTop: true
    }

    this.refPannel = React.createRef()
  }

  _onSnap = ({ nativeEvent }) => {
    if (nativeEvent.index === 0) {
      this.props.onFullHeight && this.props.onFullHeight()
    } else {
      LayoutAnimation.configureNext(CustomLayoutSpring)
      this.setState({
        tabbarSetTop: true
      })

      this.props.onMinHeight && this.props.onMinHeight()
    }
  }

  onPressMinHeight = () => {
    LayoutAnimation.configureNext(CustomLayoutSpring)
    this.setState({
      tabbarSetTop: false
    }, () => {
      this.props.touchToFullHeight && this.refPannel.current.snapTo({ index: 0 })
    })
  }

  renderTabbarOverlay = () => {
    return (
      <View style={{ height: this.props.tabbarHeight }} />
    )
  }

  _onDrag = () => {
    LayoutAnimation.configureNext(CustomLayoutSpring)
    if (this.state.tabbarSetTop) {
      this.setState({
        tabbarSetTop: false
      })
    }

  }

  showMinHeight = () => {
    this.refPannel.current.snapTo({ index: 1 })
  }

  showFullHeight = () => {
    this.refPannel.current.snapTo({ index: 0 })
  }

  render() {
    const {
      minHeight,
      width,
      height,
      renderMinHeight,
      renderFullHeight,
      backgroundMinHeight,
      backgroundFullHeight,
      paddingTabbar,
      tabbarHeight,
      interactableProp
    } = this.props;
    const { tabbarSetTop } = this.state;
    
    return (
      <Interactable.View
        {...interactableProp}
        ref={this.refPannel}
        style={styles.panel}
        verticalOnly
        animatedNativeDriver
        snapPoints={[
          { y: -minHeight - (isIOS ? 0 : (paddingTabbar ? 0 : 0)) },
          { y: height - minHeight - (paddingTabbar ? tabbarHeight : 0) }
        ]}
        dragWithSpring={{ tension: 2000, damping: 0.5 }}
        boundaries={{ top: -minHeight - (isIOS ? 0 : (paddingTabbar ? 0 : 0)), bottom: height - minHeight, bounce: 0.1, haptics: true }}
        initialPosition={{ y: height - minHeight - (paddingTabbar ? tabbarHeight : 0) }}
        onSnap={this._onSnap}
        onDrag={this._onDrag}
      >
        <Animated.View
          style={[
            {
              height: height + minHeight + (paddingTabbar ? tabbarHeight : 0),
              width: width
            }
          ]}
        >
          <TouchableWithoutFeedback
            onPress={this.onPressMinHeight}
          >
            <View
              style={[
                {
                  height: minHeight,
                  backgroundColor: backgroundMinHeight
                }
              ]}
            >
              {
                renderMinHeight && renderMinHeight()
              }
            </View>
          </TouchableWithoutFeedback>

          {
            paddingTabbar && tabbarSetTop && this.renderTabbarOverlay()
          }

          <View
            style={[
              {
                height: paddingTabbar ? height - (isIOS ? tabbarHeight : tabbarHeight) : height,
                backgroundColor: backgroundFullHeight
              }
            ]}
          >
            {
              renderFullHeight && renderFullHeight()
            }
          </View>

          {
            paddingTabbar && !tabbarSetTop && this.renderTabbarOverlay()
          }
        </Animated.View>
      </Interactable.View>
    );
  }
}

SmartPanel.defaultProps = {
  minHeight: 55,
  width: SCREEN.width,
  height: SCREEN.height,
  backgroundMinHeight: 'lightgreen',
  backgroundFullHeight: 'steelblue',
  touchToFullHeight: true,
  paddingTabbar: true,
  tabbarHeight: 100
}

SmartPanel.propTypes = {
  minHeight: PropTypes.number,
  width: PropTypes.number,
  height: PropTypes.number,
  backgroundMinHeight: PropTypes.string,
  backgroundFullHeight: PropTypes.string,
  renderMinHeight: PropTypes.func,
  renderFullHeight: PropTypes.func,
  touchToFullHeight: PropTypes.bool,
  onMinHeight: PropTypes.func,
  onFullHeight: PropTypes.func,
  paddingTabbar: PropTypes.bool,
  tabbarHeight: PropTypes.number
}

interface Props {
  /**
   * Default value 55
   */
  minHeight?: number;
  /**
   * Default value Dimensions.get('window').width
   */
  width?: number;
  /**
   * Default value Dimensions.get('window').height
   */
  height?: number;
  /**
   * Default value lightgreen
   */
  backgroundMinHeight?: string;
  /**
   * Default value steelblue
   */
  backgroundFullHeight?: string;
  renderMinHeight?: () => void;
  renderFullHeight?: () => void;
  /**
   * Call when min height
   */
  onMinHeight?: () => void;
  /**
   * Call when full height
   */
  onFullHeight?: () => void;
  /**
   * When true, set childrent margin bottom tabbar height
   */
  paddingTabbar?: boolean;
  /**
   * default value 100. Use when set paddingTabbar = true
   */
  tabbarHeight?: number
}

export default SmartPanel;
