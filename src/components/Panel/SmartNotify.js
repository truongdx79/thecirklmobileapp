/**
* Created by bav on Thu Aug 02 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, ViewPropTypes, Text } from 'react-native';
import Interactable from 'react-native-interactable';
import { BlurView } from 'react-native-blur';
import { common } from '../../config/common';
import { safeAreaInsetX, isX } from '../../common/utils';

const styles = StyleSheet.create({
  panel: {
    ...StyleSheet.absoluteFillObject
  },
  absolute: {
    ...StyleSheet.absoluteFillObject
  },
  viewTitle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: isX ? safeAreaInsetX.top : 0
  },
  title: {
    fontSize: 20,
    color: 'white'
  }
});

class SmartNotify extends PureComponent<Props> {
  constructor(props) {
    super(props);
    this.state = {
      viewRef: null,
      data: null,
      queue: [],
    }
    this.currentIndex = 0
    this.refPannel = React.createRef()
  }

  componentWillUnmount() {
    if (this.timeoutDisappear) {
      clearTimeout(this.timeoutDisappear)
    }
  }

  show = (data) => {
    this.setState({ data })
    this.refPannel.current.snapTo({ index: 1 })
    this.currentIndex = 1

    if (data && !data.autoHide) {
      return
    }

    if (this.timeoutDisappear) {
      clearTimeout(this.timeoutDisappear)
    }
    this.timeoutDisappear = setTimeout(() => {
      this.hide()
    }, data && data.timeDismiss ? data.timeDismiss : 2000)
  }


  pushNotify(data) {
    const { queue } = this.state
    if (queue.length == 0 && this.currentIndex == 0) {
      this.show(data)
      return
    }

    this.setState({
      queue: queue.concat([data])
    })
  }

  hide = () => {
    this.refPannel.current.snapTo({ index: 0 })
  }

  _onSnap = ({ nativeEvent }) => {
    if (nativeEvent.index === 0) {
      if (this.timeoutDisappear) {
        clearTimeout(this.timeoutDisappear)
      }
      
      this.currentIndex = 0
      this.props.onHide && this.props.onHide()
      const { queue } = this.state
      if (queue.length != 0) {
        let data = queue[0]
        queue.splice(0, 1)
        this.setState({
          queue: [...queue]
        })
        this.show(data)
      }
    } else {
      this.props.onShow && this.props.onShow()
    }
  }

  render() {
    const {
      height,
      backgroundColor,
      interactableProp,
      blurOverlay
    } = this.props;
    const { data } = this.state;

    return (
      <Interactable.View
        {...interactableProp}
        ref={this.refPannel}
        style={[
          styles.panel,
          {
            backgroundColor,
            height
          }
        ]}
        verticalOnly
        animatedNativeDriver
        snapPoints={[
          { y: -height },
          { y: 0 }
        ]}
        dragWithSpring={{ tension: 2000, damping: 0.5 }}
        boundaries={{ top: -height, bottom: 0, bounce: 0.1, haptics: true }}
        initialPosition={{ y: -height }}
        onSnap={this._onSnap}
        onDrag={this._onDrag}
      >
        {
          blurOverlay && (
            <BlurView
              style={styles.absolute}
              viewRef={this.state.viewRef}
              blurType="light"
              blurAmount={11}
              blurRadius={3}
            />
          )
        }
        <View style={[styles.absolute, { backgroundColor: 'rgba(0, 0, 0, 0.1)' }]} />
        {
          data && data.renderItem ? data.renderItem() : data && data.title && (
            <View
              style={[
                styles.viewTitle,
                data.style,
                data.backgroundColor ? { backgroundColor: data.backgroundColor } : undefined,
              ]}
            >
              <Text
                style={[
                  styles.title,
                  data.titleStyle,
                  data.titleColor ? { color: data.titleColor } : undefined
                ]}
              >
                {data.title}
              </Text>
            </View>
          )
        }
        {

        }
      </Interactable.View>
    );
  }
}

SmartNotify.defaultProps = {
  height: 44 + (isX ? safeAreaInsetX.top : 0),
  backgroundColor: 'transparent',
  blurOverlay: true
}

SmartNotify.propTypes = {
  height: PropTypes.number,
  backgroundColor: PropTypes.string,
  style: ViewPropTypes.style,
  blurOverlay: PropTypes.bool,
  onShow: PropTypes.func,
  onHide: PropTypes.func
}

interface Props {
  /**
   * Defaut height = 44
   */
  height?: number;
  backgroundColor?: string;
  style?: StyleProp<ViewStyle>;
  /**
   * Enable blur background mode. Default true
   */
  blurOverlay?: boolean;
  /**
   * Call when show notify
   */
  onShow?: () => void;
  /**
   * Call when hide notify
   */
  onHide?: () => void;
  autoHide?: boolean;
}

export default SmartNotify;
