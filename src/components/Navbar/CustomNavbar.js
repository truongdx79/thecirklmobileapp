import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Image, Platform, StyleSheet, Text, TouchableOpacity, View, ViewPropTypes, StatusBar, Dimensions, ImageBackground } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import { common } from '../../config/common';
import { imgs } from '../../config/imgs';

var DeviceInfo = require('react-native-device-info');

export const heightNavbar = Platform.OS === 'ios' ? DeviceInfo.getModel() === 'iPhone X' ? 100 : 80 : 60
export const paddingTop = Platform.OS === 'ios' ? DeviceInfo.getModel() === 'iPhone X' ? 40 : 20 : 0

const styles = StyleSheet.create({
  root: {
    height: heightNavbar,
  },
  container: {
    height: heightNavbar,
    flexDirection: 'row',
    paddingTop: paddingTop,
    justifyContent: 'center',
    // alignItems: 'center',
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: 'white',
    // backgroundColor: common().COLOR_BACKGROUND_NABAR
  },
  navBarItem: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    alignSelf: 'center',
    top: paddingTop,
    height: heightNavbar - paddingTop,
    maxWidth: 180,
  },
  titleStyle: {
    color: common().COLOR_TEXT_NAVBAR,
    fontSize: common().FONT_SIZE_HEADER,
    fontWeight: common().FONT_WEIGHT_HEADER,
    alignSelf: 'center'
  },
  headerStyle: {
    color: common().COLOR_TEXT_NAVBAR,
    fontSize: common().FONT_SIZE_TITLE,
    fontWeight: common().FONT_WEIGHT_TITLE
  },
  leftIcon: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    position: 'absolute',
    alignSelf: 'center',
    top: paddingTop,
    left: 0,
    height: heightNavbar - paddingTop
  },
  rightIcon: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    position: 'absolute',
    alignSelf: 'center',
    top: paddingTop,
    right: 0,
    height: heightNavbar - paddingTop
  },
  icon: {
    width: 22,
    height: 22
  },
  leftView: {
    width: 38,
    height: 20,
  },
  rightView: {
    width: 38,
    height: 20,
  }
})

class CustomNavbar extends Component {
  handleLeft() {
    if (this.props.onLeft) {
      this.props.onLeft()
    }
  }

  handleRight() {
    if (this.props.onRight) {
      this.props.onRight()
    }
  }

  render() {
    let {
      style,
      gradient,
      rightIcon,
      rightImage,
      leftIcon,
      leftImage,
      title,
      titleStyle,
      leftStyle,
      rightStyle,
      leftTitle,
      rightTitle,
      middleComponent,
      noBackground,
      back,
      onTitle,
      leftIconName,
      rightIconName
    } = this.props

    const renderMiddle = (
      <View style={styles.navBarItem}>
        {
          onTitle ? (
            <TouchableOpacity onPress={onTitle}>
              <Text numberOfLines={1} style={[styles.titleStyle, titleStyle]}>
                {
                  (typeof title === "function") ? title = title() : ` ${title} `
                }
              </Text>
            </TouchableOpacity>
          ) : (
              <Text numberOfLines={1} style={[styles.titleStyle, titleStyle]}>
                {
                  (typeof title === "function") ? title = title() : ` ${title} `
                }
              </Text>
            )
        }
      </View>
    )

    const renderLeftIcon = (
      <TouchableOpacity
        onPress={back ? Actions.pop : this.handleLeft.bind(this)}
        style={[styles.leftIcon, leftImage ? {} : { paddingLeft: 0 }]}>
        {
          leftImage ? <Image
            style={[styles.icon, leftStyle]}
            resizeMode="contain"
            source={leftImage}
          /> : leftIconName ? <Icon style={{ paddingLeft: 10 }} name={leftIconName} size={26} color="#fff" /> : <Icon style={{ paddingLeft: 0 }} name={'chevron-left'} size={36} color="#fff" />
        }
      </TouchableOpacity>
    )

    const renderLeftTitle = (
      <TouchableOpacity
        onPress={back ? Actions.pop : this.handleLeft.bind(this)}
        style={styles.leftIcon}>
        <Text numberOfLines={1} style={[styles.headerStyle, leftStyle]}>
          {leftTitle}
        </Text>
      </TouchableOpacity>
    )

    const renderRightIcon = (
      <TouchableOpacity
        onPress={this.handleRight.bind(this)}
        style={[styles.rightIcon, rightImage ? {} : { paddingRight: 2 }]}>
        {
          rightImage ? <Image
            style={[styles.icon, rightStyle]}
            resizeMode="contain"
            source={rightImage}
          /> : rightIconName ? <Icon style={{ paddingRight: 10 }} name={rightIconName} size={26} color="#fff" /> : <Icon name={'menu'} size={36} color="#fff" />
        }
      </TouchableOpacity>
    )

    const renderRightTitle = (
      <TouchableOpacity
        onPress={this.handleRight.bind(this)}
        style={styles.rightIcon}>
        <Text numberOfLines={1} style={[styles.headerStyle, rightStyle]}>
          {rightTitle}
        </Text>
      </TouchableOpacity>
    )

    const ComponentNav = noBackground ? View : ImageBackground;

    return (
      <ComponentNav
        source={imgs().bgProfile}
        resizeMode='stretch'
        style={styles.root}
      >
        <View style={[styles.container, style]}>
          {
            leftIcon ? renderLeftIcon : leftTitle ? renderLeftTitle : <View style={[styles.leftView]} />
          }
          {
            title ? renderMiddle : middleComponent ? middleComponent : <View />
          }
          {
            rightIcon ? renderRightIcon : rightTitle ? renderRightTitle : <View style={[styles.rightView]} />
          }
        </View>
      </ComponentNav>
    )
  }
}

CustomNavbar.defaultProps = {
  gradient: true
}

CustomNavbar.propTypes = {
  style: ViewPropTypes.style,
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
  titleStyle: ViewPropTypes.style,
  leftIcon: PropTypes.bool,
  leftImage: Image.propTypes.source,
  leftTitle: PropTypes.string,
  leftStyle: ViewPropTypes.style,
  rightIcon: PropTypes.bool,
  rightImage: Image.propTypes.source,
  rightTitle: PropTypes.string,
  rightStyle: ViewPropTypes.style,
  onLeft: PropTypes.func,
  onRight: PropTypes.func,
  onTitle: PropTypes.func,
  middleComponent: PropTypes.node,
  back: PropTypes.bool,
  noBackground: PropTypes.bool,
  // name icon vector icon
  leftIconName: PropTypes.string,
  rightIconName: PropTypes.string,
  gradient: PropTypes.bool
}

export default CustomNavbar;
