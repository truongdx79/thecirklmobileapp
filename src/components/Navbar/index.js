/**
* Created by bav on Wed Jul 25 2018
* Copyright (c) 2018 bav
*/

import CustomNavbar from './CustomNavbar';
import CustomNavbar2 from './CustomNavbar2';

export {
  CustomNavbar,
  CustomNavbar2,
}
