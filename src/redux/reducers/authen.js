/**
* Created by bav on Thu May 31 2018
* Copyright (c) 2018 bav
*/

import { fromJS } from "immutable";
import * as types from '../types';

const initialState = {
  authenticated: false,
  isLogin: false,
  hobbies: [],
};

export default function authen(state = initialState, action) {
  switch (action.type) {
    case types.GET_ALL_HOBBIES_SUCCESS:
      console.log(`action.payload => ${action.payload.hobbies}`);
      return {
        ...state,
        hobbies: action.payload.hobbies
      }
    default:
      return state
  }
}
