/**
* Created by bav on Thu May 31 2018
* Copyright (c) 2018 bav
*/

import * as types from '../types';
import { getLanguageDefault } from '../../common/utils';

const initialState = {
  language: getLanguageDefault(),
  orientation: 'PORTRAIT',
  theme: 'light-theme',
  barStyle: 'light-content',
  notify: true,
  vibrate: true,
  firstInstall: true,
  user: {},
  userTemp: {},
  linkedinId: undefined,
  token: undefined,
};

export default function config(state = initialState, action) {
  switch (action.type) {
    case types.SET_LANGUAGE:
      return {
        ...state,
        language: action.payload
      }
    case types.SET_ORIENTATION:
      return {
        ...state,
        orientation: action.payload
      }
    case types.SET_THEME:
      return {
        ...state,
        theme: action.payload
      }
    case types.SET_FIRST_INSTALL:
      return {
        ...state,
        firstInstall: action.payload
      }
    case types.LOGIN_SUCCESS: {
      return {
        ...state,
        token: action.payload.token
      }
    }
    case types.REGISTER_SUCCESS: {
      return {
        ...state,
        user: action.payload.user,
        userTemp: {}
      }
    }
    case types.SAVE_USER_TEMP: {
      return {
        ...state,
        userTemp: action.payload.user,
      }
    }
    case types.SAVE_USER_LINKEDIN: {
      let userLinked = action.payload.user;
      console.log(`SAVE_USER_LINKEDIN userLinked => ${JSON.stringify(userLinked)}`);
      const user = {
        "linkedinId": userLinked.id,
        "emailAddress": userLinked.emailAddress,
        "firstName": userLinked.firstName,
        "lastName": userLinked.lastName,
        "headline": userLinked.headline,
        "avatarUrl": userLinked.pictureUrl,
        "currentPosition": userLinked.positions && userLinked.positions._total > 0 && userLinked.positions.values[0].title,
        "introduction": userLinked.summary,
        "location": userLinked.location && userLinked.location.name,
        "company": '',
        "phone": '',
        "interests": [],
        "educations": [
          {
            "school": "",
            "degree": "",
            "fieldOfStudy": "",
            "completed": ""
          }
        ],
        "socials": [
          {
            "type": "Linkedin",
            "name": ""
          },
          {
            "type": "Twitter",
            "name": ""
          },
          {
            "type": "Facebook",
            "name": ""
          },
          {
            "type": "WhatsApp",
            "name": ""
          },
          {
            "type": "Wechat",
            "name": ""
          },
          {
            "type": "Instagram",
            "name": ""
          }
        ]
      };
      return {
        ...state,
        user: user,
        userTemp: user,
        linkedinId: userLinked.id,
      }
    }
    case types.LOGOUT: {
      return {
        ...state,
        user: {},
        linkedinId: undefined,
        token: undefined
      }
    }
    default:
      return state
  }
}
