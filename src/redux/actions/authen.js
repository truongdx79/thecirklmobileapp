/**
* Created by bav on Mon Jul 23 2018
* Copyright (c) 2018 bav
*/

import * as types from '../types';

export const autoLogin = () => {
  return {
    type: types.AUTO_LOGIN
  }
}

export const loginAction = (data) => {
  return {
    type: types.LOGIN_ACTION,
    payload: data
  }
}

export const loginSuccess = (data) => {
  return {
    type: types.LOGIN_SUCCESS,
    payload: data
  }
}

export const loginFailed = () => {
  return {
    type: types.LOGIN_FAILED
  }
}

export const logOut = () => {
  return {
    type: types.LOGOUT
  }
}

export const logOutSuccess = () => {
  return {
    type: types.LOGOUT_SUCCESS
  }
}

export const logOutFailed = () => {
  return {
    type: types.LOGOUT_FAILED
  }
}

export const register = (data) => {
  return {
    type: types.REGISTER,
    payload: data
  }
}

export const registerSuccess = (data) => {
  return {
    type: types.REGISTER_SUCCESS,
    payload: data
  }
}

export const registerFailed = () => {
  return {
    type: types.REGISTER_FAILED
  }
}

export const updateProfile = (data) => {
  return {
    type: types.UPDATE_PROFILE,
    payload: data
  }
}

export const updateProfileSuccess = (data) => {
  return {
    type: types.UPDATE_PROFILE_SUCCESS,
    payload: data
  }
}

export const updateProfileFailed = () => {
  return {
    type: types.UPDATE_PROFILE_FAILED
  }
}

export const getAllHobbies = (data) => {
  return {
    type: types.GET_ALL_HOBBIES,
    payload: data
  }
}

export const getAllHobbiesSuccess = (data) => {
  return {
    type: types.GET_ALL_HOBBIES_SUCCESS,
    payload: data
  }
}

export const getAllHobbiesFailed = () => {
  return {
    type: types.GET_ALL_HOBBIES_FAILED
  }
}

export const saveUserTemp = (data) => {
  return {
    type: types.SAVE_USER_TEMP,
    payload: data
  }
}