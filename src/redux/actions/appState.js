/**
* Created by bav on Sat Jun 02 2018
* Copyright (c) 2018 bav
*/
import * as types from '../types';

export const appStateChange = (type) => {
  return {
    type: types.APP_STATE_CHANGE,
    payload: type
  }
}
