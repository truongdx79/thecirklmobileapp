/**
* Created by bav on Thu May 31 2018
* Copyright (c) 2018 bav
*/

import LocalizedStrings from 'react-native-localization';
import * as types from '../types';
import Langs from '../../languages';

export const setLanguage = (lang) => {
  for (let i = 0; i < Langs.length; i++) {
    Langs[i].default.setLanguage(lang)
  }

  return {
    type: types.SET_LANGUAGE,
    payload: lang
  }
}

export const setVibrate = (value) => {
  return {
    type: types.SET_VIBRATE,
    payload: value
  }
}

export const setTheme = (theme) => {
  return {
    type: types.SET_THEME,
    payload: theme
  }
}

export const setNotify = (notify) => {
  return {
    type: types.SET_NOTIFYCATION,
    payload: notify
  }
}

export const setOrientation = (orientation) => {
  return {
    type: types.SET_ORIENTATION,
    payload: orientation
  }
}

export const setFirstInstall = (isFirstIntall) => {
  return {
    type: types.SET_FIRST_INSTALL,
    payload: isFirstIntall
  }
}

export const saveUserLinkedin = (user) => {
  return {
    type: types.SAVE_USER_LINKEDIN,
    payload: user
  }
}