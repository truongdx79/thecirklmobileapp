/**
* Created by bav on Thu May 31 2018
* Copyright (c) 2018 bav
*/

'use strick';

import { all, fork } from 'redux-saga/effects';
import * as configs from './config';
import * as appState from './appState';
import * as authen from './authen';

export default function* rootSaga() {
    yield all([
        ...Object.values(configs),
        ...Object.values(appState),
        ...Object.values(authen),
    ].map(fork));
}
