/**
* Created by bav on Mon Jul 23 2018
* Copyright (c) 2018 bav
*/

import { takeLatest, put, take, select } from "redux-saga/effects";
const tokenSelect = state => state.config.token;
import langs from '../../languages/common';
import { F_POST, F_PUT, F_GET, F_DELETE } from '../../api/index';

export function* GET(url, data) {
  const token = yield select(tokenSelect);
  window.customLoading.show();
  try {
    const response = yield F_GET(url, data, token)
    window.customLoading.hide();

    if (response.message !== 'Success') {
      window.customAlert.alert({
        title: langs.notification,
        message: response.message,
        leftButton: { text: langs.ok }
      });
    }
    return response
  } catch (e) {
    window.customLoading.hide();

    window.customAlert.alert({
      title: langs.notification,
      message: langs.errorInternet,
      leftButton: { text: langs.ok }
    });
    return new Error(e)
  }
}

export function* POST(url, data) {
  const token = yield select(tokenSelect);
  window.customLoading.show();
  try {
    const response = yield F_POST(url, data, token)
    window.customLoading.hide();

    if (response.message !== 'Success') {
      window.customAlert.alert({
        title: langs.notification,
        message: response.message,
        leftButton: { text: langs.ok }
      });
    }
    return response
  } catch (e) {
    window.customLoading.hide();

    window.customAlert.alert({
      title: langs.notification,
      message: langs.errorInternet,
      leftButton: { text: langs.ok }
    });
    return new Error(e)
  }
}

export function* DELETE(url, data) {
  const token = yield select(tokenSelect);
  window.customLoading.show();
  try {
    const response = yield F_DELETE(url, data, token)
    window.customLoading.hide();

    if (response.message !== 'Success') {
      window.customAlert.alert({
        title: langs.notification,
        message: response.message,
        leftButton: { text: langs.ok }
      });
    }
    return response
  } catch (e) {
    window.customLoading.hide();

    window.customAlert.alert({
      title: langs.notification,
      message: langs.errorInternet,
      leftButton: { text: langs.ok }
    });
    return new Error(e)
  }
}

export function* PUT(url, data) {
  const token = yield select(tokenSelect);
  window.customLoading.show();
  try {
    const response = yield F_PUT(url, data, token)
    window.customLoading.hide();

    if (response.message !== 'Success') {
      window.customAlert.alert({
        title: langs.notification,
        message: response.message,
        leftButton: { text: langs.ok }
      });
    }
    return response
  } catch (e) {
    window.customLoading.hide();

    window.customAlert.alert({
      title: langs.notification,
      message: langs.errorInternet,
      leftButton: { text: langs.ok }
    });
    return new Error(e)
  }
}