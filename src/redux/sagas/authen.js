/**
* Created by bav on Mon Jul 23 2018
* Copyright (c) 2018 bav
*/

import { takeLatest, put, select, call } from "redux-saga/effects";
import { delay } from 'redux-saga';
import Toast from '@remobile/react-native-toast';
import * as types from '../types';
import { Actions } from "react-native-router-flux";
import { POST, PUT, GET } from './api';
import { F_POST, F_PUT, F_GET, F_DELETE } from '../../api/index';
import { API_STATUS } from '../../api/api_status';
import { API_TYPES } from '../../api/api_types';
import {
  loginSuccess, loginFailed, registerSuccess, registerFailed, updateProfileSuccess, updateProfileFailed, getAllHobbiesSuccess, getAllHobbiesFailed
} from '../actions/authen';
import langs from '../../languages/common';

const tokenSelect = state => state.config.token;

function* sagaAutoLogin() {
  try {
    const token = yield select(tokenSelect);
    if (token) {
      Actions.home();
    } else {
      Actions.login();
    }
  } catch (e) {
    console.log(`auto login error => ${e}`);
  }
}

export function* watchAutoLogin() {
  yield takeLatest(types.AUTO_LOGIN, sagaAutoLogin)
}

function* sagaLoginAction(action) {
  try {
    window.customLoading.show();
    let data = action.payload.linkedinId;//5a90f95c91c64b13f890ed8s
    let res = yield F_POST(API_TYPES.LOGIN, { linkedinId: data });
    window.customLoading.hide();
    console.log(`res => ${JSON.stringify(res)}`);
    if (res.error === '' && res.message === 'Success') {
      yield put(loginSuccess({ token: res.data.token }));
      action.payload.callbackLogin && action.payload.callbackLogin(true);
    } else if (res.error === '') {
      yield put(loginFailed());
      action.payload.callbackLogin && action.payload.callbackLogin(false);
    } else {
      window.customAlert.alert({
        title: langs.notification,
        message: res.message,
        leftButton: { text: langs.ok }
      });
      yield put(loginFailed());
      action.payload.callbackLogin && action.payload.callbackLogin(false);
    }
  } catch (e) {
    console.log('cache login action', e);
    yield put(loginFailed());
    action.payload.callbackLogin && action.payload.callbackLogin(false);
    window.customLoading.hide();

    window.customAlert.alert({
      title: langs.notification,
      message: langs.errorInternet,
      leftButton: { text: langs.ok }
    });
  }
}

export function* watchLoginAction() {
  yield takeLatest(types.LOGIN_ACTION, sagaLoginAction);
}

function* sagaLogOut(action) {
  yield delay(100);
  yield Actions.popTo('login');
}

export function* watchLogOutAction() {
  yield takeLatest(types.LOGOUT, sagaLogOut);
}

function* sagaRegister(action) {
  try {
    const data = action.payload.user;
    //   Toast.showShortCenter(langs.sendConfirmCodeSuccess);
    let res = yield POST(API_TYPES.REGISTER, data);
    console.log('res', res);
    if (res.error === '' && res.message === 'Success') {
      yield put(registerSuccess({ user: data }));
      action.payload.callbackRegister && action.payload.callbackRegister(true);
    } else {
      window.customAlert.alert({
        title: langs.notification,
        message: res.message,
        leftButton: { text: langs.ok }
      });
    }
  } catch (e) {
    console.log('error register', e);
    yield put(registerFailed());
  }
}

export function* watchRegister() {
  yield takeLatest(types.REGISTER, sagaRegister);
}

function* sagaUpdateProfile(action) {
  try {
    const data = action.payload.user;
    let res = yield POST(API_TYPES.UPDATE_PROFILE, data);
    if (res.error === '' && res.message === 'Success') {
      yield put(updateProfileSuccess(res));
    } else {
      window.customAlert.alert({
        title: langs.notification,
        message: res.message,
        leftButton: { text: langs.ok }
      });
      yield put(updateProfileFailed());
    }
  } catch (e) {
    console.log('catche update profile', e);
    yield put(updateProfileFailed());
  }
}

export function* watchUpdateProfile() {
  yield takeLatest(types.UPDATE_PROFILE, sagaUpdateProfile);
}

function* sagaGetAllHobbies(action) {
  try {
    let res = yield GET(API_TYPES.ALL_HOBBIES);
    console.log(`res => ${JSON.stringify(res)}`);
    if (res.error === '' && res.message === 'Success') {
      yield put(getAllHobbiesSuccess({ hobbies: res.data.result }));
    } else {
      window.customAlert.alert({
        title: langs.notification,
        message: res.message,
        leftButton: { text: langs.ok }
      });
      yield put(getAllHobbiesFailed());
    }
  } catch (e) {
    console.log('catche update profile', e);
    yield put(getAllHobbiesFailed());
  }
}

export function* watchGetAllHobbies() {
  yield takeLatest(types.GET_ALL_HOBBIES, sagaGetAllHobbies);
}
