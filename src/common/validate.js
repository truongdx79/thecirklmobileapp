/**
* Created by bav on Thu May 31 2018
* Copyright (c) 2018 bav
*/

/**
 * validate email
 */
export const isValidEmail = value => value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ? false : true

/**
 * validate web address
 */
export const isWebAddress = value => value && /\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i.test(value) ? true : false

export const isValidPhonenumber = input => {
    // var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    var phoneno = /([+]?\d{1,2}[.-\s]?)?(\d{3}[.-]?){2}\d{4}/;
    if (input.match(phoneno)) {
        return true;
    }
    else {
        return false;
    }
}
