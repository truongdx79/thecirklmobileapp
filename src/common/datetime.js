/**
* Created by bav on Thu May 31 2018
* Copyright (c) 2018 bav
*/

'use strick';

/**
 * @returns current day, month, year
 */
export const getDate = () => {
  const date = new Date();

  const dateTime = {
    day: date.getDate(),
    month: date.getMonth() + 1,
    year: date.getFullYear(),
  };

  return dateTime;
}

/**
 * @returns current hour, minutes, second
 */
export const getTime = () => {
  const date = new Date();

  const time = {
    hour: date.getHours(),
    minutes: date.getMinutes(),
    second: date.getSeconds()
  };

  return time;
}

/**
 * @returns current string day, month, year
 */
export const getDateString = () => {
  const date = new Date();

  const dateTime = {
    day: date.getDate() < 10 ? `0${date.getDate()}` : `${date.getDate()}`,
    month: date.getMonth() + 1 < 10 ? `0${date.getMonth() + 1}` : `${date.getMonth() + 1}`,
    year: `${date.getFullYear()}`,
  };

  return dateTime;
}

/**
 * @returns current string hour, minutes, second
 */
export const getTimeString = () => {
  const date = new Date();

  const time = {
    hour: date.getHours() < 10 ? `0${date.getHours()}` : `${date.getHours()}`,
    minutes: date.getMinutes() < 10 ? `0${date.getMinutes()}` : `${date.getMinutes()}`,
    second: date.getSeconds() < 10 ? `0${date.getSeconds()}` : `${date.getSeconds()}`
  };

  return time;
}
