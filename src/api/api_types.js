/**
* Created by bav on Mon Jul 23 2018
* Copyright (c) 2018 bav
*/

export const API_TYPES = {
  LOGIN: '/login',
  REGISTER: '/api/user/insert',
  UPDATE_PROFILE: '/api/user/update',
  ALL_HOBBIES: '/api/interest',
}