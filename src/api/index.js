/**
 * Created by duydatpham@gmail.com on Mon Aug 27 2018
 * Copyright (c) 2018 duydatpham@gmail.com
 */

let BASE_URL = 'http://168.63.251.166:8685';

export function F_POST(url, data, token, language = 'vi', timeout) {
  console.log('POST ', url, JSON.stringify(data), token, language, timeout);
  return new Promise((resolve, reject) => {
    let timeoutId = undefined
    let didTimeOut = false
    if (timeout) {
      timeoutId = setTimeout(() => {
        didTimeOut = true
        resolve({
          success: false
        })
      }, timeout);
    }

    fetch(BASE_URL + url, {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `${token}`
      },
      method: 'POST',
      body: JSON.stringify(data),
    })
      .then(res => res.json())
      .then(json => {
        if (timeoutId) {
          clearTimeout(timeoutId)
        }
        if (didTimeOut) return
        console.log('POST res::', url, json);
        resolve(json);
      })
      .catch(e => {
        if (timeoutId) {
          clearTimeout(timeoutId)
        }
        if (didTimeOut) return
        reject(e);
      });
  });
}

export function F_PUT(url, data, token, apikey, language = 'vi') {
  console.log('PUT ', url, data, token);
  return new Promise((resolve, reject) => {
    fetch(BASE_URL + url, {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `${token}`
      },
      method: 'PUT',
      body: JSON.stringify(data),
    })
      .then(res => res.json())
      .then(json => {
        console.log('PUT res::', url, json);
        resolve(json);
      })
      .catch(e => {
        reject(e);
      });
  });
}

export function F_GET(url, data, token, apikey, language = 'vi') {
  console.log('GET ', url, data, token);
  return new Promise((resolve, reject) => {
    let params = url;
    if (data) {
      params += '?';
      let i = 0;
      for (const key in data) {
        if (data[key] !== undefined)
          if (i != 0) params += `&${key}=${data[key]}`;
          else params += `${key}=${data[key]}`;
        i++;
      }
    }
    fetch(BASE_URL + params, {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `${token}`
      },
      method: 'GET',
    })
      .then(res => res.json())
      .then(json => {
        console.log('GET_FETCH res::', url, json);
        resolve(json);
      })
      .catch(e => {
        reject(e);
      });
  });
}

export function F_DELETE(url, data, token) {
  console.log('DELETE ', url, data, token)
  return new Promise((resolve, reject) => {
    let params = BASE_URL + url
    if (data) {
      params += '?'
      let i = 0
      for (let key in data) {
        if (data[key] != '')
          if (i != 0)
            params += '&' + key + '=' + data[key]
          else
            params += key + '=' + data[key]
        i++
      }
    }
    fetch(params,
      {
        // mode: 'no-cors',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `${token}`
        },
        method: 'DELETE',
      })
      .then((res) => { return res.json(); })
      .then(json => {
        console.log('DELETE res::', url, json)
        resolve(json)
      })
      .catch(e => {
        console.log(url, e)
        Alert.alert(langs.notify, langs.errorNetwork)
        reject(e)
      })
  })
}

export function F_UPLOAD(files) {
  console.log('UPLOAD::files', files)
  return new Promise((resolve, reject) => {

    if (!files) {
      resolve({
        "success": true,
        "statusCode": 200,
        "message": "Upload thành công",
        "data": {
          "files": [

          ]
        }
      })
      return
    }

    let formData = new FormData();
    if (files instanceof Array) {
      if (files.length == 0) {
        resolve({
          "success": true,
          "statusCode": 200,
          "message": "Upload thành công",
          "data": {
            "files": [

            ]
          }
        })
        return
      }
      for (let i = 0; i < files.length; i++) {
        console.log('name---upload::', files[i].name.replace(/ /gi, '_'))
        formData.append('UploadForm[files][]', { uri: files[i].url, name: files[i].name.replace(/ /gi, '_'), type: 'multipart/form-data' });

      }
    } else {
      console.log('name---upload::', files.name.replace(/ /gi, '_'))
      formData.append('UploadForm[files][]', { uri: files.url, name: files.name.replace(/ /gi, '_'), type: 'multipart/form-data' });
    }

    fetch(URL_UPLOAD_FILE_SERVER, {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `${token}`
      },
      method: 'POST',
      body: formData
    })
      .then((res) => { return res.json(); })
      .then(json => {
        resolve(json)
      })
      .catch(e => {
        Alert.alert(langs.notify, langs.errorNetwork)
        reject(e)
      })
  })
}

export function F_DELETE_FILES(files) {
  console.log('DELETE_FILES ', files)
  return new Promise((resolve, reject) => {
    fetch(URL_DELETE_FILE_SERVER,
      {
        // mode: 'no-cors',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `${token}`
        },
        method: 'POST',
        body: JSON.stringify({ files })
      })
      .then((res) => res.json())
      .then(json => {
        console.log('DELETE_FILES res::', json)
        resolve(json)
      })
      .catch(e => {
        console.log('/site/del-file', e)
        Alert.alert(langs.notify, langs.errorNetwork)
        reject(e)
      })
  })
}
