/**
* Created by bav on Sat Jul 14 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import RegisterComponent from '../../screens/register';
import { BodyLogin } from '../../components';

class Register extends PureComponent {
  render() {
    return (
      <BodyLogin>
        <RegisterComponent {...this.props} />
      </BodyLogin>
    )
  }
}

const mapDispathToProps = {

}

const mapStateToProps = (state) => {
  return {
    language: state.config.language,
    orientation: state.config.orientation
  }
}

export default connect(mapStateToProps, mapDispathToProps)(Register);
