/**
* Created by bav on Sat Jul 14 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import EventDetailComponent from '../../screens/home/EventDetail';
import { Body } from '../../components';

class EventDetail extends PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Body paddingBottom={true}>
        {
          <EventDetailComponent {...this.props} />
        }
      </Body>
    );
  }
}

const mapDispathToProps = {
}

const mapStateToProps = (state) => {
  return {
    language: state.config.language,
    orientation: state.config.orientation,
    user: state.config.user,
  }
}

export default connect(mapStateToProps, mapDispathToProps)(EventDetail);
