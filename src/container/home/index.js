/**
* Created by bav on Sat Jul 14 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import HomeComponent from '../../screens/home';
import { Body } from '../../components';

class Home extends PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Body paddingBottom={true}>
        {
          <HomeComponent {...this.props} />
        }
      </Body>
    );
  }
}

const mapDispathToProps = {
}

const mapStateToProps = (state) => {
  return {
    language: state.config.language,
    orientation: state.config.orientation,
    user: state.config.user,
  }
}

export default connect(mapStateToProps, mapDispathToProps)(Home);
