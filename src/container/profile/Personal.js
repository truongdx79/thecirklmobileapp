/**
* Created by bav on Sat Jul 14 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PersonalComponent from '../../screens/profile/Personal';
import { Body } from '../../components';
import { saveUserTemp } from '../../redux/actions/authen';

class Personal extends PureComponent {
  render() {
    return (
      <Body>
        {
          <PersonalComponent {...this.props} />
        }
      </Body>
    );
  }
}

const mapDispathToProps = {
  saveUserTemp,
}

const mapStateToProps = (state) => {
  return {
    language: state.config.language,
    orientation: state.config.orientation,
    userTemp: state.config.userTemp,
  }
}

export default connect(mapStateToProps, mapDispathToProps)(Personal);
