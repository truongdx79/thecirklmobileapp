/**
* Created by bav on Sat Jul 14 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import InterestComponent from '../../screens/profile/Interest';
import { BodyLogin } from '../../components';
import { getAllHobbies, saveUserTemp } from '../../redux/actions/authen';

class Interest extends PureComponent {
  constructor(props) {
    super(props);
    this.props.getAllHobbies();
  }

  render() {
    return (
      <BodyLogin>
        {
          <InterestComponent {...this.props} />
        }
      </BodyLogin>
    );
  }
}

const mapDispathToProps = {
  getAllHobbies,
  saveUserTemp,
}

const mapStateToProps = (state) => {
  return {
    language: state.config.language,
    orientation: state.config.orientation,
    userTemp: state.config.userTemp,
    hobbies: state.authen.hobbies,
  }
}

export default connect(mapStateToProps, mapDispathToProps)(Interest);
