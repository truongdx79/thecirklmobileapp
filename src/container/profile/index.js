/**
* Created by bav on Sat Jul 14 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import UpdateProfileComponent from '../../screens/profile/UpdateProfile';
import { Body } from '../../components';
import { register } from '../../redux/actions/authen';

class UpdateProfile extends PureComponent {
  render() {
    return (
      <Body>
        {
          <UpdateProfileComponent {...this.props} />
        }
      </Body>
    );
  }
}

const mapDispathToProps = {
  register,
}

const mapStateToProps = (state) => {
  return {
    language: state.config.language,
    orientation: state.config.orientation,
    userTemp: state.config.userTemp,
  }
}

export default connect(mapStateToProps, mapDispathToProps)(UpdateProfile);
