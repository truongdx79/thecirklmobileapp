/**
* Created by bav on Sat Jul 14 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import ForgotPasswordComponent from '../../screens/forgotPassword';
import { BodyLogin } from '../../components';

class ForgotPassword extends PureComponent {
  render() {
    return (
      <BodyLogin>
        <ForgotPasswordComponent {...this.props} />
      </BodyLogin>
    )
  }
}

const mapDispathToProps = {
  
}

const mapStateToProps = (state) => {
  return {
    language: state.config.language,
    orientation: state.config.orientation
  }
}

export default connect(mapStateToProps, mapDispathToProps)(ForgotPassword);
