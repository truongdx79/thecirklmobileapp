/**
* Created by bav on Tue Jul 17 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import CreatePasswordComponent from '../../screens/forgotPassword/CreatePassword';
import { BodyLogin } from '../../components';

class CreatePassword extends PureComponent {
  render() {
    return (
      <BodyLogin>
        {
          <CreatePasswordComponent {...this.props} />
        }
      </BodyLogin>
    )
  }
}

const mapDispathToProps = {

}

const mapStateToProps = (state) => {
  return {
    language: state.config.language
  }
}

export default connect(mapStateToProps, mapDispathToProps)(CreatePassword);
