/**
* Created by bav on Sat Jul 14 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import DashboardComponent from '../../screens/dashboard';
import { Body } from '../../components';

class Dashboard extends PureComponent {
  render() {
    return (
      <Body>
        {
          <DashboardComponent {...this.props} />
        }
      </Body>
    );
  }
}

const mapDispathToProps = {
  
}

const mapStateToProps = (state) => {
  return {
    language: state.config.language,
    orientation: state.config.orientation
  }
}

export default connect(mapStateToProps, mapDispathToProps)(Dashboard);
