/**
* Created by bav on Sat Jul 14 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import SettingComponent from '../../screens/setting';
import { Body } from '../../components';
import { logOut } from '../../redux/actions/authen';

class Setting extends PureComponent {
  render() {
    return (
      <Body>
        {
          <SettingComponent {...this.props} />
        }
      </Body>
    );
  }
}

const mapDispathToProps = {
  logOut,
}

const mapStateToProps = (state) => {
  return {
    language: state.config.language,
    orientation: state.config.orientation
  }
}

export default connect(mapStateToProps, mapDispathToProps)(Setting);
