/**
* Created by bav on Sat Jul 14 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import LoginLinkedinComponent from '../../screens/login/LoginLinkedin';
import { setTheme } from '../../redux/actions/config';
import { BodyLogin } from '../../components';

class LoginLinkedin extends PureComponent {
  render() {
    return (
      <BodyLogin>
        <LoginLinkedinComponent {...this.props} />
      </BodyLogin>
    )
  }
}

const mapDispathToProps = {
  setTheme
}

const mapStateToProps = (state) => {
  return {
    language: state.config.language,
    theme: state.config.theme
  }
}

export default connect(mapStateToProps, mapDispathToProps)(LoginLinkedin);
