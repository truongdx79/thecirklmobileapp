/**
* Created by bav on Sat Jul 14 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import LoginComponent from '../../screens/login';
import { setTheme } from '../../redux/actions/config';
import { BodyLogin } from '../../components';
import { loginAction } from '../../redux/actions/authen';
import { saveUserLinkedin } from '../../redux/actions/config';

class Login extends PureComponent {
  render() {
    return (
      <BodyLogin>
        <LoginComponent {...this.props} />
      </BodyLogin>
    )
  }
}

const mapDispathToProps = {
  setTheme,
  loginAction,
  saveUserLinkedin
}

const mapStateToProps = (state) => {
  return {
    language: state.config.language,
    theme: state.config.theme,
    linkedinId: state.config.linkedinId,
  }
}

export default connect(mapStateToProps, mapDispathToProps)(Login);
