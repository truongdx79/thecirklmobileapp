/**
* Created by bav on Mon Jul 16 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import AppIntroComponent from '../../screens/Intro';

class AppIntro extends PureComponent {
  render() {
    return <AppIntroComponent {...this.props} />
  }
}

const mapDispathToProps = {
  
}

const mapStateToProps = (state) => {
  return {
    language: state.config.language
  }
}

export default connect(mapStateToProps, mapDispathToProps)(AppIntro);
