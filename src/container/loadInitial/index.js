/**
* Created by bav on Thu May 31 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import LoadInitialComponent from '../../screens/loadInitial';
import { autoLogin } from '../../redux/actions/authen';

class LoadInitial extends PureComponent {
  render() {
    return <LoadInitialComponent {...this.props} />
  }
}

const mapDispathToProps = {
  autoLogin,
}

const mapStateToProps = (state) => {
  return {
    language: state.config.language,
  }
}

export default connect(mapStateToProps, mapDispathToProps)(LoadInitial);
