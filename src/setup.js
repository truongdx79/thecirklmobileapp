/**
* Created by bav on Thu May 31 2018
* Copyright (c) 2018 bav
*/

'use strick';

import React, { PureComponent } from 'react';
import { LayoutAnimation, UIManager, Text, Platform, StatusBar } from 'react-native';
import { Provider } from 'react-redux';
// import { Sentry, SentryLog } from 'react-native-sentry';
import SplashScreen from 'react-native-smart-splash-screen';
import Orientation from 'react-native-orientation';
import DeviceInfo from 'react-native-device-info';
import { setFont, isTablet } from './common/utils';
import configureStore from './redux/configureStore';
import { CustomLayoutSpring } from './common/animation';
import App from './app';
import { Loading } from './components';
import { setOrientation, setToken } from './redux/actions/config';
import { common } from './config/common';
import { View } from 'react-native-animatable';
// import Connection from './connection';

// Disable ignored remote debugger
console.ignoredYellowBox = ['Remote debugger', 'Setting a timer', 'Warning: isMounted(...) is deprecated in plain JavaScript React classes'];
Text.defaultProps.allowFontScaling = false;

// Set font family default
setFont('Quicksand');

// if (!__DEV__) {
//   Sentry.config('https://49b0519f2bde491d9a6f7c352af1d934@sentry.io/1242615', {
//     deactivateStacktraceMerging: true,
//     logLevel: SentryLog.Verbose,
//     // currently sentry is not reporting errors on android using the native module
//     disableNativeIntegration: Platform.OS === 'android',
//   }).install();
// }


export default class Setup extends PureComponent {
  constructor(props) {
    super(props);
    UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
    this.state = {
      isLoading: true,
      store: configureStore(() => {
        LayoutAnimation.configureNext(CustomLayoutSpring);
        this.setState({ isLoading: false });
      })
    }
  }

  componentDidMount() {
    // if (isTablet) {
    //   Orientation.lockToLandscapeLeft();
    // } else {
    Orientation.lockToPortrait();
    // }

    const initial = Orientation.getInitialOrientation();
    if (initial === 'PORTRAIT') {
      this.state.store.dispatch(setOrientation('PORTRAIT'))
    } else {
      this.state.store.dispatch(setOrientation('LANDSCAPE'))
    }

    Orientation.addOrientationListener(this._orientationDidChange);

    SplashScreen.close({
      animationType: SplashScreen.animationType.scale,
      duration: 200,
      delay: 200,
    });
  }

  componentWillUnmount() {
    Orientation.removeOrientationListener(this._orientationDidChange);
  }

  _orientationDidChange = (orientation) => {
    if (orientation === 'LANDSCAPE') {
      this.state.store.dispatch(setOrientation('LANDSCAPE'))
    } else {
      this.state.store.dispatch(setOrientation('PORTRAIT'))
    }
  }

  render() {
    let { isLoading, store } = this.state;

    if (isLoading) {
      return <View></View>
      // return <Loading backgroundColor={common().BACKGROUND_GRADIENT1} colorIndicator={common().COLOR_INDICATOR} />
    }

    return (
      <Provider store={store}>
        <App />
      </Provider>
    );
  }
}
