/**
* Created by bav on Sat Jul 14 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';

class Setting extends PureComponent {
  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={this.onPress}>
          <Text>Setting Component</Text>
        </TouchableOpacity>
      </View>
    );
  }

  onPress = () => {
    console.log(`logout clicked`);
    this.props.logOut();
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
});

export default Setting;
