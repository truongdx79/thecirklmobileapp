/**
* Created by bav on Sat Jul 14 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import { StyleSheet, View, Image } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { imgs } from '../../config/imgs';
import { common } from '../../config/common';
import { Button, ButtonLabel, Input } from '../../components';
import langs from '../../languages/login';

class ForgotPassword extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      email: ''
    }
  }

  render() {
    const { email } = this.state;

    return (
      <View style={styles.container}>
        <View style={styles.viewLogo}>
          <Image
            style={styles.logo}
            source={imgs().logo}
            resizeMode='contain'
          />
        </View>
        <View style={styles.viewMiddle}>
          <View style={styles.viewInput}>
            <Input
              style={styles.input}
              placeholder={langs.email}
              rounded
              leftIcon
              rightIcon
              leftIconName='mail'
              value={email}
              onChangeText={this.onChangeEmail}
              returnKeyType='done'
            />
            <Button
              title={langs.resetPassword}
              style={styles.button}
              onPress={this.handleResetPassword}
              rounded
            />
          </View>
          <View style={styles.viewFooterMiddle} />
        </View>
        <View style={styles.viewFooter}>
          <ButtonLabel
            title={langs.back}
            titleStyle={styles.underline}
            onPress={this.onBackLogin}
          />
        </View>
      </View>
    );
  }

  handleResetPassword = () => {
    Actions.createPassword()
  }

  onChangeEmail = (text) => {
    this.setState({
      email: text
    })
  }

  onBackLogin = () => {
    Actions.pop()
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  viewLogo: {
    flex: 2,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  viewMiddle: {
    flex: 5,
    alignItems: 'center',
    justifyContent: 'center'
  },
  viewFooter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  viewInput: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center'
  },
  viewFooterMiddle: {
    flex: 1
  },
  button: {
    marginBottom: 8
  },
  input: {
    marginBottom: 22
  },
  underline: {
    textDecorationLine: 'underline',
    fontSize: common().FONT_SIZE_SMALL
  }
});

export default ForgotPassword;
