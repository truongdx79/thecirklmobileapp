/**
* Created by bav on Tue Jul 17 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import { StyleSheet, View, Image } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { imgs } from '../../config/imgs';
import { common } from '../../config/common';
import { Button, ButtonLabel, InputPassword } from '../../components';
import langs from '../../languages/login';

class CreatePassword extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      newPassword: '',
      confirmPassword: ''
    }
  }

  render() {
    const { newPassword, confirmPassword } = this.state;

    return (
      <View style={styles.container}>
        <View style={styles.viewLogo}>
          <Image
            style={styles.logo}
            source={imgs().logo}
            resizeMode='contain'
          />
        </View>
        <View style={styles.viewMiddle}>
          <View style={styles.viewInput}>
            <InputPassword
              style={styles.input}
              placeholder={langs.newPassword}
              rounded
              leftIcon
              rightIcon
              value={newPassword}
              onChangeText={this.onChangeNewPassword}
              textInputRef="newPass"
              ref="newPass"
              onSubmitEditing={() => this.focusNextField("confirmPass")}
            />
            <InputPassword
              style={styles.input}
              placeholder={langs.confirmPassword}
              rounded
              leftIcon
              rightIcon
              value={confirmPassword}
              onChangeText={this.onChangeConfirmPassword}
              returnKeyType='done'
              textInputRef="confirmPass"
              ref="confirmPass"
            />

          </View>
          <View style={styles.viewFooterMiddle} >
            <Button
              title={langs.resetPassword}
              style={styles.button}
              onPress={this.handleResetPassword}
              rounded
            />
          </View>
        </View>
        <View style={styles.viewFooter}>
          <ButtonLabel
            title={langs.back}
            titleStyle={styles.underline}
            onPress={this.onBackLogin}
          />
        </View>
      </View>
    );
  }

  focusNextField = (nextField) => {
    this.refs[nextField].focus();
  }

  handleResetPassword = () => {

  }

  onChangeNewPassword = (text) => {
    this.setState({
      newPassword: text
    })
  }

  onChangeConfirmPassword = (text) => {
    this.setState({
      confirmPassword: text
    })
  }

  onBackLogin = () => {
    Actions.pop()
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  viewLogo: {
    flex: 2,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  viewMiddle: {
    flex: 5,
    alignItems: 'center',
    justifyContent: 'center'
  },
  viewFooter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  viewInput: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center'
  },
  viewFooterMiddle: {
    flex: 1
  },
  button: {
    marginTop: 12,
    marginBottom: 8
  },
  input: {
    marginBottom: 22
  },
  underline: {
    textDecorationLine: 'underline',
    fontSize: common().FONT_SIZE_SMALL
  }
});

export default CreatePassword;
