/**
* Created by bav on Sat Jul 14 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import { StyleSheet, View, Image, Dimensions, Platform } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { MKButton } from 'react-native-material-kit';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
// import ImagePicker from 'react-native-image-crop-picker';
import { imgs } from '../../config/imgs';
import { common } from '../../config/common';
import { Button, ButtonLabel, Input, InputLabel, InputPassword, ActionSheet, Loading } from '../../components';
import langs from '../../languages/login';

const SCREEN = Dimensions.get('window');

class Register extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      email: '',
      newPassword: '',
      confirmPassword: '',
      avatar: null
    }
  }

  render() {
    const {
      username,
      email,
      newPassword,
      confirmPassword,
      avatar
    } = this.state;

    return (
      <View style={styles.container}>
        <View style={styles.viewLogo}>
          <MKButton
            style={styles.viewAvatar}
            onPress={this.addAvatar}
          >
            {
              avatar ? avatar.sourceURL ? <Image source={{ uri: avatar.sourceURL }} resizeMode='cover' style={styles.avatar} /> : 
              <Icon name='add-a-photo' color={common().COLOR_ICON_NORMAL} size={32} /> :
              <Icon name='add-a-photo' color={common().COLOR_ICON_NORMAL} size={32} />
            }
          </MKButton>
        </View>
        <View style={styles.viewMiddle}>
          <InputLabel
            style={styles.input}
            placeholder={langs.username}
            rounded
            // leftIcon
            rightIcon
            value={username}
            onChangeText={this.onChangeUsername}
            textInputRef="user"
            ref="user"
            onSubmitEditing={() => this.focusNextField("email")}
          />
          <Input
            style={styles.input}
            placeholder={langs.email}
            rounded
            leftIcon
            rightIcon
            leftIconName='email'
            value={email}
            onChangeText={this.onChangeEmail}
            textInputRef="email"
            ref="email"
            onSubmitEditing={() => this.focusNextField("newPassword")}
          />
          <InputPassword
            style={styles.input}
            placeholder={langs.newPassword}
            rounded
            leftIcon
            rightIcon
            value={newPassword}
            onChangeText={this.onChangeNewPassword}
            textInputRef="newPassword"
            ref="newPassword"
            onSubmitEditing={() => this.focusNextField("confirmPassword")}
          />
          <InputPassword
            style={styles.input}
            placeholder={langs.confirmPassword}
            rounded
            leftIcon
            rightIcon
            value={confirmPassword}
            onChangeText={this.onChangeConfirmPassword}
            returnKeyType='done'
            textInputRef="confirmPassword"
            ref="confirmPassword"
          />

          <Button
            title={langs.register}
            style={styles.button}
            onPress={this.handleRegister}
            rounded
          />
        </View>
        <View style={styles.viewFooter}>
          <ButtonLabel
            title={langs.login}
            firstTitle={langs.haveAccount}
            onPress={this.handleBackLogin}
            titleStyle={styles.txtLabel}
            firstTitleStyle={styles.txtLabel}
          />
        </View>

        <ActionSheet
          ref='modal'
          bottomTitle={langs.cancel}
          options={[
            { title: langs.fromLibrary, leftIconName: 'photo', onPress: this.onChooseLibrary },
            { title: langs.camera, leftIconName: 'photo-camera', onPress: this.onTakePhoto }
          ]}
        />
      </View>
    );
  }

  focusNextField = (nextField) => {
    this.refs[nextField].focus();
  };

  onChangeUsername = (text) => {
    this.setState({ username: text })
  }

  onChangeEmail = (text) => {
    this.setState({ email: text })
  }

  onChangeNewPassword = (text) => {
    this.setState({ newPassword: text })
  }

  onChangeConfirmPassword = (text) => {
    this.setState({ confirmPassword: text })
  }

  handleBackLogin = () => {
    Actions.pop()
  }

  addAvatar = () => {
    if (this.refs.modal) {
      this.refs.modal.show()
    }
  }

  onChooseLibrary = () => {
    // ImagePicker.openPicker({
    //   width: SCREEN.width,
    //   height: SCREEN.width,
    //   multiple: false,
    //   minFiles: 1,
    //   maxFiles: 1,
    //   mediaType: 'photo',
    //   compressImageQuality: 1,
    //   cropping: true
    // }).then(images => {
    //   console.log(images);
    //   if (Platform.OS === 'android') {
    //     images.sourceURL = images.path
    //     if (!images.filename) {
    //       images.filename = `${(new Date()).getTime()}.JPG`
    //     }
    //     if (!images.mime) {
    //       images.mime = 'image/jpeg'
    //     }
    //   } else {
    //     images.sourceURL = images.path
    //     if (!images.filename) {
    //       images.filename = `${(new Date()).getTime()}.JPG`
    //     }
    //     if (!images.mime) {
    //       images.mime = 'image/jpeg'
    //     }
    //   }

    //   this.setState({
    //     avatar: images
    //   })
    // }).catch(e => console.log(e));
  }

  onTakePhoto = () => {
    // ImagePicker.openCamera({
    //   width: SCREEN.width,
    //   height: SCREEN.width,
    //   cropping: true
    // }).then(image => {
    //   console.log(image);
    //   if (Platform.OS === 'android') {
    //     image.sourceURL = image.path
    //     if (!images.filename) {
    //       images.filename = `${(new Date()).getTime()}.JPG`
    //     }
    //     if (!image.mime) {
    //       image.mime = 'image/jpeg'
    //     }
    //   } else {
    //     image.sourceURL = image.path
    //     if (!images.filename) {
    //       images.filename = `${(new Date()).getTime()}.JPG`
    //     }
    //     if (!image.mime) {
    //       image.mime = 'image/jpeg'
    //     }
    //   }

    //   this.setState({
    //     avatar: image
    //   })
    // }).catch(e => console.log(e));
  }

  handleRegister = () => {

  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewLogo: {
    flex: 2,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  viewMiddle: {
    flex: 5,
    alignItems: 'center',
    justifyContent: 'center'
  },
  viewFooter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  viewAvatar: {
    width: 100,
    height: 100,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
    backgroundColor: 'rgba(255, 255, 255, 0.1)'
  },
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 50,
    overflow: 'hidden'
  },
  button: {
    marginBottom: 8
  },
  input: {
    marginBottom: 22
  },
  txtLabel: {
    fontSize: common().FONT_SIZE_SMALL
  }
});

export default Register;
