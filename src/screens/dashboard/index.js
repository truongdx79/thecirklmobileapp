/**
* Created by bav on Sat Jul 14 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import { StyleSheet, View, Text, ScrollView, Image, Dimensions } from 'react-native';
import { imgs } from '../../config/imgs';

const SCREEN = Dimensions.get('window')

class Dashboard extends PureComponent {
  render() {
    return (
      <ScrollView style={styles.container}>
        <Image
          source={imgs().backgroundImageLogin}
          resizeMode='cover'
          style={{ width: SCREEN.width, height: SCREEN.height }}
        />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center'
  }
});

export default Dashboard;
