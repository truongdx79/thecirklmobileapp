
import React, { PureComponent } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Image, ScrollView, Dimensions, Platform, ImageBackground } from 'react-native';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import langs from '../../languages/profile';
import { CustomNavbar, Button, InputLabel } from '../../components';
import { common } from '../../config/common';
import { imgs } from '../../config/imgs';
import { Actions } from 'react-native-router-flux';

const SCREEN = Dimensions.get('window');

class RowEducation extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      checked: false,
    }
  }

  render() {
    const {
      school,
      degree,
      fieldOfStudy,
      completed
    } = this.props.education;

    const index = this.props.index;
    if (this.props.resetCheck) {
      this.setState({ checked: false });
      this.props.deleteCheck();
    }

    console.log(`checked = ${this.state.checked}`);

    return (
      <View style={styles.container}>
        <View style={styles.viewLine} />
        <View style={{ padding: 16 }}>
          <View style={styles.viewMiddle}>
            <InputLabel
              style={[styles.input, {marginTop: 0}]}
              placeholder={''}
              title='School'
              // rightIcon
              value={school}
              onChangeText={this.onChangeSchool}
              textInputRef="school"
              ref="school"
              onSubmitEditing={() => this.focusNextField("degree")}
            />

            <InputLabel
              style={styles.input}
              placeholder={''}
              title='Degree'
              // rightIcon
              value={degree}
              onChangeText={this.onChangeDegree}
              textInputRef="degree"
              ref="degree"
              onSubmitEditing={() => this.focusNextField("fieldOfStudy")}
            />

            <InputLabel
              style={styles.input}
              placeholder={''}
              title='Field of study'
              // rightIcon
              value={fieldOfStudy}
              onChangeText={this.onChangeStudy}
              textInputRef="fieldOfStudy"
              ref="fieldOfStudy"
              onSubmitEditing={() => this.focusNextField("completed")}
            />

            <InputLabel
              style={styles.input}
              placeholder={''}
              title='Completed'
              keyboardType='numeric'
              // rightIcon
              value={completed}
              onChangeText={this.onChangeCompleted}
              textInputRef="completed"
              ref="completed"
              returnKeyType='done'
            />
          </View>
          {
            this.state.checked && <TouchableOpacity style={styles.rightIcon} onPress={this.onSelectItem}>
              <Icon name={'check-box'} size={24} color={'#12B8CB'} />
            </TouchableOpacity>
          }
          {
            !this.state.checked && <TouchableOpacity style={styles.rightIcon} onPress={this.onSelectItem}>
              <Icon name={'check-box-outline-blank'} size={24} color={common().COLOR_TEXT_INACTIVE} />
            </TouchableOpacity>
          }
        </View>
      </View>
    );
  }

  focusNextField = (nextField) => {
    this.refs[nextField].focus();
  };

  onChangeSchool = (text) => {
    this.onUpdateItem({
      ...this.props.education,
      school: text
    });
  }

  onChangeDegree = (text) => {
    this.onUpdateItem({
      ...this.props.education,
      degree: text
    });
  }

  onChangeStudy = (text) => {
    this.onUpdateItem({
      ...this.props.education,
      fieldOfStudy: text
    });
  }

  onChangeCompleted = (text) => {
    this.onUpdateItem({
      ...this.props.education,
      completed: text
    });
  }

  onSelectItem = () => {
    this.props.onSelectItem && this.props.onSelectItem(!this.state.checked, this.props.index);
    this.setState({
      checked: !this.state.checked,
    });
  }

  onUpdateItem = (item) => {
    this.props.onUpdateItem && this.props.onUpdateItem({
      index: this.props.index,
      item,
    });
  }

  resetCheck = () => {
    this.setState({
      checked: false,
    });

    console.log(`resetCheck`);
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: common().BACKGROUND_COLOR_CARD,
    // padding: 16,
    // marginBottom: 10,
    // shadowColor: '#000000',
    // elevation: 4,
    // shadowOffset: {
    //   width: 1,
    //   height: 1
    // },
    // shadowRadius: 1,
    // shadowOpacity: 0.3
  },
  viewMiddle: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  input: {
    marginTop: 20,
  },
  viewLine: {
    height: 12,
    backgroundColor: '#E4E4E4'
  },
  rightIcon: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: 10,
    right: 10,
  },
});

export default RowEducation;

