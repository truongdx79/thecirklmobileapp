
import React, { PureComponent } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Image, ScrollView, Dimensions, Platform, ImageBackground } from 'react-native';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import langs from '../../languages/profile';
import langCommon from '../../languages/common';
import { CustomNavbar, Button, InputLabel } from '../../components';
import { common } from '../../config/common';
import { imgs } from '../../config/imgs';
import { Actions } from 'react-native-router-flux';
import RowInterest from './RowInterest';
import GridView from 'react-native-super-grid';
import USER_DEFAULT from './UserDefault';

const SCREEN = Dimensions.get('window');

class Interest extends PureComponent {
  constructor(props) {
    super(props);

    let user = USER_DEFAULT;
    if (props.userTemp) {
      user = props.userTemp;
    }

    this.state = {
      user,
    }
  }

  render() {
    let {
      user
    } = this.state;

    let hobbies = user.interests || [];
    let allHobbies = this.props.hobbies;

    return (
      <View style={styles.container}>
        <CustomNavbar
          title={'Interests'}
          noBackground
        // rightIcon
        // rightImage={imgs().filter}
        // onRight={this.onFilter}
        />
        <View style={styles.viewHeader}>
          <Text style={styles.txtInfo}>
            Tap on interests you like.
          </Text>
        </View>

        <ScrollView style={styles.viewMiddle}>
          <GridView
            itemDimension={(SCREEN.width - (15 * 5)) / 4}
            items={allHobbies}
            style={styles.gridView}
            renderItem={item => (
              <RowInterest onPressItem={this.onPressItem} item={item} hobbies={hobbies} />
            )}
          />

          <View style={styles.viewFooter}>
            <View style={styles.viewButton}>
              <Button
                title={"Next"}
                style={styles.button}
                onPress={this.handleNext}
                rounded
                backgroundColor={'#FFDE03'}
                color={'rgba(0, 0, 0, 0.7)'}
                titleStyle={{ fontSize: common().FONT_SIZE_TITLE, fontWeight: common().FONT_WEIGHT_HEADER }}
              />
            </View>
          </View>

          <View style={style = styles.viewBottom} />
        </ScrollView>

      </View>
    );
  }

  focusNextField = (nextField) => {
    this.refs[nextField].focus();
  };

  onPressItem = (item) => {
    this.onSelectedHobbies(item);
  }

  onSelectedHobbies = (hobby) => {
    let hobbies = this.state.user.interests;
    var data = hobbies.filter((h) => {
      return h._id === hobby._id;
    });
    if (data && data.length > 0) {
      this.removeHobbies(hobbies, hobby);
    } else {
      hobbies.push(hobby);
    }

    const user = {
      ...this.state.user,
      hobbies: [...hobbies],
    };

    this.props.saveUserTemp({user: user});

    this.setState({
      user: user
    });
  }

  removeHobbies = (arr, item) => {
    for (var i = arr.length; i--;) {
      if (arr[i]._id === item._id) {
        arr.splice(i, 1);
      }
    }
  }

  handleNext = () => {
    let hobbies = this.state.user.interests;
    console.log(`hobbies => ${hobbies}`);
    if (!hobbies || !hobbies.length) {
      window.customAlert.alert({
        title: langCommon.notification,
        message: 'Please select an interest.',
        leftButton: { text: langCommon.ok }
      });
      return
    }
    Actions.personal();
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent'
  },
  viewHeader: {
    padding: 16
  },
  viewMiddle: {
    flex: 1
  },
  viewFooter: {
    // position: 'absolute',
    // bottom: 22,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 16,
  },
  viewButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    width: SCREEN.width - 34,
  },
  gridView: {
    flex: 1,
  },
  viewGridContainer: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
  },
  txtInfo: {
    color: 'rgba(255, 255, 255, 0.58)',
    fontSize: common().FONT_SIZE_HEADER,
  },
  viewBottom: {
    marginBottom: 20
  },
});

export default Interest;

