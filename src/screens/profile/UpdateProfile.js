import React, { PureComponent } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Image, ScrollView, Dimensions, Platform, ImageBackground } from 'react-native';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { CachedImage } from 'react-native-cached-image';
import langs from '../../languages/profile';
import { CustomNavbar, Button, Avatar } from '../../components';
import { common } from '../../config/common';
import { imgs } from '../../config/imgs';
import { Actions } from 'react-native-router-flux';
import Accordion from 'react-native-collapsible/Accordion';
import USER_DEFAULT from './UserDefault';

const SCREEN = Dimensions.get('window');

class UpdateProfile extends PureComponent {
  constructor(props) {
    super(props);

    let user = USER_DEFAULT;
    if (props.userTemp) {
      user = props.userTemp || USER_DEFAULT;
    }

    console.log(`UpdateProfile user => ${JSON.stringify(user)}`);

    this.state = {
      user,
      activeSections: [0]
    }
  }

  render() {
    let { user } = this.state;

    let hobbies = user.interests;
    let socials = user.socials;
    let educations = user.educations;

    // console.log(`user => ${JSON.stringify(user)}`);
    // console.log(`hobbies => ${JSON.stringify(hobbies)}`);

    return (
      <View style={styles.container}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <ImageBackground
            source={imgs().bgProfile}
            resizeMode='cover'
            style={styles.viewHeader}
          >
            <View style={{ flex: 1 }} >
              <View style={styles.viewTitle}>
                <Text numberOfLines={1} style={styles.txtHeader}>
                  {langs.title}
                </Text>
              </View>
              <View style={styles.separator} />
              <View style={styles.viewAvatar}>
                <Avatar
                  width={87}
                  // imageSource={user.avatarUrl}
                  imageSource={user.avatarUrl}
                />
                <Text numberOfLines={1} style={[styles.txtHeader, { fontSize: common().FONT_SIZE_HUGE, marginTop: 16 }]}>
                  {`${user.lastName} ${user.firstName} `}
                </Text>
                <Text numberOfLines={1} style={[styles.txtTitle, { fontSize: common().FONT_SIZE_SMALL, marginTop: 5 }]}>
                  {user.headline}
                </Text>
              </View>
            </View>
          </ImageBackground>

          <View style={styles.body}>
            <View style={styles.viewInfo}>
              <Text numberOfLines={1} style={styles.txtSubTitle}>
                Current position
                </Text>
              <Text numberOfLines={1} style={styles.txtDecription}>
                {user.currentPosition}
              </Text>
            </View>

            <View style={styles.viewInfo}>
              <Text numberOfLines={1} style={styles.txtSubTitle}>
                Company name
                </Text>
              <Text numberOfLines={1} style={styles.txtDecription}>
                {user.company}
              </Text>
            </View>

            <View style={styles.viewInfo}>
              <Text numberOfLines={1} style={styles.txtSubTitle}>
                Location
                </Text>
              <Text numberOfLines={1} style={styles.txtDecription}>
                {user.location}
              </Text>
            </View>

            <View style={styles.viewInfo}>
              <Text numberOfLines={1} style={styles.txtSubTitle}>
                Email
                </Text>
              <Text numberOfLines={1} style={styles.txtDecription}>
                {user.emailAddress}
              </Text>
            </View>

            <View style={styles.viewInfo}>
              <Text numberOfLines={1} style={styles.txtSubTitle}>
                Phone
                </Text>
              <Text numberOfLines={1} style={styles.txtDecription}>
                {user.phone}
              </Text>
            </View>

            <View style={styles.viewInfo}>
              <Text style={styles.txtSubTitle}>
                Introduction
                </Text>
              <Text style={styles.txtDecription}>
                {user.introduction}
              </Text>
            </View>
          </View>

          <View style={styles.body}>
            <View style={[styles.viewInfo, { paddingBottom: 0 }]}>
              <Text numberOfLines={1} style={styles.txtSubTitle}>
                Interests
              </Text>
              <View style={{ flexWrap: 'wrap', flexDirection: 'row', paddingVertical: 10 }}>
                {
                  hobbies.length > 0 && hobbies.map(hobby => {
                    return (
                      <Button
                        title={hobby.name}
                        style={styles.buttonHobby}
                        // onPress={() => this.onSelectedHobbies(hobby)}
                        // width={80}
                        height={30}
                        wrapContent
                        rounded
                        backgroundColor={this.checkHobbyId(hobby._id) ? '#B8E6EB' : '#EAEAEA'}
                        color={'rgba(0, 0, 0, 0.7)'}
                      />
                    )
                  })
                }
              </View>
            </View>
          </View>

          <View style={styles.body}>
            <Text numberOfLines={1} style={[styles.txtHeader, { color: 'black', marginBottom: 20 }]}>
              Education
                </Text>
            <Accordion
              sections={educations}
              activeSections={this.state.activeSections}
              // renderSectionTitle={this._renderSectionTitle}
              renderHeader={this._renderHeader}
              renderContent={this._renderContent}
              onChange={this._updateSections}
              expandMultiple={true}
              touchableComponent={TouchableOpacity}
            />
          </View>

          <View style={styles.body}>
            <Text numberOfLines={1} style={[styles.txtHeader, { color: 'black', marginBottom: 20 }]}>
              Contact info
            </Text>
            {
              socials.length > 0 && socials.map(social => {
                return (
                  social.name && <View style={styles.viewInfo2}>
                    <View style={styles.leftIcon}>
                      {
                        social.type === 'Linkedin' && <FontAwesome style={styles.icon} name={'linkedin'} size={24} color={'#5E5E5E'} />
                      }
                      {
                        social.type === 'Twitter' && <FontAwesome style={styles.icon} name={'twitter'} size={24} color={'#5E5E5E'} />
                      }
                      {
                        social.type === 'WhatsApp' && <FontAwesome style={styles.icon} name={'whatsapp'} size={24} color={'#5E5E5E'} />
                      }
                      {
                        social.type === 'Wechat' && <FontAwesome style={styles.icon} name={'weixin'} size={24} color={'#5E5E5E'} />
                      }
                      {
                        social.type === 'Instagram' && <FontAwesome style={styles.icon} name={'instagram'} size={24} color={'#5E5E5E'} />
                      }
                      {
                        social.type === 'Facebook' && <FontAwesome style={styles.icon} name={'facebook-f'} size={24} color={'#5E5E5E'} />
                      }
                    </View>
                    <Text numberOfLines={1} style={styles.txtContact}>
                      {social.name}
                    </Text>
                  </View>
                )
              })
            }

          </View>

          <View style={styles.viewButton}>
            <Button
              title={"Let's go"}
              style={styles.button}
              onPress={this.handleLetGo}
              rounded
              backgroundColor={'#FFDE03'}
              color={'rgba(0, 0, 0, 0.7)'}
              titleStyle={{ fontSize: common().FONT_SIZE_TITLE, fontWeight: common().FONT_WEIGHT_HEADER }}
            />
          </View>

          {/* <View style={style = styles.viewBottom} /> */}

        </ScrollView>
      </View>
    );
  }

  checkHobbyId = (id) => {
    let hobbies = this.state.user.interests;
    for (let hobby of hobbies) {
      if (hobby._id === id) {
        return true;
      }
    }

    return false;
  }

  onSelectedHobbies = (hobby) => {
    let hobbies = this.state.user.interests;
    var data = hobbies.filter((h) => {
      return h._id === hobby._id;
    });
    if (data && data.length > 0) {
      this.removeHobbies(hobbies, hobby);
    } else {
      hobbies.push(hobby);
    }

    this.setState({
      user: {
        ...this.state.user,
        hobbies
      }
    });
  }

  removeHobbies = (arr, item) => {
    console.log(`arr => ${JSON.stringify(arr)}`);
    for (var i = arr.length; i--;) {
      if (arr[i]._id === item._id) {
        arr.splice(i, 1);
      }
    }
    console.log(`arr2 => ${JSON.stringify(arr)}`);
  }

  handleLetGo = () => {
    this.props.register({ user: this.state.user, callbackRegister: this.handleCallback });
  }

  handleCallback = (isSuccess) => {
    // LayoutAnimation.configureNext(CustomLayoutSpring);
    if (isSuccess) {
      Actions.home();
    }

  }

  handleChangeAvatar = () => {
    this.props.updateResident && this.props.updateResident(this.state.user)
  }

  //render expand list education
  // _renderSectionTitle = section => {
  //   return (
  //     <View style={styles.content}>
  //       <Text>{section.content}</Text>
  //     </View>
  //   );
  // };

  _renderHeader = (section, index, isActive) => {
    return (
      <View style={styles.header}>
        <View style={styles.viewHeaderText}>
          <Text style={styles.headerText} numberOfLines={1}>{section.school}</Text>
        </View>

        <View style={styles.viewArrow}>
          {isActive && <Icon style={styles.iconArrow} name={'keyboard-arrow-down'} size={22} color={'#5E5E5E'} />}
          {!isActive && <Icon style={styles.iconArrow} name={'keyboard-arrow-right'} size={22} color={'#5E5E5E'} />}
        </View>
      </View>
    );
  };

  _renderContent = section => {
    return (
      <View>
        <View style={styles.viewInfo}>
          <Text numberOfLines={1} style={styles.txtSubTitle}>
            Degree
                </Text>
          <Text style={styles.txtDecription}>
            {section.degree}
          </Text>
        </View>

        <View style={styles.viewInfo}>
          <Text numberOfLines={1} style={styles.txtSubTitle}>
            Field of study
                </Text>
          <Text style={styles.txtDecription}>
            {section.fieldOfStudy}
          </Text>
        </View>

        <View style={styles.viewInfo}>
          <Text numberOfLines={1} style={styles.txtSubTitle}>
            Completed
                </Text>
          <Text style={styles.txtDecription}>
            {section.completed}
          </Text>
        </View>
      </View>
    );
  };

  _updateSections = activeSections => {
    console.log('section onChange', activeSections);
    this.setState({ activeSections });
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#E4E4E4'
  },
  body: {
    flex: 1,
    backgroundColor: common().BACKGROUND_COLOR_CARD,
    marginTop: 10,
    padding: 16,
    shadowColor: '#000000',
    elevation: 4,
    shadowOffset: {
      width: 1,
      height: 1
    },
    shadowRadius: 1,
    shadowOpacity: 0.3
  },
  body2: {
    backgroundColor: common().BACKGROUND_COLOR_CARD,
    padding: 16,
    shadowColor: '#000000',
    elevation: 4,
    shadowOffset: {
      width: 1,
      height: 1
    },
    shadowRadius: 1,
    shadowOpacity: 0.3
  },
  viewHeader: {
    width: SCREEN.width,
    height: 290,
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewTitle: {
    width: SCREEN.width,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: Platform.OS === 'ios' ? 40 : 16,
    marginBottom: 16,
  },
  viewAvatar: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatarImage: {
    width: SCREEN.width - 180,
    height: SCREEN.width - 180,
    marginVertical: 32,
    borderRadius: 8,
    borderWidth: 4,
    borderColor: 'white',
    overflow: 'hidden',
    position: 'relative'
  },
  avatar: {
    width: SCREEN.width - 188,
    height: SCREEN.width - 188,
    justifyContent: 'center',
    alignItems: 'center'
  },
  viewCameraButton: {
    position: 'absolute',
    bottom: 4,
    right: 4,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent'
  },
  row: {
    backgroundColor: 'transparent',
  },
  separator: {
    height: StyleSheet.hairlineWidth,
    backgroundColor: common().COLOR_SEPARATOR_WHITE
  },
  separatorGrey: {
    height: StyleSheet.hairlineWidth,
    backgroundColor: 'rgba(112, 112, 112, 1)',
    marginTop: 7.5
  },
  button: {
    alignSelf: 'center',
    flexDirection: 'row',
    paddingVertical: 4,
    // marginLeft: 18,
    // marginRight: 18
  },
  buttonHobby: {
    marginRight: 10,
    marginBottom: 10,
  },
  viewImage: {
    height: 44,
    width: 44,
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtHeader: {
    color: common().COLOR_TEXT_NORMAL,
    fontSize: common().FONT_SIZE_HEADER,
    fontWeight: common().FONT_WEIGHT_HEADER
  },
  txtTitle: {
    color: common().COLOR_TEXT_NORMAL,
    fontSize: common().FONT_SIZE_TITLE
  },
  txtSubTitle: {
    color: common().COLOR_TEXT_TITLE,
    fontSize: common().FONT_SIZE_SMALL
  },
  txtHobby: {
    color: 'rgba(0, 0, 0, 0.7)',
    fontSize: common().FONT_SIZE_SMALL
  },
  viewInfo: {
    backgroundColor: 'transparent',
    paddingBottom: 18
  },
  viewInfo2: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 18
  },
  leftIcon: {
    width: 28,
    height: 28,
    marginRight: 16,
    alignItems: 'center',
    justifyContent: 'center'
  },
  icon: {
  },
  txtContact: {
    color: common().COLOR_TEXT_ACTIVE,
    fontWeight: common().FONT_WEGHT_HEADER,
    fontSize: 15,
  },
  viewButton: {
    paddingBottom: 20,
    backgroundColor: 'white'
  },
  txtDecription: {
    color: common().COLOR_TEXT_ACTIVE,
    fontSize: common().FONT_SIZE_TITLE,
    fontWeight: common().FONT_WEGHT_HEADER,
    marginTop: 5,
  },
  viewBottom: {
    marginBottom: 22
  },
  viewLoading: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: 11000,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.2)'
  },

  header: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'transparent',
    marginBottom: 18,
  },
  viewArrow: {
    // marginRight: 10,
  },
  iconArrow: {
  },
  viewHeaderText: {
    flex: 1,
    marginRight: 10
  },
  headerText: {
    fontSize: 16,
    fontWeight: '500',
  },
  content: {
    backgroundColor: '#fff',
  },
});

export default UpdateProfile;

