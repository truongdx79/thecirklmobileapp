
import React, { PureComponent } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Image, ScrollView, Dimensions, Platform, ImageBackground, TouchableWithoutFeedback, Keyboard } from 'react-native';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import langs from '../../languages/profile';
import { CustomNavbar, Button, InputLabel, KeyBoardScroll } from '../../components';
import { common } from '../../config/common';
import { imgs } from '../../config/imgs';
import langCommon from '../../languages/common';
import { Actions } from 'react-native-router-flux';
import USER_DEFAULT from './UserDefault';

const SCREEN = Dimensions.get('window');

class Social extends PureComponent {
  constructor(props) {
    super(props);
    let user = USER_DEFAULT;
    if (props.userTemp) {
      user = props.userTemp || USER_DEFAULT;
    }

    console.log(`Social user => ${JSON.stringify(user)}`);

    this.state = {
      user,
    }
  }

  render() {
    let { user } = this.state;

    let linkedAccount = this.getValueByType('Linkedin');
    if (!linkedAccount || linkedAccount === '') {
      linkedAccount = user.emailAddress;
    }
    return (
      <View style={styles.container}>
        <CustomNavbar
          title={'Social'}
        // rightIcon
        // rightImage={imgs().filter}
        // onRight={this.onFilter}
        />
        <KeyBoardScroll scrollEnabled={true}>
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <View>
              <View style={styles.viewMiddle}>
                <InputLabel
                  style={styles.input}
                  placeholder={''}
                  title='Linkedin'
                  leftIcon
                  leftIconName={'linkedin'}
                  // rightIcon
                  value={linkedAccount}
                  onChangeText={this.onChangeLinkedin}
                  textInputRef="linkedin"
                  ref="linkedin"
                  onSubmitEditing={() => this.focusNextField("twitter")}
                />

                <InputLabel
                  style={styles.input}
                  placeholder={''}
                  title='Twitter'
                  leftIcon
                  leftIconName={'twitter'}
                  // rightIcon
                  value={this.getValueByType('Twitter')}
                  onChangeText={this.onChangeTwitter}
                  textInputRef="twitter"
                  ref="twitter"
                  onSubmitEditing={() => this.focusNextField("whatsapp")}
                />

                <InputLabel
                  style={styles.input}
                  placeholder={''}
                  title='WhatsApp'
                  leftIcon
                  leftIconName={'whatsapp'}
                  // rightIcon
                  value={this.getValueByType('WhatsApp')}
                  onChangeText={this.onChangeWhatApp}
                  textInputRef="whatsapp"
                  ref="whatsapp"
                  onSubmitEditing={() => this.focusNextField("wechat")}
                />

                <InputLabel
                  style={styles.input}
                  placeholder={''}
                  title='WeChat'
                  leftIcon
                  leftIconName={'weixin'}
                  // rightIcon
                  value={this.getValueByType('WeChat')}
                  onChangeText={this.onChangeWeChat}
                  textInputRef="wechat"
                  ref="wechat"
                  onSubmitEditing={() => this.focusNextField("instagram")}
                />

                <InputLabel
                  style={styles.input}
                  placeholder={''}
                  title='Instagram'
                  leftIcon
                  leftIconName={'instagram'}
                  // rightIcon
                  value={this.getValueByType('Instagram')}
                  onChangeText={this.onChangeInstagram}
                  textInputRef="instagram"
                  ref="instagram"
                  onSubmitEditing={() => this.focusNextField("facebook")}
                />

                <InputLabel
                  style={styles.input}
                  placeholder={''}
                  title='Facebook'
                  leftIcon
                  leftIconName={'facebook-f'}
                  // rightIcon
                  value={this.getValueByType('Facebook')}
                  onChangeText={this.onChangeFacebook}
                  textInputRef="facebook"
                  ref="facebook"
                  returnKeyType='done'
                />
              </View>

              <View style={styles.viewFooter}>
                <View style={styles.viewButton}>
                  <Button
                    title={"Back"}
                    style={styles.buttonBack}
                    onPress={this.handleBack}
                    rounded
                    backgroundColor={'white'}
                    color={'rgba(0, 0, 0, 0.7)'}
                    titleStyle={{ fontSize: common().FONT_SIZE_TITLE, fontWeight: common().FONT_WEIGHT_HEADER }}
                  />
                </View>
                <View style={styles.viewButton}>
                  <Button
                    title={"Next"}
                    style={styles.button}
                    onPress={this.handleNext}
                    rounded
                    backgroundColor={'#FFDE03'}
                    color={'rgba(0, 0, 0, 0.7)'}
                    titleStyle={{ fontSize: common().FONT_SIZE_TITLE, fontWeight: common().FONT_WEIGHT_HEADER }}
                  />
                </View>
              </View>

              <View style={style = styles.viewBottom} />
            </View>

          </TouchableWithoutFeedback>
        </KeyBoardScroll>

      </View>
    );
  }

  focusNextField = (nextField) => {
    this.refs[nextField].focus();
  };

  getValueByType = (type) => {
    let socials = this.state.user.socials;
    for (let item of socials) {
      if (item.type === type) {
        return item.name;
      }
    }
    return null;
  }

  onChangeLinkedin = (text) => {
    let socials = this.state.user.socials;
    socials = socials.filter((item, i) => {
      return item.type !== "Linkedin";
    });

    const user = {
      ...this.state.user,
      socials: [...socials, {
        "type": "Linkedin",
        "name": text
      }]
    };

    this.props.saveUserTemp({ user: user });

    this.setState({
      user: user
    });
  }

  onChangeTwitter = (text) => {
    let socials = this.state.user.socials;
    socials = socials.filter((item, i) => {
      return item.type !== "Twitter";
    });

    const user = {
      ...this.state.user,
      socials: [...socials, {
        "type": "Twitter",
        "name": text
      }]
    };

    this.props.saveUserTemp({ user: user });

    this.setState({
      user: user
    });
  }

  onChangeWhatApp = (text) => {
    let socials = this.state.user.socials;
    socials = socials.filter((item, i) => {
      return item.type !== "WhatsApp";
    });

    const user = {
      ...this.state.user,
      socials: [...socials, {
        "type": "WhatsApp",
        "name": text
      }]
    };

    this.props.saveUserTemp({ user: user });

    this.setState({
      user: user
    });
  }

  onChangeWeChat = (text) => {
    let socials = this.state.user.socials;
    socials = socials.filter((item, i) => {
      return item.type !== "Wechat";
    });

    const user = {
      ...this.state.user,
      socials: [...socials, {
        "type": "Wechat",
        "name": text
      }]
    };

    this.props.saveUserTemp({ user: user });

    this.setState({
      user: user
    });
  }

  onChangeInstagram = (text) => {
    let socials = this.state.user.socials;
    socials = socials.filter((item, i) => {
      return item.type !== "Instagram";
    });

    const user = {
      ...this.state.user,
      socials: [...socials, {
        "type": "Instagram",
        "name": text
      }]
    };

    this.props.saveUserTemp({ user: user });

    this.setState({
      user: user
    });
  }

  onChangeFacebook = (text) => {
    let socials = this.state.user.socials;
    socials = socials.filter((item, i) => {
      return item.type !== "Facebook";
    });

    const user = {
      ...this.state.user,
      socials: [...socials, {
        "type": "Facebook",
        "name": text
      }]
    };

    this.props.saveUserTemp({ user: user });

    this.setState({
      user: user
    });
  }

  handleBack = () => {
    Keyboard.dismiss();
    Actions.pop();
  }

  handleNext = () => {
    Keyboard.dismiss();
    // let socials = this.state.user.socials;
    // for (let item of socials) {
    //   if (!item.name) {
    //     window.customAlert.alert({
    //       title: langCommon.notification,
    //       message: 'Please fill all field.',
    //       leftButton: { text: langCommon.ok }
    //     });
    //     return;
    //   }
    // }

    Actions.updateProfile();
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  viewHeader: {
    padding: 16
  },
  viewMiddle: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 16
  },
  viewFooter: {
    // position: 'absolute',
    // bottom: 22,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 16,
    marginTop: 16
  },
  viewButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonBack: {
    width: (SCREEN.width - 50) / 2,
    borderWidth: 1,
    borderColor: '#707070',
  },
  button: {
    width: (SCREEN.width - 50) / 2,
  },
  input: {
    marginBottom: 20
  },
  txtInfo: {
    color: common().COLOR_TEXT_ACTIVE,
    fontSize: common().FONT_SIZE_HEADER,
  },
  viewBottom: {
    marginBottom: 20
  },
});

export default Social;

