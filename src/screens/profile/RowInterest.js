import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Image,
  Dimensions,
  ImageBackground,
  ActivityIndicator
} from 'react-native';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import { common } from '../../config/common';
import { imgs } from '../../config/imgs';
import { Actions } from 'react-native-router-flux';

const { width, height } = Dimensions.get('window');
const SPACE = 15;
const ITEM_DEFAULT = {
  "_createDate": "2018-10-03T08:12:54.578Z",
  "_id": "5bb49323c4b07f12f86ead4a",
  "name": "Music",
  "icon": "19.png"
}

class RowInterest extends Component {
  _onPress(item) {
    this.props.onPressItem && this.props.onPressItem(item);
  }

  render() {
    let { item, hobbies } = this.props;

    // item = ITEM_DEFAULT;

    return (
      <TouchableOpacity style={styles.root} activeOpacity={0.8} onPress={() => this._onPress(item)}>
        <View style={[styles.container, this.checkHobbyId(item._id) ? {backgroundColor: '#FFDE03'} : {backgroundColor: '#B8E6EB'}]} 
          >
          <View style={styles.viewBody}>
            <Image
              style={styles.imageContent}
              // source={{ uri: 'http://radiotray.sourceforge.net/radio.png' }}
              source={{ uri: `http://168.63.251.166:8685/file/icons/${item.icon}` }}
              resizeMode='contain'
            />
          </View>
        </View>
        <View style={styles.viewTitle}>
          <Text numberOfLines={1} style={styles.txtTitle}>
            {item ? item.name : ''}
          </Text>
        </View>

      </TouchableOpacity >
    )
  }

  checkHobbyId = (id) => {
    let hobbies = this.props.hobbies;
    for (let hobby of hobbies) {
      if (hobby._id === id) {
        return true;
      }
    }

    return false;
  }

}

const styles = StyleSheet.create({
  root: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 20,
  },
  container: {
    height: (width - (SPACE * 5)) / 4,
    width: (width - (SPACE * 5)) / 4,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: "#B8E6EB",
    borderRadius: 10,
    shadowColor: '#000000',
    shadowOffset: {
      width: 1,
      height: 1
    },
    shadowRadius: 5,
    shadowOpacity: 0.1,
    overflow: 'hidden',
  },
  viewBody: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageContent: {
    width: 52,
    height: 52,
    // tintColor: 'black'
  },
  viewTitle: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
  },
  txtTitle: {
    fontSize: common().FONT_SIZE_TITLE,
    fontWeight: common().FONT_WEGHT_HEADER,
    color: 'white'
  },
})

export default RowInterest;
