
import React, { PureComponent } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Image, ScrollView, Dimensions, Platform, ImageBackground, TouchableWithoutFeedback, Keyboard } from 'react-native';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import langs from '../../languages/profile';
import { CustomNavbar, Button, InputLabel, InputLabel2, Avatar, KeyBoardScroll } from '../../components';
import { common } from '../../config/common';
import { imgs } from '../../config/imgs';
import { Actions } from 'react-native-router-flux';
import langCommon from '../../languages/common';
import { isValidEmail, isValidPhonenumber } from '../../common/validate';
import USER_DEFAULT from './UserDefault';

const SCREEN = Dimensions.get('window');

class Personal extends PureComponent {
  constructor(props) {
    super(props);
    let user = USER_DEFAULT;
    if (props.userTemp) {
      user = props.userTemp || USER_DEFAULT;
    }

    console.log(`Personal user => ${JSON.stringify(user)}`);

    this.state = {
      user,
    }
  }

  render() {
    let { user } = this.state;

    return (
      <KeyBoardScroll>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <View style={styles.container}>
            <ImageBackground
              source={imgs().bgProfile}
              resizeMode='cover'
              style={styles.viewHeader}
            >
              <View style={{ flex: 1 }} >
                <View style={styles.viewTitle}>
                  <Text numberOfLines={1} style={styles.txtHeader}>
                    Personal
                </Text>
                </View>
                <View style={styles.separator} />
                <View style={styles.viewAvatar}>
                  <Avatar
                    width={87}
                    // imageSource={user.avatarUrl}
                    imageSource={user.avatarUrl}
                  />
                  <Text numberOfLines={1} style={[styles.txtHeader, { fontSize: common().FONT_SIZE_HUGE, marginTop: 16 }]}>
                    {`${user.lastName} ${user.firstName} `}
                  </Text>
                  <Text numberOfLines={1} style={[styles.txtTitle, { fontSize: common().FONT_SIZE_SMALL, marginTop: 5 }]}>
                    {user.headline}
                  </Text>
                </View>
              </View>
            </ImageBackground>

            <View style={styles.viewMiddle}>
              <InputLabel
                style={styles.input}
                placeholder={''}
                title='Current position'
                // rightIcon
                value={user.currentPosition}
                onChangeText={this.onChangeCurrentPosition}
                textInputRef="current_position"
                ref="current_position"
                onSubmitEditing={() => this.focusNextField("company")}
              />

              <InputLabel
                style={styles.input}
                placeholder={''}
                title='Company name'
                // rightIcon
                value={user.company}
                onChangeText={this.onChangeCompanyName}
                textInputRef="company"
                ref="company"
                onSubmitEditing={() => this.focusNextField("location")}
              />

              <InputLabel
                style={styles.input}
                placeholder={''}
                title='Location'
                // rightIcon
                value={user.location}
                onChangeText={this.onChangeLocation}
                textInputRef="location"
                ref="location"
                onSubmitEditing={() => this.focusNextField("email")}
              />

              <InputLabel
                style={styles.input}
                placeholder={''}
                title='Email'
                // rightIcon
                value={user.emailAddress}
                onChangeText={this.onChangeEmail}
                textInputRef="email"
                ref="email"
                onSubmitEditing={() => this.focusNextField("phone")}
              />

              <InputLabel
                style={styles.input}
                placeholder={''}
                title='Phone'
                keyboardType='phone-pad'
                // rightIcon
                value={user.phone}
                onChangeText={this.onChangePhone}
                textInputRef="phone"
                ref="phone"
                onSubmitEditing={() => this.focusNextField("introduction")}
              />

              <InputLabel2
                style={styles.input}
                placeholder={''}
                title='Introduction'
                // rightIcon
                value={user.introduction}
                onChangeText={this.onChangeIntroduction}
                textInputRef="introduction"
                ref="introduction"
                returnKeyType='done'
              />

              {/* <View style={styles.viewInfo}>
              <View style={{ width: SCREEN.width - 32 }}>
                <Text numberOfLines={1} style={styles.txtSubTitle}>
                  Introduction
                </Text>
                <Text style={styles.txtDecription}>
                  {user.introduction}
                </Text>
              </View>

              <View style={styles.separatorGrey} />
            </View> */}
            </View>

            <View style={styles.viewFooter}>
              <View style={styles.viewButton}>
                <Button
                  title={"Back"}
                  style={styles.buttonBack}
                  onPress={this.handleBack}
                  rounded
                  backgroundColor={'white'}
                  color={'rgba(0, 0, 0, 0.7)'}
                  titleStyle={{ fontSize: common().FONT_SIZE_TITLE, fontWeight: common().FONT_WEIGHT_HEADER }}
                />
              </View>
              <View style={styles.viewButton}>
                <Button
                  title={"Next"}
                  style={styles.button}
                  onPress={this.handleNext}
                  rounded
                  backgroundColor={'#FFDE03'}
                  color={'rgba(0, 0, 0, 0.7)'}
                  titleStyle={{ fontSize: common().FONT_SIZE_TITLE, fontWeight: common().FONT_WEIGHT_HEADER }}
                />
              </View>
            </View>

            <View style={style = styles.viewBottom} />
          </View>
        </TouchableWithoutFeedback>
      </KeyBoardScroll>


    );
  }

  focusNextField = (nextField) => {
    this.refs[nextField].focus();
  };

  onChangeCurrentPosition = (text) => {
    const user = {
      ...this.state.user,
      currentPosition: text
    }

    this.props.saveUserTemp({user: user});

    this.setState({
      user: user
    });
  }

  onChangeCompanyName = (text) => {
    const user = {
      ...this.state.user,
      company: text
    }

    this.props.saveUserTemp({user: user});

    this.setState({
      user: user
    });
  }

  onChangeLocation = (text) => {
    const user = {
      ...this.state.user,
      location: text
    }

    this.props.saveUserTemp({user: user});

    this.setState({
      user: user
    });
  }

  onChangeEmail = (text) => {
    const user = {
      ...this.state.user,
      emailAddress: text
    }

    this.props.saveUserTemp({user: user});

    this.setState({
      user: user
    });
  }

  onChangePhone = (text) => {
    const user = {
      ...this.state.user,
      phone: text
    }

    this.props.saveUserTemp({user: user});

    this.setState({
      user: user
    });
  }

  onChangeIntroduction = (text) => {
    const user = {
      ...this.state.user,
      introduction: text
    }

    this.props.saveUserTemp({user: user});

    this.setState({
      user: user
    });
  }

  handleBack = () => {
    Keyboard.dismiss();
    Actions.pop();
  }

  handleNext = () => {
    Keyboard.dismiss();
    let { user } = this.state;

    // if (!user.currentPosition) {
    //   window.customAlert.alert({
    //     title: langCommon.notification,
    //     message: 'Please fill current position.',
    //     leftButton: { text: langCommon.ok }
    //   });
    //   return;
    // }

    // if (!user.company) {
    //   window.customAlert.alert({
    //     title: langCommon.notification,
    //     message: 'Please fill company.',
    //     leftButton: { text: langCommon.ok }
    //   });
    //   return;
    // }

    // if (!user.location) {
    //   window.customAlert.alert({
    //     title: langCommon.notification,
    //     message: 'Please fill location.',
    //     leftButton: { text: langCommon.ok }
    //   });
    //   return;
    // }

    if (user.emailAddress && !isValidEmail(user.emailAddress)) {
      window.customAlert.alert({
        title: langCommon.notification,
        message: 'Please fill a correct email.',
        leftButton: { text: langCommon.ok }
      });
      return;
    }

    if (user.phone && !isValidPhonenumber(user.phone)) {
      window.customAlert.alert({
        title: langCommon.notification,
        message: 'Please fill a correct phone.',
        leftButton: { text: langCommon.ok }
      });
      return;
    }

    // if (!user.introduction) {
    //   window.customAlert.alert({
    //     title: langCommon.notification,
    //     message: 'Please fill introduction.',
    //     leftButton: { text: langCommon.ok }
    //   });
    //   return;
    // }

    Actions.education();
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  viewHeader: {
    width: SCREEN.width,
    height: 290,
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewMiddle: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 16,
  },
  viewFooter: {
    // position: 'absolute',
    // bottom: 22,
    marginTop: 16,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 16,
  },
  viewButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonBack: {
    width: (SCREEN.width - 50) / 2,
    borderWidth: 1,
    borderColor: '#707070',
  },
  button: {
    width: (SCREEN.width - 50) / 2,
  },
  input: {
    marginBottom: 15
  },
  txtInfo: {
    color: common().COLOR_TEXT_ACTIVE,
    fontSize: common().FONT_SIZE_HEADER,
  },
  txtHeader: {
    color: common().COLOR_TEXT_NORMAL,
    fontSize: common().FONT_SIZE_HEADER,
    fontWeight: common().FONT_WEIGHT_HEADER
  },
  txtTitle: {
    color: common().COLOR_TEXT_NORMAL,
    fontSize: common().FONT_SIZE_TITLE
  },
  viewAvatar: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewTitle: {
    width: SCREEN.width,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: Platform.OS === 'ios' ? 40 : 16,
    marginBottom: 16,
  },
  separator: {
    height: StyleSheet.hairlineWidth,
    backgroundColor: common().COLOR_SEPARATOR_WHITE
  },
  separatorGrey: {
    height: StyleSheet.hairlineWidth,
    backgroundColor: 'rgba(112, 112, 112, 1)',
    marginTop: 7.5
  },
  viewInfo: {
    marginBottom: 16,
    marginLeft: 16,
    marginRight: 16,
  },
  txtSubTitle: {
    color: common().COLOR_TEXT_TITLE,
    fontSize: common().FONT_SIZE_SMALL
  },
  txtDecription: {
    color: common().COLOR_TEXT_ACTIVE,
    fontWeight: common().FONT_WEIGHT_HEADER,
    fontSize: 15,
    marginTop: 5,
  },
  viewBottom: {
    paddingBottom: 20
  },
});

export default Personal;

