export default USER_DEFAULT = {
    "_linkedinId": "",
    "firstName": "",
    "lastName": "",
    "headline": "",
    "avatarUrl": "",
    "company": "",
    "currentPosition": "",
    "location": "",
    "emailAddress": "",
    "phone": "",
    "introduction": "",
    "interests": [],
    "educations": [
        {
            "school": "",
            "degree": "",
            "fieldOfStudy": "",
            "completed": ""
        }
    ],
    "socials": [
        {
            "type": "Linkedin",
            "name": ""
        },
        {
            "type": "Twitter",
            "name": ""
        },
        {
            "type": "Facebook",
            "name": ""
        },
        {
            "type": "WhatsApp",
            "name": ""
        },
        {
            "type": "Wechat",
            "name": ""
        },
        {
            "type": "Instagram",
            "name": ""
        }
    ]
};