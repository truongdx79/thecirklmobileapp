
import React, { PureComponent } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Image, ScrollView, Dimensions, Platform, ImageBackground, TouchableWithoutFeedback, Keyboard } from 'react-native';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import langs from '../../languages/profile';
import { CustomNavbar2, Button, InputLabel, KeyBoardScroll } from '../../components';
import { common } from '../../config/common';
import { imgs } from '../../config/imgs';
import { Actions } from 'react-native-router-flux';
import langCommon from '../../languages/common';
import RowEducation from './RowEducation';
import USER_DEFAULT from './UserDefault';

const SCREEN = Dimensions.get('window');

class Education extends PureComponent {
  constructor(props) {
    super(props);
    let user = USER_DEFAULT;
    if (props.userTemp) {
      user = props.userTemp || USER_DEFAULT;
    }

    console.log(`Education user => ${JSON.stringify(user)}`);

    this.state = {
      user,
      indexs: [],
      resetCheck: false
    }
  }

  render() {
    let { educations } = this.state.user;
    let { indexs } = this.state;

    console.log(`educations => ${JSON.stringify(educations)}`);

    return (
      <View style={styles.container}>
        <CustomNavbar2
          title={'Education'}
          rightIcon
          rightIconName={'add'}
          onRight={this.onAddEducation}
          rightIcon2={indexs && indexs.length > 0 ? true : false}
          rightIconName2={'delete'}
          onRight2={this.onDeleteEducation}
        />
        {/* <View style={styles.viewHeader}>
          <Text style={styles.txtInfo}>
            Information about your education
          </Text>
        </View> */}
        <KeyBoardScroll>
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <View>
              {
                educations && educations.map((item, index) => {
                  return <RowEducation resetCheck={this.state.resetCheck} deleteCheck={() => this.setState({ resetCheck: false })} education={item} index={index} onUpdateItem={this.onUpdateItem} onSelectItem={this.onSelectItem} key={`row-${index}`} />
                })
              }

              <View style={styles.viewFooter}>
                <View style={styles.viewButton}>
                  <Button
                    title={"Back"}
                    style={styles.buttonBack}
                    onPress={this.handleBack}
                    rounded
                    backgroundColor={'white'}
                    color={'rgba(0, 0, 0, 0.7)'}
                    titleStyle={{ fontSize: common().FONT_SIZE_TITLE, fontWeight: common().FONT_WEIGHT_HEADER }}
                  />
                </View>
                <View style={styles.viewButton}>
                  <Button
                    title={"Next"}
                    style={styles.button}
                    onPress={this.handleNext}
                    rounded
                    backgroundColor={'#FFDE03'}
                    color={'rgba(0, 0, 0, 0.7)'}
                    titleStyle={{ fontSize: common().FONT_SIZE_TITLE, fontWeight: common().FONT_WEIGHT_HEADER }}
                  />
                </View>
              </View>

              <View style={style = styles.viewBottom} />
            </View>

          </TouchableWithoutFeedback>
        </KeyBoardScroll>

      </View >
    );
  }

  focusNextField = (nextField) => {
    this.refs[nextField].focus();
  };

  onAddEducation = () => {
    let education = {
      school: "",
      degree: "",
      fieldOfStudy: "",
      completed: ""
    };

    const user = {
      ...this.state.user, educations: [...this.state.user.educations, education]
    };

    this.props.saveUserTemp({ user: user });

    this.setState({
      user: user,
    });
  }

  onDeleteEducation = () => {
    let educations = this.state.user.educations;
    let indexs = this.state.indexs;

    let filterItems = educations.filter(item => {
      return !indexs.includes(educations.indexOf(item));
    });

    const user = {
      ...this.state.user, educations: [...filterItems]
    };

    this.props.saveUserTemp({ user: user });

    this.setState({
      indexs: [],
      user: user,
      resetCheck: true
    });
  }

  onSelectItem = (checked, index) => {
    let indexs = this.state.indexs;
    if (!checked) {
      this.removeItem(indexs, index);
    } else {
      indexs.push(index);
    }

    this.setState({
      indexs: [...indexs],
    });
  }

  removeItem = (arr, item) => {
    for (var i = arr.length; i--;) {
      if (arr[i] === item) {
        arr.splice(i, 1);
      }
    }
  }

  onDeleteItem = (index) => {
    let educations = this.state.user.educations;
    educations.splice(index, 1);

    const user = {
      ...this.state.user, educations: [...educations]
    };

    this.props.saveUserTemp({ user: user });

    this.setState({
      user: user,
    });
  }

  onUpdateItem = (item) => {
    let educations = this.state.user.educations;
    let index = item.index;
    educations[index] = item.item;

    const user = { 
      ...this.state.user, educations: [...educations] 
    };

    this.props.saveUserTemp({ user: user });

    this.setState({
      user: user,
    });
  }

  handleBack = () => {
    Keyboard.dismiss();
    Actions.pop();
  }

  handleNext = () => {
    Keyboard.dismiss();
    // let educations = this.state.user.educations;
    // if (!educations || !educations.length) {
    //   window.customAlert.alert({
    //     title: langCommon.notification,
    //     message: 'Please create an education.',
    //     leftButton: { text: langCommon.ok }
    //   });
    //   return;
    // }

    // for (let item of educations) {
    //   if (!item.school || !item.degree || !item.fieldOfStudy || !item.completed) {
    //     window.customAlert.alert({
    //       title: langCommon.notification,
    //       message: 'Please fill all field.',
    //       leftButton: { text: langCommon.ok }
    //     });
    //     return;
    //   }
    // }

    Actions.social();
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  viewHeader: {
    padding: 16,
  },
  viewMiddle: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  viewFooter: {
    // position: 'absolute',
    // bottom: 22,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 16,
    marginTop: 20
  },
  viewButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonBack: {
    width: (SCREEN.width - 50) / 2,
    borderWidth: 1,
    borderColor: '#707070',
  },
  button: {
    width: (SCREEN.width - 50) / 2,
  },
  input: {
    marginBottom: 15
  },
  txtInfo: {
    color: common().COLOR_TEXT_ACTIVE,
    fontSize: common().FONT_SIZE_HEADER,
  },
  viewBottom: {
    marginBottom: 20
  },
});

export default Education;

