/**
* Created by bav on Sat Jul 14 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import { StyleSheet, View, Image, Text, ActivityIndicator } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { imgs } from '../../config/imgs';
import { common } from '../../config/common';
import { Button, ButtonLoginLinkedin, Input, InputPassword, Loading, Alert } from '../../components';
import langs from '../../languages/login';
import langCommon from '../../languages/common';
import LinkedInModal from 'react-native-linkedin';

const CLIENT_ID = '819kjga13hrtp9';
const CLIENT_SECRET = 'N4sdEZIvM6CLa9OY';

class Login extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      access_token: undefined,
      expires_in: undefined,
      refreshing: false,
      needUpdateProfile: false,
    }
  }

  componentDidMount() {
    let linkedinId = this.props.linkedinId;
    console.log(`linkedinId => ${linkedinId}`);
    if (linkedinId) {
      this.props.loginAction({ linkedinId: linkedinId, callbackLogin: this.handleBack });
    }
  }

  render() {
    const { refreshing, emailAddress } = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.viewLogo}>
          <Image
            style={styles.logo}
            source={imgs().logo}
            resizeMode='contain'
          />
        </View>
        <View style={styles.viewMiddle}>
          <View style={styles.viewWelcome}>
            <Text style={styles.txtWelcome}>
              {langs.txtWelcome}
            </Text>
            <Text style={styles.txtContent}>
              {langs.txtContent}
            </Text>
          </View>
          <View style={styles.viewButton}>
            <LinkedInModal
              ref={ref => {
                this.modal = ref
              }}
              clientID={CLIENT_ID}
              clientSecret={CLIENT_SECRET}
              redirectUri="http://iotech.vn"
              onSuccess={data => this.getUser(data)}
              onError={err => this.onError(err)}
              onClose={err => this.onClose}
              onOpen={err => this.onOpen}
              onSignIn={err => this.onSignIn}
              renderButton={this.renderButtonLogin}
            />
          </View>

          <Text style={styles.textCopyright}>
            Copyright © 2018 TheCirkl
          </Text>
        </View>
        <Alert
          ref={ref => this.alert = ref}
          leftButton={{ text: 'Huy' }}
          rightButton={{ text: 'Delete' }}
          title='Hello'
          message='Cong hoa xa hoi chu nghia Viet Nam'
        />
      </View>
    );
  }

  focusNextField = (nextField) => {
    this.refs[nextField].focus();
  };

  renderButtonLogin = () => {
    return <ButtonLoginLinkedin
      title={langs.login}
      style={styles.button}
      // full={true}
      onPress={this.handleLogin}
      rounded
    />;
  }

  onChangeUsername = (text) => {
    this.setState({
      username: text
    })
  }

  onChangePassword = (text) => {
    this.setState({
      password: text
    })
  }

  handleLogin = () => {
    // const { username, password } = this.state;
    // console.log('handleLogin', username, password)
    // this.alert.open()
    // Actions.loginLinkedin();
    this.modal.open();
  }

  async getUser({ access_token }) {
    console.log(`loginlinkedin success`);

    window.customLoading.show();

    this.setState({ refreshing: true })
    const baseApi = 'https://api.linkedin.com/v1/people/'
    const qs = { format: 'json' }

    //https://developer.linkedin.com/docs/fields/basic-profile
    const params = [
      'id',
      'first-name',
      'last-name',
      'picture-url',
      'location',
      'summary',
      'positions',
      'picture-urls::(original)',
      'headline',
      'email-address',
    ]

    const response = await fetch(`${baseApi}~:(${params.join(',')})?format=json`, {
      method: 'GET',
      headers: {
        Authorization: 'Bearer ' + access_token,
      },
    })
    const payload = await response.json();
    console.log(`payload => ${JSON.stringify(payload)}`);
    this.setState({ ...payload, refreshing: false });

    // window.customLoading.hide();

    this.props.saveUserLinkedin({ user: payload });

    this.props.loginAction({ linkedinId: payload.id, callbackLogin: this.handleBack });
  }

  handleBack = (isSuccess) => {
    // LayoutAnimation.configureNext(CustomLayoutSpring);
    this.setState({
      needUpdateProfile: !this.state.needUpdateProfile
    });

    if (isSuccess) {
      Actions.home();
    } else {
      Actions.interest();
    }

  }

  onError = (err) => {
    window.customAlert.alert({
      title: langCommon.notification,
      message: JSON.stringify(err),
      leftButton: { text: langCommon.ok }
    });
    console.log(`login linkedin error => ${JSON.stringify(err)}`);
  }

  onClose = () => {
    console.log(`login linkedin onClose`);
  }

  onOpen = () => {
    console.log(`login linkedin onOpen`);
  }

  onSignIn = () => {
    console.log(`login linkedin onSignIn`);
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewLogo: {
    flex: 2,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  viewMiddle: {
    flex: 5,
    alignItems: 'center'
  },
  viewFooter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  logo: {

  },
  viewWelcome: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 17
  },
  viewButton: {
    flex: 1
  },
  button: {
    marginTop: 12,
    marginBottom: 8
  },
  input: {
    marginBottom: 22
  },
  txtLabel: {
    fontSize: common().FONT_SIZE_SMALL
  },
  txtWelcome: {
    fontSize: common().FONT_SIZE_HUGE,
    fontWeight: common().FONT_WEIGHT_HEADER,
    color: common().COLOR_TEXT_NORMAL
  },
  txtContent: {
    fontSize: common().FONT_SIZE_CONTENT,
    fontWeight: common().FONT_WEIGHT_CONTENT,
    color: common().COLOR_TEXT_NORMAL,
    textAlign: 'center',
    marginTop: 13
  },
  textCopyright: {
    fontSize: common().FONT_SIZE_CONTENT,
    color: common().COLOR_TEXT_NORMAL,
    bottom: 25
  },
  linkedInContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Login;
