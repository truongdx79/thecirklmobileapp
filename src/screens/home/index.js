/**
* Created by bav on Sat Jul 14 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import { StyleSheet, View, Text, FlatList, TouchableOpacity, Dimensions, ImageBackground, SafeAreaView, LayoutAnimation, RefreshControl } from 'react-native';
import { CustomNavbar, Card, ButtonIcon, ButtonLabelBorder, ButtonOverlay, Row, SingleTabView } from '../../components';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import ActionButton from 'react-native-action-button';
import langs from '../../languages/home';
import RowHome from './RowHome';
import { imgs } from '../../config/imgs';
import { common } from '../../config/common';
import { CustomLayoutSpring } from '../../common/animation';

const SCREEN = Dimensions.get('window');
// const HOMES = [];
const HOMES = [{
  "createDate": "2018-08-09T07:14:27.352Z",
  "title": "Baggins",
  "location": "2nd Generation Adventurer",
  "imageUrl": "https://lh6.googleusercontent.com/-PYvLVdvXywk/URqunwd8hfI/AAAAAAAAAbs/qiMwgkFvf6I/s1024/Fitzgerald%252520Streaks.jpg",
  "userName": "CEO-TheCirkl",
  "description": "Lorem Ipsum is simply dummy text of the printing ",
},
{
  "createDate": "2018-08-09T07:14:27.352Z",
  "title": "David",
  "location": "Nan lian ...",
  "imageUrl": "https://lh6.googleusercontent.com/-PYvLVdvXywk/URqunwd8hfI/AAAAAAAAAbs/qiMwgkFvf6I/s1024/Fitzgerald%252520Streaks.jpg",
  "userName": "CTO",
  "description": "2 Lorem Ipsum is simply dummy text of the printing ",
}];

class ListHome extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      edit: false,
      refreshing: false
    }
  }

  componentDidMount() {
    console.log(`user => ${JSON.stringify(this.props.user)}`);
  }

  renderItem = ({ item, index }) => {
    return (
      <RowHome
      // language={this.props.language}
      // post={item}
      // onLikePost={this._onLikePost}
      // userInfo={userInfo}
      // deletePost={this.props.deletePost}
      // group={this.props.group}
      // votePoll={this.props.votePoll}
      // userID={userID}
      onPress={this.onPressItem}
      onPressJoinMe={this.onPressJoinMe}
      />
    )
  }

  onPressItem = () => {
    // Actions.eventDetail();
  }

  onPressJoinMe = () => {
    
  }

  renderEmptyListHome = () => {
    return (
      <View style={styles.viewEmptyListHome}>
        <Text style={styles.txtHeader}>
          No items!
          </Text>
        {/* <Text style={styles.txtContent}>
          Empty
          </Text> */}
      </View>
    )
  }

  renderAddIcon = () => {
    return (
      <Icon name="add" size={22} color={'black'} />
    )
  }

  render() {
    const { edit, refreshing } = this.state;
    let { listHome } = this.props;

    listHome = HOMES;

    return (
      <View style={styles.container}>
        <CustomNavbar
          title={'Home'}
          rightIcon
          rightImage={imgs().filter}
          onRight={this.onFilter}
        />
        {
          listHome.length > 0 ? (
            <FlatList
              data={listHome}
              keyExtractor={(item, index) => String(index)}
              renderItem={this.renderItem}
              extraData={this.state}
              contentContainerStyle={styles.containerList}
              refreshControl={
                <RefreshControl
                  refreshing={refreshing}
                  onRefresh={this.onRefresh}
                  tintColor={common().COLOR_INDICATOR}
                />
              }
            />
          ) : this.renderEmptyListHome()
        }

        <ActionButton
          buttonColor='#FFDE03'
          onPress={() => this.handleAdd}
          renderIcon={this.renderAddIcon}
          offsetX={17}
          offsetY={17}
        // style={{ position: 'absolute', right: 17, bottom: 17 }}
        />
      </View>
    );
  }

  onRefresh = () => {
    this.setState({
      refreshing: true
    })

    this.props.refreshListHome && this.props.refreshListHome({ onDoneRefresh: this.onDoneRefresh })
  }

  onDoneRefresh = () => {
    this.setState({
      refreshing: false
    })
  }

  onFilter = () => {
    console.log(`onFilter`);
  }

  handleAdd = () => {

  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  containerList: {
    flexGrow: 1,
    paddingTop: 10
  },
  containerStyleCard: {
    marginBottom: 25
  },
  card: {
    justifyContent: 'flex-end'
  },
  viewTitle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 10
  },
  txtName: {
    fontSize: common().FONT_SIZE_TITLE,
    fontWeight: common().FONT_WEIGHT_HEADER,
    color: common().COLOR_TEXT_NORMAL
  },
  viewRightTitle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  viewStatus: {
    width: 8,
    height: 8,
    borderRadius: 4,
    elevation: 2,
    shadowOffset: {
      width: 1,
      height: 1
    },
    shadowRadius: 2,
    shadowOpacity: 0.6
  },
  txtStatus: {
    fontSize: common().FONT_SIZE_CONTENT,
    fontWeight: common().FONT_WEIGHT_TITLE,
    color: common().COLOR_TEXT_NORMAL,
    marginLeft: 4
  },
  txtContent: {
    fontSize: common().FONT_SIZE_CONTENT,
    fontWeight: common().FONT_WEIGHT_TITLE,
    color: common().COLOR_TEXT_ACTIVE,
  },
  viewRightNavBar: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  viewEmptyListHome: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 16
  },
  txtHeader: {
    color: common().COLOR_TEXT_ACTIVE,
    fontSize: common().FONT_SIZE_HEADER,
    fontWeight: common().FONT_WEIGHT_HEADER,
    marginBottom: 10
  },
  buttonLearnAbout: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: 10,
    padding: 5,
  },
});

export default ListHome;
