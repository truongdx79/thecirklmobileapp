/**
* Created by nghinv on Tue Jul 24 2018
* Copyright (c) 2018 nghinv
*/

import React, { PureComponent } from 'react';
import { StyleSheet, View, Text, Dimensions, ImageBackground, Image } from 'react-native';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import langs from '../../languages/home';
import { imgs } from '../../config/imgs';
import { common } from '../../config/common';

const SCREEN = Dimensions.get('window');
const AVATAR_URLS = [
  'https://lh6.googleusercontent.com/-PYvLVdvXywk/URqunwd8hfI/AAAAAAAAAbs/qiMwgkFvf6I/s1024/Fitzgerald%252520Streaks.jpg',
  "https://lh6.googleusercontent.com/-GHeImuHqJBE/URqu_FKfVLI/AAAAAAAAAbs/axuEJeqam7Q/s1024/Sailing%252520Stones.jpg",
  "https://lh3.googleusercontent.com/-hBbYZjTOwGc/URqu_ycpIrI/AAAAAAAAAbs/nAdJUXnGJYE/s1024/Seahorse.jpg",
  "https://lh3.googleusercontent.com/-Iwi6-i6IexY/URqvAYZHsVI/AAAAAAAAAbs/5ETWl4qXsFE/s1024/Shinjuku%252520Street.jpg",
  "https://lh6.googleusercontent.com/-amhnySTM_MY/URqvAlb5KoI/AAAAAAAAAbs/pFCFgzlKsn0/s1024/Sierra%252520Heavens.jpg",
  "https://lh5.googleusercontent.com/-dJgjepFrYSo/URqvBVJZrAI/AAAAAAAAAbs/v-F5QWpYO6s/s1024/Sierra%252520Sunset.jpg",
  "https://lh4.googleusercontent.com/-Z4zGiC5nWdc/URqvBdEwivI/AAAAAAAAAbs/ZRZR1VJ84QA/s1024/Sin%252520Lights.jpg",
  "https://lh4.googleusercontent.com/-_0cYiWW8ccY/URqvBz3iM4I/AAAAAAAAAbs/9N_Wq8MhLTY/s1024/Starry%252520Lake.jpg",
]

class RowRelatedEvent extends PureComponent {
  render() {
    const { firstItem, lastItem } = this.props;

    let urls = AVATAR_URLS;
    let avatarUrls = [];

    if (urls && urls.length > 5) {
      for (let i = 0; i < 5; i++) {
        avatarUrls[i] = urls[i];
      }
    } else {
      avatarUrls = urls;
    }

    return (
      <View style={styles.container}>
        <View style={styles.body}>

          <View style={styles.viewCalendar}>
            <ImageBackground
              source={imgs().calendar0}
              resizeMode='stretch'
              style={styles.imageDate}
            >
              <Text numberOfLines={1} style={[styles.txtDate, { color: 'white', paddingTop: 2 }]}>
                12/4/18
              </Text>
            </ImageBackground>
            <ImageBackground
              source={imgs().calendar1}
              resizeMode='stretch'
              style={[styles.imageDate, { marginTop: -1 }]}
            >
              <Text numberOfLines={1} style={[styles.txtDate, { color: 'rgba(0, 0, 0, 0.7)' }]}>
                4 days
              </Text>
            </ImageBackground>
          </View>

          <View style={styles.viewTitle}>
            <Text numberOfLines={1} style={[styles.txtTitle]}>
              Friendly Spring Tournament
            </Text>
            <View style={styles.viewLeftHeader}>
              <Icon name={'location-on'} size={15} color={common().COLOR_TEXT_INACTIVE} />
              <Text numberOfLines={1} style={styles.txtPosition}>
                Nan Lian Garden, Hong Kong
              </Text>
            </View>

          </View>

          <View style={styles.viewRightButton}>
            <View style={styles.viewJoin}>
              {
                avatarUrls.map(url => {
                  if (urls.length > 5 && avatarUrls.indexOf(url) === avatarUrls.length - 1) {
                    return (
                      <View style={styles.viewFolow}>
                        {
                          <Text numberOfLines={1} style={styles.textFolow}>
                            {`+${urls.length - avatarUrls.length + 1}`}
                          </Text>
                        }
                      </View>
                    );
                  }
                  return (
                    <View style={styles.viewFolow}>
                      {
                        <Image source={{ uri: url }} resizeMode="cover" style={styles.avatarFolow} />
                      }
                    </View>
                  );
                })
              }
            </View>
          </View>
        </View>
        <View style={styles.viewSeparator} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: 85,
    backgroundColor: common().COLOR_SEPARATOR,
    alignItems: 'center'
  },
  body: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'white'
  },
  viewAvatar: {
    width: 40,
    height: 40,
    borderRadius: 20,
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatarBody: {
    flex: 1,
    width: SCREEN.width - 32,
    height: 190,
    borderRadius: 6
  },
  avatar: {
    width: 40,
    height: 40,
    borderRadius: 20
  },
  viewCalendar: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewTitle: {
    flex: 1,
    justifyContent: 'center',
    marginLeft: 16,
  },
  viewNameUser: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 15
  },
  imageDate: {
    width: 54,
    height: 24,
    padding: 5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  txtDate: {
    color: common().COLOR_TEXT_ACTIVE,
    fontSize: common().FONT_SIZE_SMALL_10,
    fontWeight: common().FONT_WEIGHT_TITLE,
  },
  txtTitle: {
    color: common().COLOR_TEXT_ACTIVE,
    fontSize: common().FONT_SIZE_CONTENT,
    fontWeight: common().FONT_WEIGHT_HEADER,
  },
  txtNameUser: {
    color: common().COLOR_TEXT_ACTIVE,
    fontSize: common().FONT_SIZE_CONTENT,
    fontWeight: common().FONT_WEIGHT_HEADER,
  },
  viewLeftHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5
  },
  txtPosition: {
    color: common().COLOR_TEXT_INACTIVE,
    fontSize: common().FONT_SIZE_SMALL,
    marginLeft: 8
  },
  viewRightHeader: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-end',
    marginBottom: 5,
    paddingLeft: 10,
  },
  viewRightButton: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  viewJoin: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  viewFolow: {
    width: 20,
    height: 20,
    borderRadius: 10,
    borderWidth: 0.5,
    borderColor: '#8F8F8F',
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: -13,
    backgroundColor: 'white'
  },
  avatarFolow: {
    width: 20,
    height: 20,
    borderRadius: 10
  },
  textFolow: {
    color: 'rgba(0, 0, 0, 1)',
    fontSize: 9,
    fontWeight: common().FONT_WEIGHT_HEADER
  },
  viewSeparator: {
    height: 0.6,
    backgroundColor: common().COLOR_SEPARATOR
  },
});

export default RowRelatedEvent;
