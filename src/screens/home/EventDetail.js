import React, { PureComponent } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  ImageBackground,
  Dimensions,
  ScrollView,
  FlatList,
} from 'react-native';
import { connect } from 'react-redux';
import { CachedImage } from 'react-native-cached-image';
import moment from 'moment';
import localization from 'moment/locale/vi';
import { Button, Avatar, CustomNavbar } from '../../components';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import langs from '../../languages/home';
import { imgs } from '../../config/imgs';
import { common } from '../../config/common';
import { Actions } from 'react-native-router-flux';
import { paddingTop } from '../../components/Navbar/CustomNavbar';
import RowRelatedEvent from './RowRelatedEvent';

const SCREEN = Dimensions.get('window');

const AVATAR_URLS = [
  'https://lh6.googleusercontent.com/-PYvLVdvXywk/URqunwd8hfI/AAAAAAAAAbs/qiMwgkFvf6I/s1024/Fitzgerald%252520Streaks.jpg',
  "https://lh6.googleusercontent.com/-GHeImuHqJBE/URqu_FKfVLI/AAAAAAAAAbs/axuEJeqam7Q/s1024/Sailing%252520Stones.jpg",
  "https://lh3.googleusercontent.com/-hBbYZjTOwGc/URqu_ycpIrI/AAAAAAAAAbs/nAdJUXnGJYE/s1024/Seahorse.jpg",
  "https://lh3.googleusercontent.com/-Iwi6-i6IexY/URqvAYZHsVI/AAAAAAAAAbs/5ETWl4qXsFE/s1024/Shinjuku%252520Street.jpg",
  "https://lh6.googleusercontent.com/-amhnySTM_MY/URqvAlb5KoI/AAAAAAAAAbs/pFCFgzlKsn0/s1024/Sierra%252520Heavens.jpg",
  "https://lh5.googleusercontent.com/-dJgjepFrYSo/URqvBVJZrAI/AAAAAAAAAbs/v-F5QWpYO6s/s1024/Sierra%252520Sunset.jpg",
  "https://lh4.googleusercontent.com/-Z4zGiC5nWdc/URqvBdEwivI/AAAAAAAAAbs/ZRZR1VJ84QA/s1024/Sin%252520Lights.jpg",
  "https://lh4.googleusercontent.com/-_0cYiWW8ccY/URqvBz3iM4I/AAAAAAAAAbs/9N_Wq8MhLTY/s1024/Starry%252520Lake.jpg",
]

const RELATED_EVENTS = [
  {
    name: 'Friendly Spring Tournament',
    location: 'Nan Lian Garden, Hong Kong',
  },
  {
    name: 'Friendly Spring Tournament',
    location: 'Nan Lian Garden, Hong Kong',
  },
  {
    name: 'Friendly Spring Tournament',
    location: 'Nan Lian Garden, Hong Kong',
  },
  {
    name: 'Friendly Spring Tournament',
    location: 'Nan Lian Garden, Hong Kong',
  },
  {
    name: 'Friendly Spring Tournament',
    location: 'Nan Lian Garden, Hong Kong',
  }
];

const HOBBIES_DEFAULT = [
  {
    "_id": "5ba89c084050f319a0c38188",
    "name": "Sports",
    "_createDate": "2018-09-24T08:10:48.874Z"
  },
  {
    "_id": "5ba89c5b4050f319a0c38189",
    "name": "Outdoor",
    "_createDate": "2018-09-24T08:12:11.326Z"
  },
  {
    "_id": "5ba89c6c4050f319a0c3818a",
    "name": "Clothing",
    "_createDate": "2018-09-24T08:12:28.891Z"
  },
  {
    "_id": "5ba89c724050f319a0c3818b",
    "name": "Travel",
    "_createDate": "2018-09-24T08:12:34.576Z"
  },
  {
    "_id": "5ba89c7d4050f319a0c3818c",
    "name": "Games",
    "_createDate": "2018-09-24T08:12:45.689Z"
  },
  {
    "_id": "5ba89c864050f319a0c3818d",
    "name": "Fitness",
    "_createDate": "2018-09-24T08:12:54.578Z"
  }
];

class RowHome extends PureComponent {
  render() {
    let { post, language, userInfo, group, userID } = this.props;
    let createTime = '';
    // if (language === 'vn') {
    //   createTime = moment(post.created_time.replace(' ', 'T')).locale('vi', localization).fromNow();
    // } else {
    //   createTime = moment(post.created_time.replace(' ', 'T')).locale('en', localization).fromNow();
    // }

    // const avatarUser = post.user_created.avatar ? post.user_created.avatar : null;
    // const images = post.medias && post.medias.images ? post.medias.images : null
    // const isAddmin = group.user_resident_id === userID
    // const isMyPost = userID === post.user_resident_id
    // const showIconMore = (userID === post.user_resident_id)

    const isAddmin = true
    const showIconMore = true

    const hobbies = HOBBIES_DEFAULT;

    let urls = AVATAR_URLS;
    let avatarUrls = [];

    if (urls && urls.length > 5) {
      for (let i = 0; i < 5; i++) {
        avatarUrls[i] = urls[i];
      }
    } else {
      avatarUrls = urls;
    }

    return (
      <View style={styles.container}>
        <CustomNavbar
          title={'Event Detail'}
          leftIcon={true}
          back
        />
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.body}>

            <View style={styles.viewHeader}>
              <View style={styles.viewTitle}>
                <Text numberOfLines={1} style={[styles.txtTitle]}>
                  Friendly Spring Tournament
            </Text>
              </View>

            </View>

            <View style={styles.viewBody}>
              <Image
                source={{ uri: 'https://lh3.googleusercontent.com/-hBbYZjTOwGc/URqu_ycpIrI/AAAAAAAAAbs/nAdJUXnGJYE/s1024/Seahorse.jpg' }}
                resizeMode="cover"
                style={styles.avatarBody} />
            </View>

            <View style={styles.viewFooter}>
              <View style={styles.viewLeftButton}>
                <View style={styles.viewAvatar}>
                  {
                    <Image source={imgs().avatarDefault} resizeMode="cover" style={styles.avatar} />
                  }
                </View>
                <View style={styles.viewNameUser}>
                  <Text numberOfLines={1} style={styles.txtNameUser}>
                    Lelia Colon
              </Text>
                  <Text numberOfLines={1} style={styles.txtSmall}>
                    Manager at IOtech
              </Text>
                </View>
              </View>
              <View style={styles.viewRightButton}>
                <View style={styles.viewJoin}>
                  {
                    avatarUrls.map(url => {
                      if (urls.length > 5 && avatarUrls.indexOf(url) === avatarUrls.length - 1) {
                        return (
                          <View style={styles.viewFolow}>
                            {
                              <Text numberOfLines={1} style={styles.textFolow}>
                                {`+${urls.length - avatarUrls.length + 1}`}
                              </Text>
                            }
                          </View>
                        );
                      }
                      return (
                        <View style={styles.viewFolow}>
                          {
                            <Image source={{ uri: url }} resizeMode="cover" style={styles.avatarFolow} />
                          }
                        </View>
                      );
                    })
                  }
                </View>
              </View>
            </View>

            <View style={styles.viewLeftHeader}>
              <Icon name={'location-on'} size={15} color={common().COLOR_TEXT_INACTIVE} />
              <Text numberOfLines={1} style={styles.txtLocation}>
                Nan Lian Garden, Hong Kong
            </Text>
            </View>

            <View style={styles.viewFromDate}>
              <Text numberOfLines={1} style={styles.txtDateTitle}>
                From
            </Text>
              <Text numberOfLines={1} style={[styles.txtContent, { marginLeft: 8 }]}>
                20h : 30 - 25/3/2018
            </Text>
            </View>
            <View style={styles.viewFromDate}>
              <Text numberOfLines={1} style={styles.txtDateTitle}>
                To
            </Text>
              <Text numberOfLines={1} style={[styles.txtContent, { marginLeft: 8 }]}>
                20h : 30 - 25/3/2018
            </Text>
            </View>

            <View style={styles.viewRightHeader}>
              <View style={{ width: 35, justifyContent: 'center', alignItems: 'flex-start' }}>
                <Image source={imgs().symbol} resizeMode="cover" style={styles.imageSymbol} />
              </View>

              <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  {
                    hobbies.length > 0 && hobbies.map(hobby => {
                      return (
                        <Button
                          title={hobby.name}
                          style={styles.buttonHobby}
                          // onPress={() => this.onSelectedHobbies(hobby)}
                          width={80}
                          height={30}
                          rounded
                          backgroundColor={'#B8E6EB'}
                          color={'rgba(0, 0, 0, 0.7)'}
                        />
                      )
                    })
                  }
                </View>
              </ScrollView>

            </View>

            <View style={{ justifyContent: 'center', alignItems: 'flex-start', marginTop: 20 }}>
              <Text style={[styles.txtContent]}>
                IOTech specializes in firmware/hardware design and development services for embedded systems.
            </Text>
            </View>

          </View>
          <View style={styles.body2}>
            <Text style={{ color: 'rgba(0, 0, 0, 0.5', fontSize: common().FONT_SIZE_SMALL }}>
              Related events (5)
            </Text>
            {
              RELATED_EVENTS.length > 0 ? (
                <FlatList
                  data={RELATED_EVENTS}
                  keyExtractor={(item, index) => String(index)}
                  renderItem={this.renderItem}
                  extraData={this.state}
                />
              ) : this.renderEmptyListHome()
            }
          </View>

          <View style={styles.viewButton}>
            <Button
              title={"Apply"}
              style={styles.buttonJoin}
              // onPress={this.handleLetGo}
              rounded
              backgroundColor={'#FFDE03'}
              color={'rgba(0, 0, 0, 0.7)'}
              titleStyle={{ fontSize: common().FONT_SIZE_TITLE, fontWeight: common().FONT_WEIGHT_HEADER }}
            />
          </View>

          {/* <View style={style = styles.viewBottom} /> */}
        </ScrollView>

      </View>
    );
  }

  renderOption = (item) => {
    return (
      <View style={styles.viewOption}>
        <Icon name={item.icon} size={24} color={common().COLOR_TEXT_TITLE} />
        <Text numberOfLines={1} style={styles.txtOpntion}>
          {item.title}
        </Text>
      </View>
    )
  }

  renderItem = () => {
    return (
      <RowRelatedEvent />
    )
  }

  onSelectedJoinMe = () => {

  }

  handDetailComment = () => {
    Actions.commentPost({ item: this.props.post, group: this.props.group })
  }

  onPressMore = () => {
    if (this.refs.modalCard) {
      this.refs.modalCard.refs.actionSheetCard.open()
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  body: {
    flex: 1,
    backgroundColor: common().BACKGROUND_COLOR_CARD,
    marginTop: 10,
    marginBottom: 10,
    padding: 16,
    shadowColor: '#000000',
    elevation: 4,
    shadowOffset: {
      width: 1,
      height: 1
    },
    shadowRadius: 1,
    shadowOpacity: 0.3
  },
  body2: {
    backgroundColor: common().BACKGROUND_COLOR_CARD,
    padding: 16,
    shadowColor: '#000000',
    elevation: 4,
    shadowOffset: {
      width: 1,
      height: 1
    },
    shadowRadius: 1,
    shadowOpacity: 0.3
  },
  viewHeader: {
    flexDirection: 'row',
  },
  viewAvatar: {
    width: 40,
    height: 40,
    borderRadius: 20,
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatarBody: {
    flex: 1,
    width: SCREEN.width - 32,
    borderRadius: 6
  },
  avatar: {
    width: 40,
    height: 40,
    borderRadius: 20
  },
  viewCalendar: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewTitle: {
    flex: 1,
    justifyContent: 'center',
  },
  viewNameUser: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 15
  },
  imageDate: {
    width: 54,
    height: 24,
    padding: 5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  txtDate: {
    color: common().COLOR_TEXT_ACTIVE,
    fontSize: common().FONT_SIZE_SMALL_10,
    fontWeight: common().FONT_WEIGHT_TITLE,
  },
  txtTitle: {
    color: common().COLOR_TEXT_ACTIVE,
    fontSize: common().FONT_SIZE_CONTENT,
    fontWeight: common().FONT_WEIGHT_HEADER,
  },
  txtNameUser: {
    color: common().COLOR_TEXT_ACTIVE,
    fontSize: common().FONT_SIZE_CONTENT,
    fontWeight: common().FONT_WEIGHT_HEADER,
  },
  viewLeftHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 20,
    marginBottom: 20,
  },
  viewFromDate: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  txtLocation: {
    color: common().COLOR_TEXT_ACTIVE,
    fontSize: common().FONT_SIZE_CONTENT,
    fontWeight: common().FONT_WEIGHT_HEADER,
    marginLeft: 8
  },
  txtSmall: {
    color: common().COLOR_TEXT_INACTIVE,
    fontSize: common().FONT_SIZE_SMALL_10,
    marginTop: 5
  },
  txtDateTitle: {
    width: 35,
    color: common().COLOR_TEXT_INACTIVE,
    fontSize: common().FONT_SIZE_SMALL,
  },
  viewMore: {
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 8,
  },
  viewRightHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 20
  },
  viewBody: {
    height: 214,
    paddingVertical: 12,
  },
  viewMoreText: {
    margin: 12,
    marginTop: 0
  },
  txtContent: {
    color: common().COLOR_TEXT_ACTIVE,
    fontSize: common().FONT_SIZE_CONTENT,
    fontWeight: common().FONT_WEIGHT_HEADER,
  },
  viewNumberComment: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 12,
    paddingBottom: 12,
    justifyContent: 'space-between',
  },
  viewNumberFavorite: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  txtNumberComment: {
    color: common().COLOR_TEXT_ACTIVE,
    fontSize: common().FONT_SIZE_CONTENT,
    fontWeight: common().FONT_WEIGHT_TITLE,
    marginLeft: 4
  },
  viewFooter: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  viewLeftButton: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  viewRightButton: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  viewJoin: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  txtBtn: {
    marginLeft: 12,
    color: common().COLOR_TEXT_ACTIVE,
    fontSize: common().FONT_SIZE_TITLE,
    fontWeight: common().FONT_WEIGHT_TITLE
  },
  viewOption: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 12
  },
  txtOpntion: {
    color: common().COLOR_TEXT_TITLE,
    fontSize: common().FONT_SIZE_HEADER,
    marginLeft: 8
  },
  viewFolow: {
    width: 20,
    height: 20,
    borderRadius: 10,
    borderWidth: 0.5,
    borderColor: '#8F8F8F',
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: -13,
    backgroundColor: 'white'
  },
  avatarFolow: {
    width: 20,
    height: 20,
    borderRadius: 10
  },
  textFolow: {
    color: 'rgba(0, 0, 0, 1)',
    fontSize: 9,
    fontWeight: common().FONT_WEIGHT_HEADER
  },
  imageSymbol: {
    width: 20,
    height: 20,
  },
  txtSymbol: {
    color: common().COLOR_TEXT_INACTIVE,
    fontSize: common().FONT_SIZE_SMALL,
    marginLeft: 7,
  },
  buttonJoin: {
    alignSelf: 'center',
    flexDirection: 'row',
    paddingVertical: 4,
  },
  textJoin: {
    color: 'rgba(0, 0, 0, 0.8)',
    fontSize: common().FONT_SIZE_CONTENT,
    fontWeight: common().FONT_WEIGHT_HEADER
  },
  viewButton: {
    paddingBottom: 16,
    justifyContent: 'center',
    backgroundColor: 'white'
  },
  buttonHobby: {
    marginRight: 8,
  },
  viewBottom: {
    marginBottom: 22
  },
});

export default RowHome;
