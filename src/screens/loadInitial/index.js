/**
* Created by bav on Sat Jul 14 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import { StyleSheet, Dimensions, Animated, Easing } from 'react-native';
import { View, Text, Image } from 'react-native-animatable';
import { imgs } from '../../config/imgs';
import { Actions } from 'react-native-router-flux';
import { common } from '../../config/common';

const SCREEN = Dimensions.get('window');

class Dashboard extends PureComponent {
    constructor() {
        super()
        this.spinValue = new Animated.Value(0);
        this.state = {
            slogan: ' '
        };
    }

    componentDidMount() {
        this.spin();

        this.logoRef.animate('zoomIn', 2000).then(() => {
            this.setState({
                slogan: 'Make your net work'
            });
            this.textRef.animate('slideInLeft', 1000).then(() => {
                setTimeout(() => {
                    this.props.autoLogin();
                }, 100);
            });
        });
    }

    handleLogoRef = ref => this.logoRef = ref;
    handleTextRef = ref => this.textRef = ref;

    spin = () => {
        this.spinValue.setValue(0);
        Animated.timing(
            this.spinValue,
            {
                toValue: 1,
                duration: 1000,
                easing: Easing.linear
            }
        ).start(() => this.spin());
    }

    render() {
        const spin = this.spinValue.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '360deg']
        });

        return (
            <View style={styles.container}>
                <Image
                    source={imgs().splash}
                    style={{ resizeMode: 'stretch', width: SCREEN.width, height: SCREEN.height }}
                />
                <View style={styles.viewLogo}>
                    <Image
                        ref={this.handleLogoRef}
                        source={imgs().logo}
                        style={styles.imgLogo}
                    />
                    {<Text ref={this.handleTextRef} style={styles.textSlogan}>
                        {this.state.slogan}
                    </Text>}
                </View>

                {/* <View style={styles.loading}>
                    <Animated.Image
                        style={{
                            transform: [{ rotate: spin }]
                        }}
                        source={imgs().icLoading}
                    />
                </View> */}
                <Text style={styles.textCopyright}>
                    Copyright © 2018 TheCirkl
                </Text>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    loading: {
        width: 100,
        height: 100,
        position: 'absolute'
    },
    viewLogo: {
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
    },
    imgLogo: {
        resizeMode: 'stretch'
    },
    textSlogan: {
        fontSize: 17,
        fontWeight: common().FONT_WEIGHT_HEADER,
        color: '#115A63',
        marginTop: 23
    },
    textCopyright: {
        position: 'absolute',
        fontSize: common().FONT_SIZE_CONTENT,
        color: 'white',
        bottom: 25
    }
});

export default Dashboard;

