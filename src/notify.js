/**
* Created by bav on Fri Jul 13 2018
* Copyright (c) 2018 bav
*/

import React, { PureComponent } from 'react';
import OneSignal from 'react-native-onesignal';

export default class Notify extends PureComponent {
  componentWillMount() {
    OneSignal.init("9f18fe5c-f2bb-4435-96cd-89f94ad45319");

    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('ids', this.onIds);
  }

  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('ids', this.onIds);
  }

  onReceived(notification) {
    console.log("Notification received: ", notification);
  }

  onOpened(openResult) {
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);
  }

  onIds(device) {
    console.log('Device info: ', device);
  }

  render() {
    return null;
  }
}
