import LocalizedStrings from 'react-native-localization';

let langs = new LocalizedStrings({
  en: {
    register: 'Register',
    notAccount: 'You have not acount?',
    username: 'Username',
    password: 'Password',
    login: 'Sign in with Linkedln',
    forgotPassword: 'Forgot password',
    back: 'Back',
    email: 'Email',
    resetPassword: 'Reset password',
    newPassword: 'New password',
    confirmPassword: 'Confirm new password',
    haveAccount: 'You have a account',
    fromLibrary: 'Select from library',
    camera: 'Take a photo',
    cancel: 'Cancel',
    txtWelcome: 'Welcome',
    txtContent: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.'
  },
  vi: {
    register: 'Đăng kí',
    notAccount: 'Bạn chưa có tài khoản?',
    username: 'Tên đăng nhập',
    password: 'Mật khẩu',
    login: 'Sign in with Linkedln',
    forgotPassword: 'Quên mật khẩu',
    back: 'Quay lại',
    email: 'Email',
    resetPassword: 'Lấy lại mật khẩu',
    newPassword: 'Mật khẩu mới',
    confirmPassword: 'Nhập lại mật khẩu',
    haveAccount: 'Bạn đã có tài khoản',
    fromLibrary: 'Chọn ảnh từ thư viện',
    camera: 'Chụp ảnh',
    cancel: 'Huỷ',
    txtWelcome: 'Welcome',
    txtContent: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.'
  }
})

export default langs;
