'use strick';

const configlang = [
  require('./common'),
  require('./login'),
  require('./camera'),
  require('./dashboard'),
  require('./setting'),
];

export default configlang;
