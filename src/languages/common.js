import LocalizedStrings from 'react-native-localization';

let langs = new LocalizedStrings({
  en: {
    ok: 'OK',
    notification: 'Notification',
    errorInternet: 'Network request failed',
    errorUploadAvatar: 'Can\'t upload file',
  },
  vi: {
    ok: 'OK',
    notification: 'Notification',
    errorInternet: 'Network request failed',
    errorUploadAvatar: 'Can\'t upload file',
  }
})

export default langs;
