/**
* Created by bav on Tue Jul 17 2018
* Copyright (c) 2018 bav
*/

import { store } from '../redux/configureStore';

let currentValue = 'light-theme';

function select(state) {
  return state.config.theme
}

function handleChange() {
  let previousValue = currentValue
  currentValue = select(store.getState())

  if (previousValue !== currentValue) {
    
  }
}

store.subscribe(handleChange)

export default function getTheme() {
  return currentValue
}