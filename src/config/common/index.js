/**
* Created by bav on Fri Jun 01 2018
* Copyright (c) 2018 bav
*/

import getTheme from '../getTheme';

const common = () => {
  return getTheme() === 'dark-theme' ? require('./dark-theme') : require('./light-theme');
}

export {
  common
};
