/**
* Created by bav on Fri Jun 01 2018
* Copyright (c) 2018 bav
*/

import { Platform } from 'react-native';

export const FONT_SIZE_HUGE = 24;
export const FONT_SIZE_HEADER = 20;
export const FONT_SIZE_TITLE = 16;
export const FONT_SIZE_CONTENT = 14;
export const FONT_SIZE_SMALL = 12;
export const FONT_SIZE_SMALL_10 = 10;
export const FONT_SIZE_HEADER_20 = 20;
export const FONT_SIZE_ICON_TABLET = 36;
export const FONT_SIZE_TITLE_TABLET = 20;
 
export const FONT_WEIGHT_HEADER = "500";
export const FONT_WEIGHT_TITLE = "400";
export const FONT_WEIGHT_CONTENT = "300";

//background
export const BACKGROUND_COLOR = "#F3F4F5";
export const BACKGROUND_GRADIENT1 = "#174E91";
export const BACKGROUND_GRADIENT2 = "#323656";
export const BACKGROUND_COLOR_TABBAR = "#12B8CB";
export const BACKGROUND_COLOR_NAVBAR = "#FEFEFE";
export const BACKGROUND_COLOR_CARD = "white";
export const BACKGROUND_COLOR_LOADING = "#919AA3";
export const BACKGROUND_COLOR_BUTTON_ACTIVE = "#008AEE";
export const BACKGROUND_COLOR_BUTTON_INACTIVE = "#242843";

export const COLOR_ICON_ACTIVE = "rgba(255, 255, 255, 0.8)";
export const COLOR_ICON_INACTIVE = "#8182A8";
export const COLOR_ICON_NORMAL = "#fff";
export const COLOR_SEPARATOR = "#707070";
export const COLOR_SEPARATOR_WHITE = "#fff";
export const COLOR_BORDER_TABLET = "#242843";

//Tabbar
export const COLOR_TEXT_TABBAR_ACTIVE = "#fff";
export const COLOR_TEXT_TABBAR_INACTIVE = "rgba(0, 0, 0, 0.54)";
export const COLOR_ICON_TABBAR_ACTIVE = "#fff";
export const COLOR_ICON_TABBAR_INACTIVE = "rgba(0, 0, 0, 0.4)";

export const COLOR_TEXT_NAVBAR = "white";

export const COLOR_TEXT_TITLE = "rgba(0, 0, 0, 0.38)";
export const COLOR_TEXT_NORMAL = "#fff";
export const COLOR_TEXT_ACTIVE = "rgba(0, 0, 0, 1)";
export const COLOR_TEXT_INACTIVE = "rgba(0, 0, 0, 0.38)";

// Input
export const INPUT_BACKGROUND_COLOR = "rgba(255, 255, 255, 0.1)";
export const INPUT_COLOR_TEXT_PLACEHOLDER = "rgba(255, 255, 255, 0.6)";
export const INPUT_COLOR_TEXT = "#fff";
export const INPUT_SELECTION_COLOR = "#008AEE";
export const INPUT_BACKGROUND_FORCUS = "rgba(0, 0, 0, 0.2)";
export const INPUT_COLOR_ICON = "#fff";

//Indicator
export const COLOR_INDICATOR = "#fff";

//Action sheet
export const ACTION_SHEET_BACKGROUND_COLOR = Platform.OS === "ios" ? "transparent" : "#585C7C";
export const ACTION_SHEET_COLOR_TEXT = "#fff";
export const ACTION_SHEET_COLOR_TEXT_ACTIVE = "#fff";
export const ACTION_SHEET_COLOR_CHECK_ICON = "#008AEE";
export const ACTION_SHEET_COLOR_SEPARATOR = "rgba(225, 255, 255, 0.3)";

//Alert
export const ALERT_BACKGROUND_COLOR = Platform.OS === "ios" ? "transparent" : "#585C7C";
export const ALERT_COLOR_TEXT = "#fff";
export const ALERT_COLOR_TEXT_ACTIVE = "#C74545";
export const ALERT_COLOR_SEPARATOR = "rgba(225, 255, 255, 0.3)";

//TabView
export const TABVIEW_UNDERLINE_COLOR = "#008AEE";
export const TABVIEW_BACKGROUND_COLOR = "transparent";
export const TABVIEW_ACTIVE_TEXT_COLOR = "white";
export const TABVIEW_INACTIVE_TEXT_COLOR = "rgba(255, 255, 255, 0.6)";
export const TABVIEW_SEPARATOR_COLOR = "rgba(255, 255, 255, 0.2)";

//Device
export const DEVICE_BACKGROUND_COLOR_ACTIVE = "#008AEE";
export const DEVICE_BACKGROUND_COLOR_INACTIVE = "#242843";
export const DEVICE_TEXT_COLOR_ACTIVE = "#fff";
export const DEVICE_TEXT_COLOR_INACTIVE = "#8182A8";
export const DEVICE_ICON_COLOR_ACTIVE = "#fff";
export const DEVICE_ICON_COLOR_INACTIVE = "#8182A8";

// Status Home
export const STATUS_ONLINE_COLOR = "#20D250";
export const STATUS_OFFLINE_COLOR = "#4E586E";
export const STATUS_LOST_CONNECT_COLOR = "#CE3F3F";

// Card+ Row
export const CARD_BACKGROUND_COLOR = "rgba(255, 255, 255, 0.1)";

// Checkbox
export const CHECKBOX_ACTIVE_COLOR = "#008AEE";
export const CHECKBOX_INACTIVE_COLOR = "rgba(255, 255, 255, 0.6)";
export const CHECKBOX_BACKGROUND_COLOR = "rgba(255, 255, 255, 0.1)";

// Switch
export const SWITCH_ACTIVE_COLOR = "#008AEE";
export const SWITCH_THUMB_ON_COLOR = "white";
export const SWITCH_INACTIVE_COLOR = "rgba(255, 255, 255, 0.6)";

// Slider
export const SLIDER_ACTIVE_COLOR = "#008AEE";
export const SLIDER_INACTIVE_COLOR = "rgba(255, 255, 255, 0.4)";
export const SLIDER_CIRCLE_COLOR = "#515775";

// Circle picker
export const CIRCLE_BACKGROUND_COLOR = "#1F223D";

export const BACKGROUND_COLOR_DATE_TIME_PICKER = "#626A8D";

export const COLOR_SECURITY_ALL_SAFE = "#20D250";
export const COLOR_SECURITY_DANGER = "#CE0000";
