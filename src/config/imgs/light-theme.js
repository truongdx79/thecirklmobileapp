/**
* Created by bav on Sat Jun 02 2018
* Copyright (c) 2018 bav
*/

export const splash = require('../../assets/imgs/splash/bg_splash_3.png');
export const icLoading = require('../../assets/imgs/loading/icon_loading.png');
export const backgroundImageLogin = require('../../assets/imgs/login/bg_login.png');
export const backgroundTabbar = require('../../assets/imgs/lightTheme/backgroundTabbar/backgroundTabbar.png');
export const logo = require('../../assets/imgs/lightTheme/logo/logo.png');
export const blurImage = require('../../assets/imgs/lightTheme/blurBackground/blurBackground.png');

export const buttonLoginLinkedin = require('../../assets/imgs/login/button_signin_linkedin.png');
export const bgLogin = require('../../assets/imgs/login/bg_login.png');
export const bgProfile = require('../../assets/imgs/profile/bg_update_profile.png');

export const avatarDefault = require('../../assets/imgs/defaultAvatar/defaultAvatar.png');
export const defaultGroup = require('../../assets/imgs/defaultGroup/defaultGroup.png');

export const tabHome = require('../../assets/imgs/tab/ic_home.png');
export const tabJob = require('../../assets/imgs/tab/ic_job.png');
export const tabGps = require('../../assets/imgs/tab/ic_gps.png');
export const tabEmail = require('../../assets/imgs/tab/ic_email.png');
export const tabSetting = require('../../assets/imgs/tab/ic_setting.png');

export const calendar0 = require('../../assets/imgs/common/ic_calendar_0.png');
export const calendar1 = require('../../assets/imgs/common/ic_calendar_1.png');
export const filter = require('../../assets/imgs/common/ic_filter.png');
export const symbol = require('../../assets/imgs/common/ic_symbol.png');
