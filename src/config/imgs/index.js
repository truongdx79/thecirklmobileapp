/**
* Created by bav on Sat Jun 02 2018
* Copyright (c) 2018 bav
*/

import getTheme from '../getTheme';

const imgs = () => {
  return getTheme() === 'dark-theme' ? require('./dark-theme') : require('./light-theme');
}

export {
  imgs
};
